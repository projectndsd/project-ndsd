﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditScript : MonoBehaviour {

    public struct SelectedCreditor
    {
        public Image portrait;
        public Text name, role, note;
    }

    public SelectedCreditor[] selectedCreditor;
    public GameObject ZeeYang, HueyJuan, PeHao, GiapYong, preloadedAssets;
    public Button button;
    public Image creditPortrait;
    public Text creditName, creditRole, creditNote;
    AudioSource onClickSound;
    int k;
	// Use this for initialization
	void Start () {
        selectedCreditor = new SelectedCreditor[4];
        LoadAssets();
        preloadedAssets = GameObject.Find("PreloadedAssets");
        ZeeYang = GameObject.Find("ZeeYang");
        HueyJuan = GameObject.Find("HueyJuan");
        PeHao = GameObject.Find("PeHao");
        GiapYong = GameObject.Find("GiapYong");
        button = GameObject.Find("CreditScene").transform.Find("Background").transform.Find("NextButton").GetComponent<Button>();
        creditPortrait = GameObject.Find("CreditScene").transform.Find("Background").transform.Find("Portrait").GetComponent<Image>();
        creditName = GameObject.Find("CreditScene").transform.Find("Background").transform.Find("Name").GetComponent<Text>();
        creditRole = GameObject.Find("CreditScene").transform.Find("Background").transform.Find("Role").GetComponent<Text>();
        creditNote = GameObject.Find("CreditScene").transform.Find("Background").transform.Find("DeveloperNote").GetComponent<Text>();
        creditPortrait.sprite = selectedCreditor[0].portrait.sprite;
        creditName.text = selectedCreditor[0].name.text;
        creditRole.text = selectedCreditor[0].role.text;
        creditNote.text = selectedCreditor[0].note.text;
        k = 0;
        onClickSound = GameObject.Find("preloaded_assets").transform.Find("onclickSound").GetComponent<AudioSource>();
        button.onClick.AddListener(delegate
        {
            onClickSound.Play();
            k++;
            creditPortrait.sprite= selectedCreditor[k % 4].portrait.sprite;
            creditName.text = selectedCreditor[k % 4].name.text;
            creditRole.text = selectedCreditor[k % 4].role.text;
            creditNote.text = selectedCreditor[k % 4].note.text;
        });

        preloadedAssets.SetActive(false);
	}
	
	

    void LoadAssets()
    {
        selectedCreditor[0].name = ZeeYang.transform.Find("Name").GetComponent<Text>();
        selectedCreditor[0].role = ZeeYang.transform.Find("Role").GetComponent<Text>();
        selectedCreditor[0].note = ZeeYang.transform.Find("Note").GetComponent<Text>();
        selectedCreditor[0].portrait = ZeeYang.transform.Find("Photo").GetComponent<Image>();

        selectedCreditor[1].name = HueyJuan.transform.Find("Name").GetComponent<Text>();
        selectedCreditor[1].role = HueyJuan.transform.Find("Role").GetComponent<Text>();
        selectedCreditor[1].note = HueyJuan.transform.Find("Note").GetComponent<Text>();
        selectedCreditor[1].portrait = HueyJuan.transform.Find("Photo").GetComponent<Image>();

        selectedCreditor[2].name = PeHao.transform.Find("Name").GetComponent<Text>();
        selectedCreditor[2].role = PeHao.transform.Find("Role").GetComponent<Text>();
        selectedCreditor[2].note = PeHao.transform.Find("Note").GetComponent<Text>();
        selectedCreditor[2].portrait = PeHao.transform.Find("Photo").GetComponent<Image>();

        selectedCreditor[3].name = GiapYong.transform.Find("Name").GetComponent<Text>();
        selectedCreditor[3].role = GiapYong.transform.Find("Role").GetComponent<Text>();
        selectedCreditor[3].note = GiapYong.transform.Find("Note").GetComponent<Text>();
        selectedCreditor[3].portrait = GiapYong.transform.Find("Photo").GetComponent<Image>();
    }
}
