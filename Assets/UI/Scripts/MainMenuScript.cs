﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour {
    AudioSource onClickSound;
    Image backgroundImage, sixteenNine, fourThree;
    GameObject preloadedAssets, settingPopup, creditPopup;
    Button closeSettingButton, settingButton, creditButton, closeCreditButton, exitGameButton;

	// Use this for initialization
	void Start () {
        Vector2 aspectRatio = AspectRatio.GetAspectRatio(Screen.width, Screen.height);
        preloadedAssets = GameObject.Find("preloaded_assets");
        backgroundImage = GameObject.Find("Canvas").transform.Find("Background").GetComponent<Image>();
        sixteenNine = preloadedAssets.transform.Find("16:9").gameObject.GetComponent<Image>();
        fourThree = preloadedAssets.transform.Find("4:3").gameObject.GetComponent<Image>();
        settingButton = GameObject.Find("Setting").GetComponent<Button>();
        settingButton.onClick.AddListener(SettingOnClick);
        creditButton = GameObject.Find("Credit").GetComponent<Button>();
        creditButton.onClick.AddListener(delegate
        {
            onClickSound.Play();
            creditPopup.SetActive(true);
        });
        onClickSound = preloadedAssets.transform.Find("onclickSound").GetComponent<AudioSource>();
        if (aspectRatio.x == 16.0f || aspectRatio.y == 9.0f)
        {
            Debug.Log("aspect ratio is 16:9");
            backgroundImage.sprite = sixteenNine.sprite;
        }
        else
        {
            Debug.Log("aspect ratio is not 16:9");
            backgroundImage.sprite = fourThree.sprite;
        }

        settingPopup = GameObject.Find("MainMenuSetting");
        closeSettingButton = settingPopup.transform.Find("backgroundImage").transform.Find("CloseSettingButton").GetComponent<Button>();
        closeSettingButton.onClick.AddListener(CloseSettingButtonOnClick);
        exitGameButton = settingPopup.transform.Find("ExitGameButton").GetComponent<Button>();
        exitGameButton.onClick.AddListener(delegate
        {
            onClickSound.Play();
            Application.Quit();

        });
        settingPopup.SetActive(false);

        creditPopup = GameObject.Find("CreditScene");
        closeCreditButton = creditPopup.transform.Find("Background").transform.Find("CloseCreditButton").GetComponent<Button>();
        closeCreditButton.onClick.AddListener(delegate
        {

            onClickSound.Play();
            creditPopup.SetActive(false);
        });
        creditPopup.SetActive(false);
    }

    public static class AspectRatio
    {
        public static Vector2 GetAspectRatio(int x, int y)
        {
            float f = (float)x / (float)y;
            int i = 0;
            while (true)
            {
                i++;
                if (System.Math.Round(f * i, 2) == Mathf.RoundToInt(f * i))
                    break;
            }
            return new Vector2((float)System.Math.Round(f * i, 2), i);
        }
        public static Vector2 GetAspectRatio(Vector2 xy)
        {
            float f = xy.x / xy.y;
            int i = 0;
            while (true)
            {
                i++;
                if (System.Math.Round(f * i, 2) == Mathf.RoundToInt(f * i))
                    break;
            }
            return new Vector2((float)System.Math.Round(f * i, 2), i);
        }
        public static Vector2 GetAspectRatio(int x, int y, bool debug)
        {
            float f = (float)x / (float)y;
            int i = 0;
            while (true)
            {
                i++;
                if (System.Math.Round(f * i, 2) == Mathf.RoundToInt(f * i))
                    break;
            }
            if (debug)
                Debug.Log("Aspect ratio is " + f * i + ":" + i + " (Resolution: " + x + "x" + y + ")");
            return new Vector2((float)System.Math.Round(f * i, 2), i);
        }
        public static Vector2 GetAspectRatio(Vector2 xy, bool debug)
        {
            float f = xy.x / xy.y;
            int i = 0;
            while (true)
            {
                i++;
                if (System.Math.Round(f * i, 2) == Mathf.RoundToInt(f * i))
                    break;
            }
            if (debug)
                Debug.Log("Aspect ratio is " + f * i + ":" + i + " (Resolution: " + xy.x + "x" + xy.y + ")");
            return new Vector2((float)System.Math.Round(f * i, 2), i);
        }
    }

    void CloseSettingButtonOnClick()
    {
        onClickSound.Play();
        settingPopup.SetActive(false);
    }

    void SettingOnClick()
    {
        onClickSound.Play();
        settingPopup.SetActive(true);
    }
}
