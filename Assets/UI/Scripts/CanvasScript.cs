﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasScript : MonoBehaviour
{
	public Text debugText;
	[Header("Setting")]
	public GameObject buttonTemplate;
	public GameObject selectionButtonTemplate;
	public GameObject kliaButtonTemplate;
	public Transform kliaPathObject;
	public Transform lastLandOnWestPathObject;

	[Header("Internal Setting")]
	public int indexOfLastWestLand;
	public int indexOfKlia;
	public CameraFollow cameraFollow;
	public List<GameObject> buttonList;

    private GameManager gameManager;
	private GameObject playerSelectionBg;
	private GameObject kliaBg;

    private GameObject[] LandData;
	private GameObject mortgageScrollBg;
	private GameObject buttonGO;
    public Image playerIndicatorImage;
    public Text kampungAmount, cityAmount, landAmount, metropolisAmount, playerX, moneyAmount, diceAmount;
	public Button sellButton, buyButton, returnCenterButton, settingButton, kliaCloseButton, mortgageCloseButton;
	public int currentCharacter;
    private int currentPlayerRace;
	private int previousPlayerIndex;
	private int playerButtonIndex;
	private LandData propertyData;
	private Text landNameText;
	private Text mortgagePriceText;
	private Text selectionInstructionText;

	//text for mortgage and buy sell
	private Text moneyAmountText;
	private Text mortgageInstructionText;
	private Text buySellInstructionText;
	private Text playerPropertyText;

	private Text moneyClearedMsgText;
	private GameObject scrollViewContent;
	private GameObject playerScrollViewContent;
	private GameObject kliaScrollViewContent;
	private GameObject moneyClearedBg;	//using ChanceCardBackground UI
	private GameObject priceNegoBg;
	private string initialMoneyDisplayText;
	private Transform[] buttonChildTrans;
	private GameObject[] playerButtonList;
    public TestScript testScript;
    public Image chineseCharacter, indianCharacter, malayCharacter;
    public GameObject preloadedAssets, settingPopup;
    public Button closeSettingButton;
    public AudioSource onClickSound;
    public struct PlayerProperties
    {
        public int landOwned, kampungOwned, cityOwned, metropolisOwned;
    }
    public PlayerProperties[] playerProperties;

	void Awake ()
	{

	}
    // Use this for initialization
    void Start()
    {
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        testScript = GameObject.Find("PathObject").GetComponent<TestScript>();
        playerProperties = new PlayerProperties[gameManager.maxPlayer]; // max player will according to the amount now.

		//find the Scroll Content throughout all the parents
		GameObject theCanvas = GameObject.Find ("Canvas");
		mortgageScrollBg = theCanvas.transform.Find ("MortgageScrollBackground").gameObject;
		playerSelectionBg = theCanvas.transform.Find("PlayerSelectionBackground").gameObject;
		priceNegoBg = theCanvas.transform.Find ("PriceNegoBackground").gameObject;
		selectionInstructionText = playerSelectionBg.transform.Find("InstructionText").gameObject.GetComponent<Text>();
		moneyAmountText = mortgageScrollBg.transform.Find ("MoneyAmountText").gameObject.GetComponent<Text>();
		playerPropertyText = mortgageScrollBg.transform.Find ("PlayerPropertyText").gameObject.GetComponent<Text>();
		mortgageInstructionText = mortgageScrollBg.transform.Find ("MortgageInstructionText").gameObject.GetComponent<Text>();
		buySellInstructionText = mortgageScrollBg.transform.Find ("BuySellInstructionText").gameObject.GetComponent<Text>();
		kliaBg = theCanvas.transform.Find ("KLIABackground").gameObject;
		moneyClearedBg = theCanvas.transform.Find ("ChanceCardBackground").gameObject;
		GameObject scrollViewGO = mortgageScrollBg.transform.Find ("ScrollView").gameObject;
		GameObject kliaScrollViewGO = kliaBg.transform.Find ("ScrollView").gameObject;
		GameObject playerScrollViewGO = playerSelectionBg.transform.Find ("ScrollView").gameObject;
		Button playerCloseButton = playerSelectionBg.transform.Find ("CloseButton").gameObject.GetComponent<Button>();
		mortgageCloseButton = mortgageScrollBg.transform.Find ("CloseButton").gameObject.GetComponent<Button>();
		scrollViewContent = scrollViewGO.transform.Find ("ScrollContent").gameObject;
		playerScrollViewContent = playerScrollViewGO.transform.Find ("ScrollContent").gameObject;
		kliaScrollViewContent = kliaScrollViewGO.transform.Find ("ScrollContent").gameObject;
		moneyClearedMsgText = moneyClearedBg.transform.Find ("ChanceCardMessage").GetComponent<Text>();
		initialMoneyDisplayText = moneyAmountText.text;

        preloadedAssets = GameObject.Find("preloaded_assets");
        cameraFollow = GameObject.Find("CameraTarget").GetComponent<CameraFollow>();

        landAmount = GameObject.Find("land_amount_text").GetComponent<Text>();
        kampungAmount = GameObject.Find("kampung_amount_text").GetComponent<Text>();
        cityAmount = GameObject.Find("city_amount_text").GetComponent<Text>();
        metropolisAmount = GameObject.Find("metropolis_amount_text").GetComponent<Text>();
        playerX = GameObject.Find("player_indicator_parent").transform.Find("player_indicator").GetComponent<Text>();
        playerX.fontSize = 8;
        moneyAmount = GameObject.Find("money_text").GetComponent<Text>();
        diceAmount = GameObject.Find("dice_roll_amount_text").GetComponent<Text>();
        playerIndicatorImage = GameObject.Find("player_indicator_parent").transform.Find("player_indicator_image").GetComponent<Image>();
        diceAmount.text = "0";

		kliaCloseButton = kliaBg.transform.Find ("CloseButton").GetComponent<Button> ();
        sellButton = GameObject.Find("sell_button").GetComponent<Button>();
        buyButton = GameObject.Find("mortgage_button").GetComponent<Button>();
        returnCenterButton = GameObject.Find("return_center_button").GetComponent<Button>();
        settingButton = GameObject.Find("setting_button").GetComponent<Button>();

        LandData = GameObject.FindGameObjectsWithTag("lands");
        //All button will have their corresponding function called
		kliaCloseButton.onClick.AddListener (CloseKLIAFunction);
		sellButton.onClick.AddListener(SellFunction);
		buyButton.onClick.AddListener(BuyFunction);
        returnCenterButton.onClick.AddListener(ReturnCenterFunction);
        settingButton.onClick.AddListener(SettingFunction);
		playerCloseButton.onClick.AddListener(ClosePlayerSelection);
		mortgageCloseButton.onClick.AddListener(CloseMortgageList);
	
		//get all the buttons under the scroll content
		buttonChildTrans = scrollViewContent.GetComponentsInChildren<Transform> ();
		foreach (Transform child in buttonChildTrans)
		{
			if (child.tag == "mortgage_button")
			{
				buttonList.Add (child.gameObject);
				child.gameObject.SetActive (false);
			}
		}

        Debug.Log("Player Property Length" + playerProperties.Length);
        for (int i = 0; i< playerProperties.Length; i++)
        {
            playerProperties[i].landOwned = 0;
            playerProperties[i].cityOwned = 0;
            playerProperties[i].kampungOwned = 0;
            playerProperties[i].metropolisOwned = 0;

        }
		playerButtonList = new GameObject[GameManager.instance.maxPlayer];
		for (int i = 1; i < GameManager.instance.maxPlayer; i++)
		{
			GameObject playerSelectionButs = (GameObject)Instantiate (selectionButtonTemplate);
			playerButtonList [i - 1] = playerSelectionButs;
			playerSelectionButs.name = selectionButtonTemplate.name;
			playerSelectionButs.transform.parent = playerScrollViewContent.transform;
			playerSelectionButs.SetActive (false);
		}
		//variables for klia land button selection
		indexOfKlia = testScript.path_objs.IndexOf (kliaPathObject);
		indexOfLastWestLand = testScript.path_objs.IndexOf (lastLandOnWestPathObject);
		int chanceLandNum = 1;
		int westCount = 1;
		int eastCount = 1;
		int totalWestFees = 0;
		//to instantiate the button for klia land button selection
		for (int i = indexOfKlia + 1; i < testScript.path_objs.Count; i++)
		{
			LandData land = testScript.path_objs [i].gameObject.GetComponent<LandData> ();
			if ((land.type == global::LandData.landType.ChanceLand) || (land.type == global::LandData.landType.Genting)
			    || (land.type == global::LandData.landType.NormalLand) || (land.type == global::LandData.landType.ThemePark))
			{
				GameObject kliaButton = (GameObject)Instantiate (kliaButtonTemplate);
				switch (land.type)
				{
				case global::LandData.landType.ChanceLand:
					kliaButton.name =  testScript.path_objs [i].name;
					break;
				case global::LandData.landType.Genting:
					kliaButton.name = "Genting Highland";
					break;
				case global::LandData.landType.ThemePark:
					kliaButton.name = "Themepark";
					break;
				case global::LandData.landType.NormalLand:
					kliaButton.name = land.landName;
					break;
				}
				kliaButton.transform.Find ("LandNameText").GetComponent<Text> ().text = kliaButton.name;
				if (i <= indexOfLastWestLand)
				{
					totalWestFees = GameManager.instance.kliaWestFeesPerLand * westCount;
					kliaButton.GetComponent<KLIASystem> ().fee = totalWestFees;
					kliaButton.transform.Find ("FeesText").GetComponent<Text> ().text = totalWestFees.ToString ();
					westCount++;
				}
				else
				{
					int eastFees = totalWestFees + GameManager.instance.kliaEastFeesPerLand * eastCount;
					kliaButton.GetComponent<KLIASystem> ().fee = eastFees;
					kliaButton.transform.Find ("FeesText").GetComponent<Text> ().text = eastFees.ToString ();
					eastCount++;
				}
				kliaButton.GetComponent<KLIASystem> ().buttonName = kliaButton.name;
				kliaButton.GetComponent<Button>().onClick.AddListener(kliaButton.GetComponent<KLIASystem>().FlyToSelectedLand);
				kliaButton.transform.parent = kliaScrollViewContent.transform;
				AiComputer.instance.kliaButton.Add (kliaButton.GetComponent<Button>());
			}
		}

        //preload assets here
        chineseCharacter = preloadedAssets.transform.Find("chinese_image").GetComponent<Image>();
        indianCharacter = preloadedAssets.transform.Find("indian_image").GetComponent<Image>();
        malayCharacter = preloadedAssets.transform.Find("malay_image").GetComponent<Image>();
        onClickSound = preloadedAssets.transform.Find("onclickSound").GetComponent<AudioSource>();

        settingPopup = GameObject.Find("SettingsPopup");    
        closeSettingButton = settingPopup.transform.Find("backgroundImage").transform.Find("CloseSettingButton").GetComponent<Button>();
        closeSettingButton.onClick.AddListener(CloseSettingButtonOnClick);
        settingPopup.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        currentCharacter = cameraFollow.delayedCurrentCharacter;
        landAmount.text = playerProperties[cameraFollow.delayedCurrentCharacter].landOwned.ToString(); // to be changed to the amount of the Player has from the game manager
        kampungAmount.text = playerProperties[cameraFollow.delayedCurrentCharacter].kampungOwned.ToString(); //same as above
        cityAmount.text = playerProperties[cameraFollow.delayedCurrentCharacter].cityOwned.ToString(); //same as above
        metropolisAmount.text = playerProperties[cameraFollow.delayedCurrentCharacter].metropolisOwned.ToString(); //same as above
        moneyAmount.text = gameManager.player[cameraFollow.delayedCurrentCharacter].money.ToString(); //same as above
        if (gameManager.player[cameraFollow.delayedCurrentCharacter].money > 9999)
            moneyAmount.fontSize = 10;
        else if (gameManager.player[cameraFollow.delayedCurrentCharacter].money > 99999)
            moneyAmount.fontSize = 8;
        else
            moneyAmount.fontSize = 12;
        playerX.text = "P " + (currentCharacter + 1) + " TURN "; //to be change to which Player currently in turn
		diceAmount.text = (testScript.totalDiceNum).ToString();
        currentPlayerRace = (int)gameManager.player[cameraFollow.delayedCurrentCharacter].race;
        
        switch (currentPlayerRace)
        {
            case 0: playerIndicatorImage.sprite = chineseCharacter.sprite;
                break;
            case 1: playerIndicatorImage.sprite = indianCharacter.sprite;
                break;
            case 2: playerIndicatorImage.sprite = malayCharacter.sprite;
                break;
        }
    }
	//calculate the mortgage money whether the mortgaged land is able to clear the debt
	public void MortgageMoneyCalculation (int amountNeeded, int mortgagePrice)
	{
		GameManager.instance.isMortgage = true;
		SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.moneySfx);
		//if the mortgage price higher than the amount owe, display debt clear and also returning remaining money msg
		if (amountNeeded < mortgagePrice)
		{
			GameManager.instance.moneyNotEnough = false;
			GameManager.instance.moneyCalculation (mortgagePrice, currentCharacter);
			GameManager.instance.player [currentCharacter].totalAssetAmount += GameManager.instance.player [currentCharacter].money;
			GameManager.instance.proceedToMortgage = false;
			GameManager.instance.mortgageCount = 0;
			//display the money cleared msg here
			moneyClearedBg.SetActive(true);
			moneyClearedMsgText.text = "You have cleared your debt! The remaining money has given back to you.";
			moneyClearedMsgText.color = GameManager.instance.greenColorSelection;
			GameManager.instance.isMortgage = false;
		}
		//if the mortgage price same as the amount owe, only display cleared msg
		else if (amountNeeded == mortgagePrice)
		{
			GameManager.instance.moneyNotEnough = false;
			GameManager.instance.moneyCalculation (mortgagePrice, currentCharacter);
			GameManager.instance.proceedToMortgage = false;
			GameManager.instance.mortgageCount = 0;
			//display the money cleared msg here
			moneyClearedBg.SetActive(true);
			moneyClearedMsgText.text = "You have cleared your debt!";
			moneyClearedMsgText.color = GameManager.instance.greenColorSelection;
			GameManager.instance.isMortgage = false;
		} 
		//if havent clear, continue proceed to mortgage list by not setting the proceedToMortgage to true
		else
		{
			GameManager.instance.moneyNotEnough = true;
			GameManager.instance.moneyCalculation (mortgagePrice, currentCharacter);
		}
		Debug.Log ("player" + (currentCharacter + 1) + "'s total asset amount: " + GameManager.instance.player [currentCharacter].totalAssetAmount);
	}
    //what does the function do will be added in the future
    public void SellFunction()
    {
		
        Debug.Log("Sell button pressed");
		playerSelectionBg.SetActive (true);
		GameManager.instance.isSell = true;
		GameManager.instance.isBuy = false;
		selectionInstructionText.text = "Select the BUYER:";
		SpawnPlayerSelectionButton ();
	}

	public void BuyFunction()
	{
        onClickSound.Play();
		Debug.Log("Buy button pressed");
		playerSelectionBg.SetActive (true);
		GameManager.instance.isBuy = true;
		GameManager.instance.isSell = false;
		selectionInstructionText.text = "Select the SELLER:";
		SpawnPlayerSelectionButton ();
	}
	public void CloseKLIAFunction ()
	{
        onClickSound.Play();
        kliaBg.SetActive (false);
		GameManager.instance.theCurrentPlayer.GetComponent<FollowPath> ().moveTheCamera ();
	}
	//spawn player selection button based on the number of player in game
	void SpawnPlayerSelectionButton()
	{
        onClickSound.Play();
        //deactivate all the buttons first before respawn
        for (int a = 0; a < GameManager.instance.maxPlayer - 1; a++)
		{
			playerButtonList [a].SetActive (false);
		}
		playerButtonIndex = 0;
		for (int i = 1; i < (GameManager.instance.maxPlayer + 1); i++)
		{
			if ((currentCharacter + 1) != i)
			{
				if (!GameManager.instance.player [i - 1].eliminated) 
				{
					if (GameManager.instance.player [currentCharacter].AI) 
					{
						AiComputer.instance.GetPlayerSelection (playerButtonList [playerButtonIndex], i - 1);
					}
					playerButtonList [playerButtonIndex].transform.Find ("PlayerNumText").GetComponent<Text> ().text = "Player " + i;
					playerButtonList [playerButtonIndex].SetActive (true);
					playerButtonIndex++;
				}
			} 
		}
	}
	//display mortgage list or player's property list based on the property owned by the player
	//the buttons will keep on reseting each time player display the list
	public void MortgageFunction (int currentPlayerIndex)
    {
        onClickSound.Play();
        int playerIndexToShowProperty;
		mortgageScrollBg.SetActive (true);	//set active the mortgage background
		//if the player not click on sell or buy button but able to proceed here, then it would be mortgage 
		if (!GameManager.instance.isSell && !GameManager.instance.isBuy) 
		{
			mortgageCloseButton.interactable = false;
			playerPropertyText.enabled = false;
			buySellInstructionText.enabled = false;
			moneyAmountText.enabled = true;
			mortgageInstructionText.enabled = true;
			moneyAmountText.text = initialMoneyDisplayText + " " + (-GameManager.instance.player [currentCharacter].money).ToString ();
			playerIndexToShowProperty = currentPlayerIndex;
		}
		//if the player clicked buy or sell, then will enable the buy sell instruction text and player property text
		else
		{
			mortgageCloseButton.interactable = true;
			moneyAmountText.enabled = false;
			mortgageInstructionText.enabled = false;
			playerPropertyText.enabled = true;
			buySellInstructionText.enabled = true;

			//the currentPlayerIndex here is refer to the selected player on player selection list
			if (GameManager.instance.isBuy) 
			{
				playerPropertyText.text = "Player " + (currentPlayerIndex + 1).ToString () + "'s Property";
				priceNegoBg.GetComponent<BuySellLand> ().buyerIndex = currentCharacter;
				priceNegoBg.GetComponent<BuySellLand> ().sellerIndex = currentPlayerIndex;
				playerIndexToShowProperty = currentPlayerIndex;
			} 
			else
			{
				playerPropertyText.text = "Player " + (currentCharacter + 1).ToString () + "'s Property";
				priceNegoBg.GetComponent<BuySellLand> ().buyerIndex = currentPlayerIndex;
				priceNegoBg.GetComponent<BuySellLand> ().sellerIndex = currentCharacter;
				playerIndexToShowProperty = currentCharacter;
			}
		}
		//inactivate all buttons first
		foreach (GameObject button in buttonList) 
		{
			button.SetActive (false);
		}
		int buttonCount = 0;
		//if there already has buttons under the scroll content, rename all the button text based on the property owned by the player
		if (buttonList.Count > 0) 
		{
			if (GameManager.instance.player [currentCharacter].AI) 
			{
				AiComputer.instance.GetPlayerLandNumber (GameManager.instance.player [playerIndexToShowProperty].property.Count);
			}
			foreach (GameObject property in GameManager.instance.player [playerIndexToShowProperty].property)
			{
				propertyData = property.GetComponent<LandData> ();
				buttonCount++;
				if (buttonCount > buttonList.Count)
				{
					buttonGO = (GameObject)Instantiate (buttonTemplate);
					EditButton (buttonGO);
				}
				else
				{
					EditButton (buttonList [buttonCount - 1]);
				}
			}
		}
		if (GameManager.instance.player [currentCharacter].AI) 
		{
			AiComputer.instance.FinishGetPlayerProperty ();
		}
    }

	public void ProceedToPropertyList(int playerIndex)
	{
        onClickSound.Play();
        ClosePlayerSelection ();
		MortgageFunction (playerIndex);
	}
	public void EditButton (GameObject buttonObj)
	{
        onClickSound.Play();
        buttonObj.SetActive (true);
		landNameText = buttonObj.transform.Find ("LandNameText").GetComponent<Text> ();
		mortgagePriceText = buttonObj.transform.Find ("MortgagePriceText").GetComponent<Text> ();
		landNameText.text = propertyData.landName;
		switch (propertyData.landCurrentStatus)
		{
		case global::LandData.landStatus.Land:
			if (!GameManager.instance.isSell && !GameManager.instance.isBuy) 
			{
				mortgagePriceText.text = propertyData.landMortgagePrice.ToString ();
			} 
			else
			{
				mortgagePriceText.text = propertyData.landCurrentStatus.ToString ();
			}
			break;
		case global::LandData.landStatus.Kampung:
			if (!GameManager.instance.isSell && !GameManager.instance.isBuy) 
			{
				mortgagePriceText.text = propertyData.villageMortgagePrice.ToString ();			} 
			else
			{
				mortgagePriceText.text = propertyData.landCurrentStatus.ToString ();
			}
			break;
		case global::LandData.landStatus.City:
			if (!GameManager.instance.isSell && !GameManager.instance.isBuy) 
			{
				mortgagePriceText.text = propertyData.cityMortgagePrice.ToString ();			} 
			else
			{
				mortgagePriceText.text = propertyData.landCurrentStatus.ToString ();
			}
			break;
		case global::LandData.landStatus.Metropolis:
			if (!GameManager.instance.isSell && !GameManager.instance.isBuy) 
			{
				mortgagePriceText.text = propertyData.metropolisMortgagePrice.ToString ();			} 
			else
			{
				mortgagePriceText.text = propertyData.landCurrentStatus.ToString ();
			}
			break;
		}
		if (GameManager.instance.player [currentCharacter].AI) 
		{
			AiComputer.instance.GetPlayerLandSelection (buttonObj,propertyData);
		}
		buttonObj.transform.parent = scrollViewContent.transform;
	}
    void ReturnCenterFunction()
    {
        onClickSound.Play();
        cameraFollow.enabled = true;
		cameraFollow.camReturn (true);
    }

    void SettingFunction()
    {
        onClickSound.Play();
        settingPopup.SetActive(true);
    }

    public void BuildLand(int currentPlayer)
    {

        playerProperties[currentPlayer].landOwned += 1; 
    }
    public void BuildKampung(int currentPlayer)
    {
        playerProperties[currentPlayer].landOwned -= 1;
        playerProperties[currentPlayer].kampungOwned += 1;
    }
    public void BuildCity(int currentPlayer)
    {
        playerProperties[currentPlayer].kampungOwned -= 1;
        playerProperties[currentPlayer].cityOwned += 1;
    }

    public void BuildMetropolis(int currentPlayer)
    {
        playerProperties[currentPlayer].cityOwned -= 1;
        playerProperties[currentPlayer].metropolisOwned += 1;
		GameManager.instance.player [currentPlayer].numOfMetropolisOwned++;
    }
	public void ClosePlayerSelection ()
	{
		
		if (!GameManager.instance.isProceedToPropertyList)
		{
			GameManager.instance.isSell = false;
			GameManager.instance.isBuy = false;
		}
		playerSelectionBg.SetActive (false);
	}
	public void CloseMortgageList ()
	{
        onClickSound.Play();
        GameManager.instance.isSell = false;
		GameManager.instance.isBuy = false;
		GameManager.instance.isProceedToPropertyList = false;
		mortgageScrollBg.SetActive (false);
	}
    void CloseSettingButtonOnClick()
    {
        onClickSound.Play();
        settingPopup.SetActive(false);
    }
}