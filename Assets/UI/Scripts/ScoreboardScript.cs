﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreboardScript : MonoBehaviour {

    public Button backToMainMenu;
    public GameObject preloadedAssets;
    AudioSource onClickSound;
	// Use this for initialization
	void Start () {
        backToMainMenu = GameObject.Find("BackToMenu").GetComponent<Button>();
        backToMainMenu.onClick.AddListener(BackButtonClick);
        preloadedAssets = GameObject.Find("preloaded_assets");
        onClickSound = preloadedAssets.transform.Find("onclickSound").GetComponent<AudioSource>();

        
	}
	
    void BackButtonClick()
    {
        onClickSound.Play();
        GameManager.instance.BackMain();
    }
}
