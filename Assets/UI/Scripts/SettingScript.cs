﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingScript : MonoBehaviour {
    public Button exitGameButton, quitToMainMenuButton;
    public GameObject exitGamePrompt, exitMenuPrompt, preloadedAssets;
    public Button exitGameYes, exitGameNo, exitMenuYes, exitMenuNo;
    AudioSource onClickSound;
    public Slider soundLevelSlider;
	// Use this for initialization
	void Start () {
        exitGameButton = GameObject.Find("ExitGameButton").GetComponent<Button>();
        exitGameButton.onClick.AddListener(ExitGameButtonOnClick);

        quitToMainMenuButton = GameObject.Find("ExitMenuButton").GetComponent<Button>();
        quitToMainMenuButton.onClick.AddListener(QuitToMainMenuButtonOnClick);

        soundLevelSlider = GameObject.Find("SoundSlider").GetComponent<Slider>();
        soundLevelSlider.onValueChanged.AddListener(delegate { SoundLevelSlider(); });

        exitMenuPrompt = GameObject.Find("ExitToMenuPrompt");
        exitMenuYes = exitMenuPrompt.transform.Find("YesButton").GetComponent<Button>();
        exitMenuYes.onClick.AddListener(ExitMenuYes);
        exitMenuNo = exitMenuPrompt.transform.Find("NoButton").GetComponent<Button>();
        exitMenuNo.onClick.AddListener(ExitMenuNo);

        exitGamePrompt = GameObject.Find("ExitGamePrompt");
        exitGameYes = exitGamePrompt.transform.Find("YesButton").GetComponent<Button>();
        exitGameYes.onClick.AddListener(ExitGameYes);
        exitGameNo = exitGamePrompt.transform.Find("NoButton").GetComponent<Button>();
        exitGameNo.onClick.AddListener(ExitGameNo);
            
        exitGamePrompt.SetActive(false);
        exitMenuPrompt.SetActive(false);

        preloadedAssets = GameObject.Find("preloaded_assets");
        onClickSound = preloadedAssets.transform.Find("onclickSound").GetComponent<AudioSource>();
	}

    void ExitGameButtonOnClick()
    {
        onClickSound.Play();
        exitGamePrompt.SetActive(true);
    }

    void QuitToMainMenuButtonOnClick()
    {
        onClickSound.Play();
        exitMenuPrompt.SetActive(true);
    }

   

    void SoundLevelSlider()
    {
        AudioListener.volume = soundLevelSlider.value;
    }

    void ExitGameYes()
    {
        onClickSound.Play();
        Application.Quit();
    }

    void ExitGameNo()
    {
        onClickSound.Play();
        exitGamePrompt.SetActive(false);
    }

    void ExitMenuYes()
    {
        onClickSound.Play();
        GameManager.instance.BackMain();
    }

    void ExitMenuNo()
    {
        onClickSound.Play();
        exitMenuPrompt.SetActive(false);
    }
}
