﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuSettingScript : MonoBehaviour {

	// Use this for initialization
    public Slider soundLevelSlider;
    void Start () {

        //soundLevelSlider = GameObject.Find("MainMenuSetting").transform.Find("backgroundImage").transform.Find("SoundDescription").transform.Find("SoundSlider").GetComponent<Slider>();
        soundLevelSlider.onValueChanged.AddListener(delegate { SoundLevelSlider(); });
        
    }
	
    

    void SoundLevelSlider()
    {
        AudioListener.volume = soundLevelSlider.value;
    }
}
