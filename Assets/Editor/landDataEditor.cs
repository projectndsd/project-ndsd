﻿using UnityEngine;
using UnityEditor;

[CustomEditor (typeof (LandData)), CanEditMultipleObjects]
public class LandDataEditor : Editor
{
	public SerializedProperty type_Prop;

	void OnEnable ()
	{
		type_Prop = serializedObject.FindProperty ("type");
	}

	public override void OnInspectorGUI ()
	{
		serializedObject.Update ();
		EditorGUILayout.PropertyField (type_Prop);
		EditorGUI.indentLevel += 1;

		LandData.landType type = (LandData.landType)type_Prop.enumValueIndex;

		switch (type)
		{
		case LandData.landType.NormalLand:
			SerializedProperty landName_Prop = serializedObject.FindProperty ("landName");
			SerializedProperty grade_Prop = serializedObject.FindProperty ("grade");
			SerializedProperty owner_Prop = serializedObject.FindProperty ("Owner");
			SerializedProperty landPrice_Prop = serializedObject.FindProperty ("landPrice");
			SerializedProperty kampungPrice_Prop = serializedObject.FindProperty ("kampungPrice");
			SerializedProperty cityPrice_Prop = serializedObject.FindProperty ("cityPrice");
			SerializedProperty metropolisPrice_Prop = serializedObject.FindProperty ("metropolisPrice");
			SerializedProperty landRental_Prop = serializedObject.FindProperty ("landRental");
			SerializedProperty kampungRental_Prop = serializedObject.FindProperty ("kampungRental");
			SerializedProperty cityRental_Prop = serializedObject.FindProperty ("cityRental");
			SerializedProperty metropolisRental_Prop = serializedObject.FindProperty ("metropolisRental");
			SerializedProperty landCurrentStatus_Prop = serializedObject.FindProperty ("landCurrentStatus");
			SerializedProperty landStatusBeforeMortgage_Prop = serializedObject.FindProperty ("landStatusBeforeMortgage");
			SerializedProperty landMortgagePrice_Prop = serializedObject.FindProperty ("landMortgagePrice");
			SerializedProperty villageMortgagePrice_Prop = serializedObject.FindProperty ("villageMortgagePrice");
			SerializedProperty cityMortgagePrice_Prop = serializedObject.FindProperty ("cityMortgagePrice");
			SerializedProperty metropolisMortgagePrice_Prop = serializedObject.FindProperty ("metropolisMortgagePrice");
			SerializedProperty landRedeemPrice_Prop = serializedObject.FindProperty ("landRedeemPrice");
			SerializedProperty villageRedeemPrice_Prop = serializedObject.FindProperty ("villageRedeemPrice");
			SerializedProperty cityRedeemPrice_Prop = serializedObject.FindProperty ("cityRedeemPrice");
			SerializedProperty metropolisRedeemPrice_Prop = serializedObject.FindProperty ("metropolisRedeemPrice");

			EditorGUILayout.PropertyField (landName_Prop);
			EditorGUILayout.PropertyField (grade_Prop);
			EditorGUILayout.PropertyField (owner_Prop);
			EditorGUILayout.PropertyField (landCurrentStatus_Prop);
			EditorGUILayout.PropertyField (landStatusBeforeMortgage_Prop);
			EditorGUILayout.PropertyField (landPrice_Prop);
			EditorGUILayout.PropertyField (kampungPrice_Prop);
			EditorGUILayout.PropertyField (cityPrice_Prop);
			EditorGUILayout.PropertyField (metropolisPrice_Prop);
			EditorGUILayout.PropertyField (landRental_Prop);
			EditorGUILayout.PropertyField (kampungRental_Prop);
			EditorGUILayout.PropertyField (cityRental_Prop);
			EditorGUILayout.PropertyField (metropolisRental_Prop);
			EditorGUILayout.PropertyField (landMortgagePrice_Prop);
			EditorGUILayout.PropertyField (villageMortgagePrice_Prop);
			EditorGUILayout.PropertyField (cityMortgagePrice_Prop);
			EditorGUILayout.PropertyField (metropolisMortgagePrice_Prop);
			EditorGUILayout.PropertyField (landRedeemPrice_Prop);
			EditorGUILayout.PropertyField (villageRedeemPrice_Prop);
			EditorGUILayout.PropertyField (cityRedeemPrice_Prop);
			EditorGUILayout.PropertyField (metropolisRedeemPrice_Prop);
			break;
		case LandData.landType.ThemePark:
			SerializedProperty stopRoundThemePark_Prop = serializedObject.FindProperty ("stopRound");

			EditorGUILayout.PropertyField (stopRoundThemePark_Prop);
			break;
		}

		EditorGUI.indentLevel -= 1;
		serializedObject.ApplyModifiedProperties ();
	}
}
