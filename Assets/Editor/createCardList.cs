﻿using System.Collections;
using UnityEditor;
using UnityEngine;

public class createCardList
{
	[MenuItem("Assets/Create/New Card List")]
	public static CollectGameData createCardData ()
	{
		//create new card list in data files
		CollectGameData data = ScriptableObject.CreateInstance<CollectGameData> ();
		AssetDatabase.CreateAsset (data, "Assets/Data Files/list.asset");
		AssetDatabase.SaveAssets ();
		return data;
	}
}