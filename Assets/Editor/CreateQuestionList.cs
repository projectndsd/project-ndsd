﻿using System.Collections;
using UnityEditor;
using UnityEngine;

public class CreateQuestionList
{
	[MenuItem("Assets/Create/New Question List")]
	public static CollectQNAData createCardData ()
	{
		//create new card list in data files
		CollectQNAData data = ScriptableObject.CreateInstance<CollectQNAData> ();
		AssetDatabase.CreateAsset (data, "Assets/Data Files/list.asset");
		AssetDatabase.SaveAssets ();
		return data;
	}
}