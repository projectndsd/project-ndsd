﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectQNAData : ScriptableObject
{
	public List<QNA> QNAList;
}