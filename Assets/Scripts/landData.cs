﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LandData : MonoBehaviour
{
    public enum owner
    {
        Default,
        Player1,
        Player2,
        Player3,
        Player4
    }
    public enum landType
    {
        NormalLand,
        ChanceLand,
        Jail,
        Genting,
        ThemePark,
		StartingPoint,
		Ferry1,
		Ferry2,
		KLIA
    }
	public enum landGrade
	{
		Grade_1,
		Grade_2,
		Grade_3,
		Grade_4,
		Grade_5,
		Grade_6,
		Grade_7,
		Grade_8,
		Grade_9,
		Grade_10,
		Grade_11
	}
	public enum landStatus
	{
		EmptyLand,
		Land,
		Kampung,
		City,
		Metropolis,
		Mortgaged
	}
	[Header ("Setting")]
	public landType type;
	public int stopRound;
	public string landName;
	public landGrade grade;

    public int landPrice, kampungPrice, cityPrice, metropolisPrice;
	public int landRental, kampungRental, cityRental, metropolisRental;
	public int landMortgagePrice, villageMortgagePrice, cityMortgagePrice, metropolisMortgagePrice;
	public int landRedeemPrice, villageRedeemPrice, cityRedeemPrice, metropolisRedeemPrice;
	public landStatus landCurrentStatus, landStatusBeforeMortgage;
    public owner Owner;

	void Awake ()
	{
		//initialize the data of the land
		landCurrentStatus = landStatus.EmptyLand;
		Owner = owner.Default;
		SetName ();

		//set the base land price
		SetBasePrice ();
		priceCalculation ();
		rentalCalculation ();
	}
	public void SetName ()
	{
		switch (type)
		{
		case landType.Genting:
			this.gameObject.name = "Genting Highland";
			break;
		case landType.ThemePark:
			this.gameObject.name = "Themepark";
			break;
		case landType.NormalLand:
			this.gameObject.name = landName;
			break;
		}
	}
	public void SetBasePrice ()
	{
		switch (grade)
		{
		case landGrade.Grade_1:
			landPrice = 500;	
			break;
		case landGrade.Grade_2:
			landPrice = 1000;	
			break;
		case landGrade.Grade_3:
			landPrice = 1500;
			break;
		case landGrade.Grade_4:
			landPrice = 2000;
			break;
		case landGrade.Grade_5:
			landPrice = 2500;
			break;
		case landGrade.Grade_6:
			landPrice = 3000;
			break;
		case landGrade.Grade_7:
			landPrice = 3500;
			break;
		case landGrade.Grade_8:
			landPrice = 4000;
			break;
		case landGrade.Grade_9:
			landPrice = 4500;
			break;
		case landGrade.Grade_10:
			landPrice = 5000;
			break;
		case landGrade.Grade_11:
			landPrice = 5500;
			break;
		}
	}
	//use land price to do other calculation
	public void priceCalculation ()
	{
		kampungPrice = landPrice + 300;
		cityPrice = kampungPrice + 300 * 2;
		metropolisPrice = cityPrice + 300 * 3;
		MortgageAndRedeemCalculation (kampungPrice, cityPrice, metropolisPrice);
	}
	//calculate rental based on property price (land, kampung, city, metropolis)
	public void rentalCalculation ()
	{
		landRental = (int)((float)landPrice * 0.1f);
		kampungRental = (int)((float)(landPrice + kampungPrice) * 0.2f);
		cityRental = (int)((float)(landPrice + kampungPrice + cityPrice) * 0.3f);
		metropolisRental = (int)((float)(landPrice + kampungPrice + cityPrice + metropolisPrice) * 0.4f);
	}

	public void MortgageAndRedeemCalculation (int passKampungPrice, int passCityPrice, int passMetropolisPrice)
	{
		landMortgagePrice = landPrice;
		villageMortgagePrice = landMortgagePrice + (passKampungPrice / 2);
		cityMortgagePrice = villageMortgagePrice + (passCityPrice / 2);
		metropolisMortgagePrice = cityMortgagePrice + (passMetropolisPrice / 2);
		landRedeemPrice = (int)((float)landMortgagePrice * 1.1f);
		villageRedeemPrice = (int)((float)villageMortgagePrice * 1.1f);
		cityRedeemPrice = (int)((float)cityMortgagePrice * 1.1f);
		metropolisRedeemPrice = (int)((float)metropolisMortgagePrice * 1.1f);
	}
}