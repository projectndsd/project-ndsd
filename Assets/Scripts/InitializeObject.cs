﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializeObject : MonoBehaviour {

	public GameObject gameManager;

	void Awake()
	{
		if (GameManager.instance == null) 
		{
			 GameObject manager = Instantiate (gameManager);
			manager.gameObject.name = "Game Manager";
		}

	}
}
