﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour
{
	public GameObject[] playerListText;

	public List<GameManager.Player> scoreList;
	private int playerIndex;
	private int highestScore;
	private string resultDisplay;
	private int numForLastEliminatedPlayer;
	private bool isDisplayed;
	private List<int> displayedPlayerIndexList;

	// Use this for initialization
	void Start ()
	{	
		GameManager.instance.gameOver = true;
		scoreList = new List<GameManager.Player> ();
		displayedPlayerIndexList = new List<int> ();
		playerIndex = -1;
		numForLastEliminatedPlayer = GameManager.instance.playerEliminationList.Count;

		for (int j = 0; j < GameManager.instance.maxPlayer; j++) 
		{
			scoreList.Add (GameManager.instance.player [j]);
		}
		//display total assets and money owned by each player from highest to lowest
		for (int i = 0; i < GameManager.instance.maxPlayer; i++)
		{
			highestScore = 0;
			if (GameManager.instance.gameMode == GameManager.GameModeChoices.TurnLimit) 
			{
				ArrangeScoreByAssetAmount ();
				resultDisplay = GameManager.instance.player [playerIndex].totalAssetAmount.ToString ();
			} 
			else if (GameManager.instance.gameMode == GameManager.GameModeChoices.Metropolis)
			{
				ArrangeScoreByMetropolisOwned ();
				resultDisplay = GameManager.instance.player [playerIndex].numOfMetropolisOwned.ToString ();
			} 
			else if (GameManager.instance.gameMode == GameManager.GameModeChoices.Bankrupt)
			{
				BankruptScoreArrangement ();
			}
			playerListText [i].SetActive (true);
			displayedPlayerIndexList.Add (playerIndex);	//add the displayed player into the list for future checking
			playerListText [i].transform.Find ("PlayerText").GetComponent<Text> ().text = "Player " + (playerIndex + 1).ToString ();
			playerListText [i].transform.Find ("ResultText").GetComponent<Text> ().text = resultDisplay;
		}
	}
	//arrange the scoreboard based on the total asset owned
	public void ArrangeScoreByAssetAmount ()
	{
		for (int k = 0; k < scoreList.Count; k++)
		{
			isDisplayed = false;
			for (int j = 0; j < displayedPlayerIndexList.Count; j++) 
			{
				if (k == displayedPlayerIndexList [j])
				{
					isDisplayed = true;
					break;
				}
			}
			if (!isDisplayed)
			{
				if ((scoreList [k].totalAssetAmount > highestScore))
				{
					highestScore = scoreList [k].totalAssetAmount;
					playerIndex = k;
				}
			}
		}
	}
	//arrange the scoreboard based on the number of metropolis owned
	public void ArrangeScoreByMetropolisOwned ()
	{
		for (int k = 0; k < scoreList.Count; k++)
		{
			isDisplayed = false;
			for (int j = 0; j < displayedPlayerIndexList.Count; j++) 
			{
				if (k == displayedPlayerIndexList [j])
				{
					isDisplayed = true;
					break;
				}
			}
			if (!isDisplayed)
			{
				if ((scoreList [k].numOfMetropolisOwned > highestScore))
				{
					highestScore = scoreList [k].numOfMetropolisOwned;
					playerIndex = k;
				}
			}
		}
	}
	public void BankruptScoreArrangement ()
	{
		Debug.Log ("num for last eliminated player: " + numForLastEliminatedPlayer.ToString());
		if (numForLastEliminatedPlayer == GameManager.instance.playerEliminationList.Count) 
		{
			playerIndex = GameManager.instance.lastPlayerInGame;
			resultDisplay = "Winner!!!";
		}
		else
		{
			playerIndex = GameManager.instance.playerEliminationList [numForLastEliminatedPlayer].playerIndex;
			resultDisplay = GameManager.instance.playerEliminationList [playerIndex].totalTurn.ToString ();
		}
		Debug.Log ("player index: " + (playerIndex + 1).ToString());
		numForLastEliminatedPlayer -= 1;
	}
}