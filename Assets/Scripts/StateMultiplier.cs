﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class State
{
	public string stateName;
	public bool multiplierActivated;
	public List<GameObject> landList;
}
public class StateMultiplier : MonoBehaviour 
{
	public List<State> landClassification;
}