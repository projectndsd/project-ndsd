﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public enum GameModeChoices
	{
		TurnLimit,
		Bankrupt,
		Metropolis
	}
	public int steps;
	public int maxPlayer = 2; //default player is 2
	public int initialMoney = 0;
	public Color greenColorSelection;

	public enum Race 
	{
		Chinese, 
		Indian,
		Malay
	}
	public Race playerRace;

	[Header("KLIA Setting")]
	public int kliaWestFeesPerLand;
	public int kliaEastFeesPerLand;
	public float characterMoveToSelectedLandTIme;
	public AnimationCurve animCurve;

	[Header("Traits Setting")]
	public float malayBuyLandRate;
	public float chineseBuyLandRate;
	public float chineseRentalRate;
	public int indianExtraBonusForEachMamakLand;
	public List<string> indianBonusLandState;
	public List<GameObject> landmarkList;

	//public int playerOweAmount;
    public PassValueToGameManager passValueToGameManager;
	public static GameManager instance;
	public int moneyForEachCompletedRound;
	public float particleDuration;
	public RuntimeAnimatorController[] animatorOverride;
	public int lastPlayerNum = 4;
	public int playerOrderNumber;

	public struct Player
	{
		public GameObject character;
		public List<GameObject> property;
		public List<GameObject> mortgagedProperty;
		public int totalSteps;
		public int completeRound;
		public int money;
		public Race race;
		public int turn;
		public int totalAssetAmount;
		public int numOfMetropolisOwned;
		public int escapePass;
		public bool isInJail;
		public bool AI;
		public bool eliminated;
		public bool isActive;
	}
	public struct PlayerEliminationInfo
	{
		public int playerIndex;
		public int totalTurn;
	}
	public Player[] player;
	public List<PlayerEliminationInfo> playerEliminationList;
	public PlayerEliminationInfo eliminateInfo;

	public GameModeChoices gameMode;

	[Header("Turn Limit Mode Setting")]
	public int numberOfTurn;

	[Header("Metropolis Mode Setting")]
	public int numberOfMetropolis;

	private string[] splitedName;
	private int ownerNum;
	private int numberOfPlayerInGame;

	[Header("Land Status Sprite List")]
	public Sprite[] buildingList;

	[Header("Player Indicator List")]
	public Sprite[] indicatorList;

	[Header("Internal Setting")]
	public StateMultiplier stateMultiplerScript;	//buy land need to access
	public int lastPlayerInGame;
	public GameObject theCurrentPlayer;	//for klia system (move character to selected land)
	public string theSelectedLandName;
	public int kliaFees;
	public int mortgageCount;
	public GameObject pathObjectGO;
	public GameObject map3DGO;	//3D map game object
	public bool buildLandmark;
	public GameObject particleSystemForLandmark;
	public bool moveToLandmark;
	public bool proceedToMortgage;
	public bool moneyNotEnough;
	public bool isMortgage;
	public bool isBuy;
	public bool isSell;
	public bool isProceedToPropertyList;
	public bool useEscapeCard;
	public bool isDeducted;
	public bool isRedeem;
	public bool gameOver;
	public List<GameObject> mapPieceList  = new List<GameObject> ();
	public List<GameObject> normalLandList = new List<GameObject> ();

	private Transform[] mapPieceArray;
	private Transform[] landmarkArray;
	private GameObject landmarkParentGO;
	private GameObject displayBankruptOrMortgageMsgBG;
	private GameObject kliaBg;
	private TestScript testScript;

    AudioSource onClickSound;
    // Use this for initialization
    void Awake () 
	{
		mortgageCount = 0;
		if (instance == null) 
		{
			instance = this;
		} 
		else if (instance != this) 
		{
			Destroy (gameObject);
		}
		DontDestroyOnLoad (this.gameObject);
		player = new Player[4];
		playerEliminationList = new List<PlayerEliminationInfo> ();
		eliminateInfo.playerIndex = 0;
		eliminateInfo.totalTurn = 0;
	}

	public void onStartButton()
	{

        onClickSound = GameObject.Find("preloaded_assets").transform.Find("onclickSound").GetComponent<AudioSource>();
        onClickSound.Play();
        UnityEngine.SceneManagement.SceneManager.LoadScene ("2.ChooseRace");
	}

	public void onCreditButton()
	{
        onClickSound = GameObject.Find("preloaded_assets").transform.Find("onclickSound").GetComponent<AudioSource>();
        onClickSound.Play();
        Debug.Log ("Credit Button Clicked");
	}

	public void onSettingButton()
	{
		Debug.Log ("Setting Button Clicked");
	}
	public void startGameButton()
	{
        //passValueToGameManager = GameObject.Find("PlayerAmount").GetComponent<PassValueToGameManager>(); //Pass the value from the input box

		UnityEngine.SceneManagement.SceneManager.LoadScene ("3.InGame");
		StartCoroutine(LoadGame ());

	}
	public void DeactivateKLIABg ()
	{
		SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.moneySfx);
		TotalAssetAmountCalculation (-kliaFees, int.Parse (theCurrentPlayer.name.Substring (theCurrentPlayer.name.Length - 1)) - 1);
		moneyCalculation (-kliaFees, int.Parse (theCurrentPlayer.name.Substring (theCurrentPlayer.name.Length - 1)) - 1);
		SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.kliaSfx);
		StartCoroutine (MoveCharToSelectedLand (theSelectedLandName));
		kliaBg.SetActive (false);
	}
	//move character to selected land (KLIA)
	public IEnumerator MoveCharToSelectedLand (string selectedLandName)
	{
		int indexOfLand = testScript.path_objs.IndexOf (pathObjectGO.transform.Find (selectedLandName).transform); //get the index of the selected land to know the current way id
		Vector3 currentPos = theCurrentPlayer.transform.position;
		Vector3 nextPos = pathObjectGO.transform.Find (selectedLandName).transform.position;
		float elapsedTime = 0f;
		float fracPath = 0f;
		Debug.Log ("Distance: " + Vector3.Distance (theCurrentPlayer.transform.position, nextPos));
		while (Vector3.Distance (theCurrentPlayer.transform.position, nextPos) > 0f)
		{
			yield return new WaitForEndOfFrame ();
			elapsedTime += Time.deltaTime;
			Debug.Log ("here 1");
			fracPath = elapsedTime / characterMoveToSelectedLandTIme;
			theCurrentPlayer.transform.position = Vector3.Lerp (currentPos, nextPos, animCurve.Evaluate (fracPath));	
		}
		if (GameManager.instance.player [AiComputer.instance.curPlayer].AI) 
		{
			while (theCurrentPlayer.transform.position != nextPos) 
			{
				yield return new WaitForSeconds (0.05f);
			}
			theCurrentPlayer.GetComponent<FollowPath> ().LandChecking (indexOfLand);
		}
		else
		theCurrentPlayer.GetComponent<FollowPath> ().LandChecking (indexOfLand);
	}
	public void moneyCalculation (int amount, int playerArrayIndex)
	{
		player [playerArrayIndex].money += amount;
		Debug.Log ("player " + (playerArrayIndex + 1).ToString() + " money: " + player [playerArrayIndex].money);
	}
	public void TotalAssetAmountCalculation (int amount, int playerArrayIndex)
	{
		if (!isMortgage)
		{
			if (player [playerArrayIndex].money < -amount)
			{
				player [playerArrayIndex].totalAssetAmount -= player [playerArrayIndex].money;
			} 
			else
			{
				player [playerArrayIndex].totalAssetAmount += amount;
			}
		}
		Debug.Log ("player" + (playerArrayIndex + 1) + "'s total asset amount: " + player [playerArrayIndex].totalAssetAmount);
	}
	public void MortgageChecking (int playerArrayIndex)
	{
		//checking for player that still have property but don have money
		if ((player [playerArrayIndex].money < 0) && (player [playerArrayIndex].property.Count > 0)) 
		{
			displayBankruptOrMortgageMsgBG.SetActive (true);
			displayBankruptOrMortgageMsgBG.transform.Find ("ChanceCardMessage").GetComponent<Text>().text = "Player " + (playerArrayIndex + 1).ToString() + " does not have enough money... Click Ok proceed to Mortgage List.";
			displayBankruptOrMortgageMsgBG.transform.Find ("ChanceCardMessage").GetComponent<Text> ().color = Color.red;
			proceedToMortgage = true;
		}
	}
	//start the coroutine for upgrade effect in this function
	public void upgradeProcess (GameObject effectSystem, BuyLandScript theScript)
	{
		StartCoroutine (UpgradeEffect (effectSystem, theScript));
	}
	//coroutine for upgrade land effect
	public IEnumerator UpgradeEffect (GameObject effectSystem, BuyLandScript theScript)
	{
		effectSystem.SetActive (true);
		yield return new WaitForSeconds (particleDuration - 1);
		effectSystem.SetActive (false);
		if (!buildLandmark) 
		{
			theScript.LandChecking ();
		} 
		else
		{
			theScript.LandmarkChecking ();
		}
		yield return new WaitForSeconds (1f);
		theScript.openNotificationMessage ();
	}
	public void addTurn (int playerArrayIndex)
	{
		player [playerArrayIndex].turn += 1;
        if (numberOfTurn == 0)
        {
            GameObject.Find("Card Manager").GetComponent<RandomChance>().turnText.text = player[playerArrayIndex].turn.ToString();
        }
        else
        {
            GameObject.Find("Card Manager").GetComponent<RandomChance>().turnText.text = player[playerArrayIndex].turn.ToString() + "/" + numberOfTurn.ToString();
        }
		
		//Debug.Log ("player" + (playerArrayIndex + 1) + "'s turn: " + player [playerArrayIndex].turn);
	}
	public void TurnLimitModeChecking ()
	{	//check the last player's turn if equal to preset number of turn limit
		if (player [maxPlayer - 1].turn == numberOfTurn) 
		{	
			UnityEngine.SceneManagement.SceneManager.LoadScene ("4.ScoreBoard");
		}
	}
	public void MetropolisModeChecking (int playerIndex)
	{
		if (player [playerIndex].numOfMetropolisOwned == numberOfMetropolis)
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene ("4.ScoreBoard");
		}
	}
	public void CheckingForBankruptAndElimination (int playerIndex, GameObject currentPlayer)
	{	//first will run into here to check whether the player is bankrupted
		if ((player [playerIndex].totalAssetAmount <= 0) && (!player [playerIndex].eliminated))
		{
			SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.sadSfx);
			player [playerIndex].money = 0;
			player [playerIndex].totalAssetAmount = 0;
			BankruptFunction (playerIndex);
		}
		//then if bankrupt, bankrupt msg will be displayed, and when closed it, will jump to here to make the player invisible
		else
		{
			GameManager.instance.MortgageChecking (playerIndex);
			if (!proceedToMortgage)
			{
				currentPlayer.GetComponent<FollowPath> ().moveTheCamera ();   //move the camera to next Player after the chance card is closed
				if (player [playerIndex].eliminated)
				{
					player [playerIndex].character.SetActive (false);
				}
			}
		}
	}
	public void BankruptFunction (int playerIndex)
	{
		int nextPlayerIndex = playerIndex + 1;

		//change the next target of previous player (before eliminated player)
		if (playerIndex == 0) 
		{
			if (!player [maxPlayer - 1].eliminated && !player [nextPlayerIndex].eliminated)
			{
				player [maxPlayer - 1].character.GetComponent<FollowPath> ().nextTarget = player [playerIndex].character.GetComponent<FollowPath> ().nextTarget;
			} 
			else if (player [maxPlayer - 1].eliminated && !player [nextPlayerIndex].eliminated)
			{
				player [maxPlayer - 2].character.GetComponent<FollowPath> ().nextTarget = player [playerIndex].character.GetComponent<FollowPath> ().nextTarget;
			} 
			else if (!player [maxPlayer - 1].eliminated && player [nextPlayerIndex].eliminated)
			{
				player [maxPlayer - 1].character.GetComponent<FollowPath> ().nextTarget = player [nextPlayerIndex].character.GetComponent<FollowPath> ().nextTarget;
			}
		} 
		else
		{
			if (nextPlayerIndex == maxPlayer)
			{
				nextPlayerIndex = 0;
			}
			if (!player [playerIndex - 1].eliminated && !player [nextPlayerIndex].eliminated)
			{
				player [playerIndex - 1].character.GetComponent<FollowPath> ().nextTarget = player [playerIndex].character.GetComponent<FollowPath> ().nextTarget;
			} 
			else if (player [playerIndex - 1].eliminated && !player [nextPlayerIndex].eliminated)
			{
				if (playerIndex < 2)
				{
					player [maxPlayer - 1].character.GetComponent<FollowPath> ().nextTarget = player [playerIndex].character.GetComponent<FollowPath> ().nextTarget;
				} else
				{
					player [playerIndex - 2].character.GetComponent<FollowPath> ().nextTarget = player [playerIndex].character.GetComponent<FollowPath> ().nextTarget;
				}
			} 
			else if (!player [playerIndex - 1].eliminated && player [nextPlayerIndex].eliminated)
			{
				player [playerIndex - 1].character.GetComponent<FollowPath> ().nextTarget = player [nextPlayerIndex].character.GetComponent<FollowPath> ().nextTarget;
			}
		}

		//display the message
		displayBankruptOrMortgageMsgBG.transform.Find ("ChanceCardMessage").GetComponent<Text>().text = "Player " + (playerIndex + 1).ToString() + " is bankrupted! Eliminate from game!";
		player [playerIndex].character.transform.Find ("CameraTarget").gameObject.transform.parent = null;
		player [playerIndex].eliminated = true;

		//add the eliminated player into the list for bankrupt mode score display
		eliminateInfo.playerIndex = playerIndex;
		eliminateInfo.totalTurn = player [playerIndex].turn;
		playerEliminationList.Add (eliminateInfo);

		displayBankruptOrMortgageMsgBG.SetActive (true);	//activate the chance card which is used to display bankrupt message
		numberOfPlayerInGame--;
	}
	public void playerSurviveChecking ()
	{
		//if the players left in game is 1 or less than 1, check who is the winner
		if (numberOfPlayerInGame <= 1) 
		{
			for (int i = 0; i < player.Length; i++)
			{
				if (player [i].character.activeInHierarchy) 
				{
					lastPlayerInGame = int.Parse (player [i].character.name.Substring (player [i].character.name.Length - 1)) - 1;
					break;
				}
			}
			UnityEngine.SceneManagement.SceneManager.LoadScene ("4.ScoreBoard");
		}
	}
	//this function will call when whole land is going to sell/buy/mortgage/player buy land
	public void CalculateLandAsset (GameObject land, int playerIndex, bool isSeller)
	{
		LandData landData = land.GetComponent<LandData> ();
		int price = 0;
		int priceForEachUpgrade = 0;
		switch (landData.landCurrentStatus)
		{
		case LandData.landStatus.Land:
			price = landData.landPrice;
			priceForEachUpgrade = landData.landPrice;
			break;
		case LandData.landStatus.Kampung:
			price = landData.landPrice + landData.kampungPrice;
			priceForEachUpgrade = landData.kampungPrice;
			break;
		case LandData.landStatus.City:
			price = landData.landPrice + landData.kampungPrice + landData.cityPrice;
			priceForEachUpgrade = landData.cityPrice;
			break;
		case LandData.landStatus.Metropolis:
			price = landData.landPrice + landData.kampungPrice + landData.cityPrice + landData.metropolisPrice;
			priceForEachUpgrade = landData.metropolisPrice;
			break;
		}
		//if this function is called by the seller, then will deduct the total asset amount of seller as he/she sold the land
		if (GameManager.instance.isBuy || GameManager.instance.isSell || isMortgage)
		{
			if (isSeller)
			{
				player [playerIndex].totalAssetAmount -= price;
			} 
			else
			{
				player [playerIndex].totalAssetAmount += price;
			}
		} 
		//this is for mortgaged land redemption
		else if (isRedeem) 
		{
			player [playerIndex].totalAssetAmount += price;
			isRedeem = false;
		} 
		//this is for buy and upgrade land
		else
		{
			player [playerIndex].totalAssetAmount += priceForEachUpgrade;
		}
		Debug.Log ("player " + (playerIndex + 1).ToString() + "'s total asset amount: " + player [playerIndex].totalAssetAmount);
	}
	public void MultiplierChecking (string theLandName, int playerNum)
	{
		splitedName = theLandName.Split (' ');	//split the name for state checking
		foreach (State state in stateMultiplerScript.landClassification) 
		{	//if same state, check for land
			if (splitedName [splitedName.Length - 1] == state.stateName)
			{
				for (int i = 0; i < state.landList.Count; i++)
				{
					ownerNum = (int)state.landList [i].GetComponent<LandData> ().Owner;	//get the owner number to know which player is the owner
					if (ownerNum != playerNum || state.landList [i].GetComponent<LandData>().landCurrentStatus == LandData.landStatus.Mortgaged)
					{
						break;
					} 
					//if all the lands of the state is under same player, change the rental according to the multiplier bonus
					if (i == (state.landList.Count - 1))
					{
						for (int a = 0; a < state.landList.Count; a++) 
						{
							LandData landData = state.landList [a].GetComponent<LandData> ();
							landData.landRental = (int)((float)landData.landRental * (1.0f + (float)state.landList.Count * 0.1f));	//update the rental
							landData.kampungRental = (int)((float)landData.kampungRental * (1.0f + (float)state.landList.Count * 0.1f));
							landData.cityRental = (int)((float)landData.cityRental * (1.0f + (float)state.landList.Count * 0.1f));
							landData.metropolisRental = (int)((float)landData.metropolisRental * (1.0f + (float)state.landList.Count * 0.1f));
						}
						state.multiplierActivated = true;
					}
				}
				break;
			}
		}
	}
	//remove the multiplier bonus
	public void DeactivateMultiplier (string theLandName)
	{
		splitedName = theLandName.Split (' ');	//split the name for state checking
		foreach (State state in stateMultiplerScript.landClassification) 
		{	//if same state, check for land
			if (splitedName [splitedName.Length - 1] == state.stateName)
			{
				//if the multiplier bonus already activated, deactivate it
				if (state.multiplierActivated)
				{
					for (int a = 0; a < state.landList.Count; a++) 
					{
						LandData landData = state.landList [a].GetComponent<LandData> ();
						landData.landRental = (int)((float)landData.landRental / (1.0f + (float)state.landList.Count * 0.1f)) + 1;	//update the rental
						landData.kampungRental = (int)((float)landData.kampungRental / (1.0f + (float)state.landList.Count * 0.1f)) + 1;
						landData.cityRental = (int)((float)landData.cityRental / (1.0f + (float)state.landList.Count * 0.1f)) + 1;
						landData.metropolisRental = (int)((float)landData.metropolisRental / (1.0f + (float)state.landList.Count * 0.1f)) + 1;
					}
					state.multiplierActivated = false;
				}
				break;
			}
		}
	}
	public void Update()
	{
		//the following line is to debug the player property
		if(Input.GetKeyDown(KeyCode.A))
			{
				for (int i = 0; i < maxPlayer; i++) 
				{
					for(int j = 0; j < player[i].property.Count; j++)
					{
						Debug.Log (player [i].property [j].gameObject.name);
						Debug.Log (player [i].property [j].GetComponent<LandData> ().grade);
						Debug.Log (player [i].property [j].GetComponent<LandData> ().landCurrentStatus);
						Debug.Log (player [i].property [j].GetComponent<LandData> ().Owner);
					}
					Debug.Log (player [i].AI);
				}
			}
//		if(Input.GetKeyDown(KeyCode.S))
//		{
//			for (int i = 0; i < 4; i++) 
//			{
//				Debug.Log ("Player" + i + " Race : " + (int)player [i].race);
//				Debug.Log ("Player" + i + " Race : " + player [i].money);
//				Debug.Log ("Player" + i + " AI : " + player [i].AI);
//			}
//		}
//		if (Input.GetKeyDown (KeyCode.I))
//		{
//			kliaBg.SetActive (true);
//		}
		//the followng line is to test the change races function
	}

	public IEnumerator LoadGame()
	{
		while(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "3.InGame")
		{
			yield return new WaitForSeconds (0.05f); //need to wait only work, i dont know why
		}
		Debug.Log ("Number of Max Player: " + maxPlayer);
		numberOfPlayerInGame = maxPlayer;
		//int extraMoney = 500;	//debug purpose, will remove
		RearrangePlayer();
		buildLandmark = false;
		pathObjectGO = GameObject.Find ("PathObject");
		testScript = pathObjectGO.GetComponent<TestScript> ();
		map3DGO = GameObject.Find ("3DMap");
		kliaBg = GameObject.Find ("Canvas").transform.Find ("KLIABackground").gameObject;
		displayBankruptOrMortgageMsgBG = GameObject.Find ("Canvas").transform.Find ("ChanceCardBackground").gameObject;
		stateMultiplerScript = GameObject.Find("Land Manager").GetComponent<StateMultiplier>();

		mapPieceArray = map3DGO.GetComponentsInChildren<Transform>();
		foreach (Transform mapPiece in mapPieceArray)
		{
			if (mapPiece.name != map3DGO.name)
			{
				mapPieceList.Add (mapPiece.gameObject);
				mapPiece.gameObject.GetComponent<MeshRenderer> ().material.SetColor ("_Color", Color.grey);
			}
		}
		foreach (Transform pathObj in testScript.path_objs)
		{
			if (pathObj.gameObject.GetComponent<LandData>().type == LandData.landType.NormalLand)
			{
				normalLandList.Add (pathObj.gameObject);
			}
		}
		landmarkParentGO = GameObject.Find ("Landmarks");
		landmarkArray = landmarkParentGO.GetComponentsInChildren<Transform> ();
		foreach (Transform landmark in landmarkArray)
		{
			if (landmark.name != landmarkParentGO.name) 
			{
				landmarkList.Add (landmark.gameObject);
				landmark.gameObject.SetActive (false);
			}
		}
		particleSystemForLandmark = GameObject.Find ("ParticleSystemForLandmark").gameObject;
		particleSystemForLandmark.SetActive (false);
		moveToLandmark = false;
		proceedToMortgage = false;
		moneyNotEnough = false;
		isMortgage = false;
		isBuy = false;
		isSell = false;
		isProceedToPropertyList = false;
		useEscapeCard = false;
		isDeducted = false;
		isRedeem = false;
		gameOver = false;
	}

	public void BackMain()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene ("1.Menu");
		player = new Player[4];
		playerEliminationList = new List<PlayerEliminationInfo> ();
		eliminateInfo.playerIndex = 0;
		eliminateInfo.totalTurn = 0;
		maxPlayer = 0;
	}

	public void RearrangePlayer()
	{
		int playerOrderNumber = 1;
		TestScript.instance.player = new GameObject[maxPlayer];
		for (int i = 0; i < maxPlayer; i++) 
		{
			if (player [i].isActive) 
			{
				Debug.Log ("Player" + playerOrderNumber + " Race : " + (int)player [i].race);
				player [i].character = GameObject.Find ("Player" + (i+1)); //assinging the gameobject into this array
				TestScript.instance.player[i] = player[i].character;
				player [i].character.transform.GetChild(0).GetComponent<Animator> ().runtimeAnimatorController = animatorOverride[(int)player[i].race];
				player [i].money = initialMoney;
				player [i].totalSteps = 0;
				player [i].completeRound = 0;
				player [i].property = new List<GameObject> ();
				player [i].turn = 0;
				player [i].totalAssetAmount = player [i].money;
				player [i].numOfMetropolisOwned = 0;
				player [i].escapePass = 0;
				player [i].eliminated = false;
				player [i].mortgagedProperty = new List<GameObject> ();
			}
			else 
			{
				Debug.Log ("Last number of player: "  + lastPlayerNum);
				lastPlayerNum--;
				for (int j = i; j < 4; j++) 
				{
					if (j + 1 < lastPlayerNum) 
					{
						player [j] = player [j + 1];
					}
					else
					{
						player [j].isActive = false;
						player [j].money = 0;
						player [j].totalSteps = 0;
						player [j].completeRound = 0;
						player [j].property = null;
						player [j].turn = 0;
						player [j].totalAssetAmount = 0;
						player [j].numOfMetropolisOwned = 0;
						player [i].escapePass = 0;
						player [j].eliminated = true;
						player [j].character = null; //assinging the gameobject into this array
					} 
				}
				i--;
			}
		}

		TestScript.instance.assignNextTarget ();

		for (int i = 4; i > maxPlayer; i--) 
		{
			GameObject.Find("Player" + i).SetActive(false);
		}
	}

	public int ZeroSumCalculation(int currentPlayer, int playerNumber)
	{
		int playerAiSum = 0;

		if (player [currentPlayer].property.Count > player [playerNumber].property.Count) 
		{
			Debug.Log ("More property");
			playerAiSum++;
			WinOrLoseProperty (0, playerNumber);
		} 
		else if (player [currentPlayer].property.Count < player [playerNumber].property.Count) 
		{
			Debug.Log ("Lesser property");
			playerAiSum--;
			WinOrLoseProperty (1, playerNumber);
		}

		if (player [currentPlayer].money > player [playerNumber].money) 
		{
			Debug.Log ("More money");
			playerAiSum++;
			WinOrLoseProperty (2, playerNumber);
		} 
		else if (player [currentPlayer].money < player [playerNumber].money) 
		{
			Debug.Log ("Lesser money");
			playerAiSum--;
			WinOrLoseProperty (3, playerNumber);
		}

		if (player [currentPlayer].totalAssetAmount > player [playerNumber].totalAssetAmount) 
		{
			Debug.Log ("More totalassetamount");
			playerAiSum++;
			WinOrLoseProperty (4, playerNumber);
		} 
		else if (player [currentPlayer].totalAssetAmount < player [playerNumber].totalAssetAmount) 
		{
			Debug.Log ("Lesser totalassetamount");
			playerAiSum--;
			WinOrLoseProperty (5, playerNumber);
		}

		if (player [currentPlayer].totalSteps > player [playerNumber].totalSteps) 
		{
			Debug.Log ("More total steps");
			playerAiSum++;
			WinOrLoseProperty (6, playerNumber);
		} 
		else if (player [currentPlayer].totalSteps < player [playerNumber].totalSteps) 
		{
			Debug.Log ("Lesser total steps");
			playerAiSum--;
			WinOrLoseProperty (7, playerNumber);
		}


		if (playerAiSum == 0)
			Debug.Log ("Balance");
		return playerAiSum;
	}

	public void WinOrLoseProperty(int num, int playerNum)
	{

		switch (num) 
		{
		case 0:
			AiComputer.instance.aiTarget.losingProperty = false;
			break;
		case 1:
			AiComputer.instance.aiTarget.losingProperty = true;
			break;
		case 2:  
			AiComputer.instance.aiTarget.losingMoney = false;
			break;
		case 3:  
			AiComputer.instance.aiTarget.losingMoney = true;
			break;
		case 4:  
			AiComputer.instance.aiTarget.losingTotalAsset = false;
			break;
		case 5: 
			AiComputer.instance.aiTarget.losingTotalAsset = true;
			break;
		case 6: 
			AiComputer.instance.aiTarget.losingSteps = false;
			break;
		case 7:  
			AiComputer.instance.aiTarget.losingSteps = true;
			break;

		}
	}
}