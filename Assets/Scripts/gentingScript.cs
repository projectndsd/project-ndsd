﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GentingScript : MonoBehaviour 
{
	[Header("Settings")]
	public GameObject smallButton;
	public GameObject bigButton;
	public GameObject theCardManager;

	[Header("Internal Setting")]
	public int whichPlayer;
	public GameObject currentPlayer;

	private RandomChance randomChanceScript;

	void Start ()
	{
		randomChanceScript = theCardManager.GetComponent<RandomChance> ();
	}
	//if player clicks on small money button, get card from random chance
	public void GetSmallCard ()
	{
		
		randomChanceScript.isSmallCard = true;
		randomChanceScript.getCard (whichPlayer, currentPlayer, LandData.landType.Genting);
	}
	//if player clicks on big money button, get card from random chance
	public void GetBigCard ()
	{
		
		randomChanceScript.isSmallCard = false;
		randomChanceScript.getCard (whichPlayer, currentPlayer, LandData.landType.Genting);
	}
}