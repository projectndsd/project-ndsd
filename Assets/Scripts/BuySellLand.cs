﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuySellLand : MonoBehaviour
{
	[Header("Internal Setting")]
	public string theLandName;
	public GameObject warningMsgForNego;
    public CanvasScript canvasScript;
	private InputField inputFieldText;
	private Text warningText;
	private Text confirmSellerMsgText;
	private Text confirmBuyerMsgText;
	private GameObject selectedLandObj;
	private GameObject confirmMsgSellerBg;
	private GameObject confirmMsgBuyerBg;
	private Button okButton;
	//private Button okButtonForWarning;
	private int inputPrice;
	public int sellerIndex;
	public int buyerIndex;

	void Start ()
	{
		inputFieldText = this.gameObject.transform.Find ("InputField").gameObject.GetComponent<InputField>();
		//confirmMsgBg = GameObject.Find ("Canvas").transform.Find ("ConfirmMsgBackground").gameObject;
		confirmMsgSellerBg = GameObject.Find("Canvas").transform.Find("SellerConfirmMsgBackground").gameObject;
		confirmMsgBuyerBg = GameObject.Find("Canvas").transform.Find("BuyerConfirmMsgBackground").gameObject;
		confirmBuyerMsgText = confirmMsgBuyerBg.transform.Find ("ConfirmMessage").gameObject.GetComponent<Text> ();
		confirmSellerMsgText = confirmMsgSellerBg.transform.Find ("ConfirmMessage").gameObject.GetComponent<Text> ();
		okButton = this.gameObject.transform.Find ("OkButton").gameObject.GetComponent<Button> ();
		//okButtonForWarning = warningMsgForNego.transform.Find ("OkButton").gameObject.GetComponent<Button> ();
		warningText = warningMsgForNego.transform.Find ("Message").gameObject.GetComponent<Text> ();
        canvasScript = GameObject.Find("UI_canvas").GetComponent<CanvasScript>();
    }
	void Update ()
	{
		if (inputFieldText.text != "")
		{
			okButton.interactable = true;
		}
		//if the player no input any amount, player unable to proceed to confirm message part
		else
		{
			okButton.interactable = false;
		}
	}

	public void DelayAwhile()
	{
		StartCoroutine (OnValueChangeCheck ());
	}

	public IEnumerator OnValueChangeCheck()
	{
		yield return new WaitForSeconds (0.01f);
		if (inputFieldText.text == "-") 
		{
			inputFieldText.text = "0";
		}
		else if (int.Parse(inputFieldText.text) > GameManager.instance.player [buyerIndex].money) 
		{
			inputFieldText.text = GameManager.instance.player [buyerIndex].money.ToString ();
		}
	}

	//change the confirmation message from seller to buyer
	public void ConfirmMsgForBuyer ()
	{
		
		confirmMsgSellerBg.SetActive (false);
		confirmMsgBuyerBg.SetActive (true);
		confirmBuyerMsgText.text = "Player " + (buyerIndex + 1).ToString () + ", do you want to buy " + theLandName + " from Player " + (sellerIndex + 1).ToString () + " with the price: " + inputPrice.ToString () + "?";
		if (GameManager.instance.player [buyerIndex].AI) 
		{
			AiComputer.instance.DelayForConfirmNegoBuySell (this.gameObject, true, inputFieldText.text, sellerIndex, buyerIndex);
		}
	}
	//pop up confirm message for seller
	public void ConfirmMsgForSeller ()
	{
		
		this.gameObject.SetActive (false);
		inputPrice = int.Parse (inputFieldText.text);

		//if the player inputed the value that < 0 or > buyer's money, pop up the warning message
		if (inputPrice < 0 || inputPrice > GameManager.instance.player [buyerIndex].money) 
		{
			warningMsgForNego.SetActive (true);
			warningText.text = "Please enter the price in between 0 to " + GameManager.instance.player [buyerIndex].money.ToString ();
		} 
		else
		{
			confirmMsgSellerBg.SetActive (true);
			confirmSellerMsgText.text = "Player " + (sellerIndex + 1).ToString () + ", do you want to sell " + theLandName + " to Player " + (buyerIndex + 1).ToString () + " with the price: " + inputPrice.ToString () + "?";
			if (GameManager.instance.player [sellerIndex].AI) 
			{
				AiComputer.instance.DelayForConfirmNegoBuySell (this.gameObject, false, inputFieldText.text, buyerIndex, sellerIndex);
			}
		}
	}
	//Close the warning message
	public void CloseWarning ()
	{
		
		warningMsgForNego.SetActive (false);
		inputPrice = 0;
		GameManager.instance.isBuy = false;
		GameManager.instance.isSell = false;
		GameManager.instance.isProceedToPropertyList = false;
	}
	//when the player decided not going to buy or sell the land during confirmation
	public void CloseNego ()
	{
		
		confirmMsgBuyerBg.SetActive (false);
		confirmMsgSellerBg.SetActive (false);
		inputPrice = 0;
		GameManager.instance.isBuy = false;
		GameManager.instance.isSell = false;
		GameManager.instance.isProceedToPropertyList = false;
	}
	//when player decided not going to buy or sell the land during negotiation
	public void CancelNego ()
	{
		
		this.gameObject.SetActive (false);
		inputPrice = 0;
		GameManager.instance.isBuy = false;
		GameManager.instance.isSell = false;
		GameManager.instance.isProceedToPropertyList = false;
	}
	//function for processing buy sell land between players
	public void BuySellLandFunction ()
	{
		
		//confirmMsgBg.transform.Find ("YesButton").GetComponent<Button> ().onClick.RemoveListener (BuySellLandFunction);
		//confirmMsgBg.transform.Find ("YesButton").GetComponent<Button> ().onClick.AddListener (ConfirmMsgForSeller);
		confirmMsgBuyerBg.SetActive (false);
		//get the land object from the list
		foreach (GameObject normalLand in GameManager.instance.normalLandList)
		{
			if (normalLand.GetComponent<LandData> ().landName == theLandName)
			{
				selectedLandObj = normalLand;
				break;
			}
		}
		//deduct buyer's money and add seller's money
		SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.moneySfx);
		GameManager.instance.TotalAssetAmountCalculation (-inputPrice, buyerIndex);
		GameManager.instance.TotalAssetAmountCalculation (inputPrice, sellerIndex);
		GameManager.instance.moneyCalculation (-inputPrice, buyerIndex);
		GameManager.instance.moneyCalculation (inputPrice, sellerIndex);
		//change the owner
		selectedLandObj.GetComponent<LandData> ().Owner = (LandData.owner)(buyerIndex + 1);
        //add the property to buyer and remove it from seller

        ChangeUIDescription(selectedLandObj.GetComponent<LandData>()); //edit UI over this function here
        GameManager.instance.player [buyerIndex].property.Add (selectedLandObj);
		GameManager.instance.player [sellerIndex].property.Remove (selectedLandObj);
		selectedLandObj.transform.Find ("LandIndicatorSprite").gameObject.GetComponent<SpriteRenderer>().sprite = GameManager.instance.indicatorList [buyerIndex + 1];	//change the indicator from bank to new owner
		GameManager.instance.CalculateLandAsset (selectedLandObj, buyerIndex, false);
		GameManager.instance.CalculateLandAsset (selectedLandObj, sellerIndex, true);
		if (AiComputer.instance.BuySell) 
		{
			AiComputer.instance.BuySell = false;
			AiComputer.instance.BuyRoll (AiComputer.instance.curPlayer);
		}

        //checking for races
        if (GameManager.instance.player [sellerIndex].race == GameManager.Race.Chinese)
		{
			LandData landData = selectedLandObj.GetComponent<LandData> ();
			landData.landRental = (int)((float)landData.landRental / GameManager.instance.chineseRentalRate) + 1;		//the +1 is to adjust the slightly different of the rental
			landData.kampungRental = (int)((float)landData.kampungRental / GameManager.instance.chineseRentalRate) + 1;
			landData.cityRental = (int)((float)landData.cityRental / GameManager.instance.chineseRentalRate) + 1;
			landData.metropolisRental = (int)((float)landData.metropolisRental / GameManager.instance.chineseRentalRate) + 1;
		}
		if (GameManager.instance.player [buyerIndex].race == GameManager.Race.Chinese)
		{
			LandData landData = selectedLandObj.GetComponent<LandData> ();
			landData.landRental = (int)((float)landData.landRental * GameManager.instance.chineseRentalRate);
			landData.kampungRental = (int)((float)landData.kampungRental * GameManager.instance.chineseRentalRate);
			landData.cityRental = (int)((float)landData.cityRental * GameManager.instance.chineseRentalRate);
			landData.metropolisRental = (int)((float)landData.metropolisRental * GameManager.instance.chineseRentalRate);
		}
		//checking for land multiplier
		GameManager.instance.DeactivateMultiplier (theLandName);
		GameManager.instance.MultiplierChecking (theLandName, buyerIndex + 1);

		GameManager.instance.isBuy = false;
		GameManager.instance.isSell = false;
		GameManager.instance.isProceedToPropertyList = false;

		if (GameManager.instance.gameMode == GameManager.GameModeChoices.Metropolis)
		{
			GameManager.instance.MetropolisModeChecking (buyerIndex);
		}
	}

    void ChangeUIDescription(LandData currentLandStatus)
    {
        switch (currentLandStatus.landCurrentStatus)
        {
			case LandData.landStatus.Land:
                canvasScript.playerProperties[sellerIndex].landOwned -= 1;
                canvasScript.playerProperties[buyerIndex].landOwned += 1;
                break;
                
            case LandData.landStatus.Kampung:
                canvasScript.playerProperties[sellerIndex].kampungOwned -= 1;
                canvasScript.playerProperties[buyerIndex].kampungOwned += 1;
                break;
            case LandData.landStatus.City:
                canvasScript.playerProperties[sellerIndex].cityOwned -= 1;
                canvasScript.playerProperties[buyerIndex].cityOwned += 1;
                break;
            case LandData.landStatus.Metropolis:
                canvasScript.playerProperties[sellerIndex].metropolisOwned -= 1;
                canvasScript.playerProperties[buyerIndex].metropolisOwned += 1;
                break;

        }
    }
}