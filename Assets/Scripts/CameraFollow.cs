﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraFollow : MonoBehaviour 
{
	[Header ("Setting")]
	public Button rollDicesButton;
	public Button buyButton;
	public Button sellButton;
	public Button settingButton;
	public float cameraMoveTime;
	public AnimationCurve animationCurve;

	[Header ("Uneditable")]
	public GameObject currentPlayer;
	public Vector3 camInitialPos;
	//public bool allowEdgeScrolling;

	private float elapsedTime;
	private float fracPath;
	private Button returnCenterButton;
	private Vector3 current;
	public Vector3 cameraOffset;
	private Vector3 cameraCurrentPos;
	private Vector3 nextCameraPos;
	private CameraFollow cameraFollowScript;
	private CamEdgeScrolling camEdgeScrollScript;
	private FollowPath followPathForPlayer;
	private bool firstEnable = true;
	private bool isCamReturnPlayer;
	private BuyLandScript buyLandScript;
	//private TestScript nextPlayer;

    //Below is UI coding
    public int delayedCurrentCharacter;
    public TestScript testScript;
	// Use this for initialization
	void Awake ()
	{
		isCamReturnPlayer = false;
		currentPlayer = GameObject.Find ("Player1");
		GameObject UICanvas = GameObject.Find ("UI_canvas");
		GameObject canvas = GameObject.Find ("Canvas");
		returnCenterButton = UICanvas.transform.Find ("return_center_button").gameObject.GetComponent<Button>();
		buyLandScript = canvas.transform.Find ("BuyLandBackground").gameObject.GetComponent<BuyLandScript> ();
		cameraFollowScript = GetComponent<CameraFollow> ();
		camEdgeScrollScript = GetComponent<CamEdgeScrolling> ();
		camEdgeScrollScript.theParent = currentPlayer;
		cameraOffset = currentPlayer.transform.position - transform.position;
		this.gameObject.transform.parent = currentPlayer.transform;
		current = transform.localPosition;
		camEdgeScrollScript.enabled = true;
		returnCenterButton.interactable = false;
        //below is UI declaration
        testScript = GameObject.Find("PathObject").GetComponent<TestScript>();
    }
	void OnEnable ()
	{
		if (!firstEnable) 
		{
			rollDicesButton.GetComponent<Button> ().interactable = false;
			buyButton.GetComponent<Button> ().interactable = false;
			sellButton.GetComponent<Button> ().interactable = false;
			settingButton.GetComponent<Button> ().interactable = false;
			camEdgeScrollScript.enabled = false;
			returnCenterButton.interactable = false;
			//allowEdgeScrolling = false;
		} 
		if (GameManager.instance.player [int.Parse (currentPlayer.name.Substring (currentPlayer.name.Length - 1)) - 1].AI) 
		{
			rollDicesButton.GetComponent<Button> ().interactable = false;
			buyButton.GetComponent<Button> ().interactable = false;
			sellButton.GetComponent<Button> ().interactable = false;
			settingButton.GetComponent<Button> ().interactable = false;
			camEdgeScrollScript.enabled = false;
			returnCenterButton.interactable = false;
		}
	}
	void OnDisable ()
	{
		if (!GameManager.instance.gameOver) 
		{
			//Debug.Log ("Player " + int.Parse (currentPlayer.name.Substring (currentPlayer.name.Length - 1)) + ": " + GameManager.instance.player [int.Parse (currentPlayer.name.Substring (currentPlayer.name.Length - 1)) - 1].money);
			elapsedTime = 0f;		//reset elapsedTime and fracPath
			fracPath = 0f;
			if (!GameManager.instance.moveToLandmark) 
			{
				followPathForPlayer = currentPlayer.GetComponent <FollowPath> ();	//get the FollowPath script of current player
				//if the current player is in jail or theme park
				if (followPathForPlayer.isJailed || followPathForPlayer.isInThemePark)
				{	
					if (GameManager.instance.gameMode == GameManager.GameModeChoices.TurnLimit)
					{
						GameManager.instance.TurnLimitModeChecking ();
					}
					//check if the current player's turn is equal to the calculated returnToGameTurn
					if (GameManager.instance.player [int.Parse (currentPlayer.name.Substring (currentPlayer.name.Length - 1)) - 1].turn != followPathForPlayer.returnToGameTurn) 
					{
						//if no, which means that the player's punishment havent end, pass his/her turn by changing the current player to next player
						//change the currentCharacter number to next player which will be needed
						//to decide which player moves based on the dices result
						testScript.currentCharacter++;
						testScript.currentCharacter %= GameManager.instance.maxPlayer;
						currentPlayer = GameManager.instance.player [testScript.currentCharacter].character;
						camEdgeScrollScript.theParent = currentPlayer;
						testScript.currentCharacter = testScript.EliminationPlayerChecking ();
						cameraFollowScript.enabled = true;	//re-enabled the camera script to move the camera again without rolling dices
					} 
					else
					{
						//if the number is equal, which means that the imprisoned player ended his/her punishment
						//set isJail and isInThemePark to false
						followPathForPlayer.isJailed = false;
						followPathForPlayer.isInThemePark = false;
						GameManager.instance.player [int.Parse (currentPlayer.name.Substring (currentPlayer.name.Length - 1)) - 1].isInJail = followPathForPlayer.isJailed;	//assign the same result to game manager's player
						if(!GameManager.instance.player[int.Parse (currentPlayer.name.Substring (currentPlayer.name.Length - 1)) - 1].AI)
						{
						rollDicesButton.GetComponent<Button> ().interactable = true;	//re-enable the roll dices button
						buyButton.GetComponent<Button> ().interactable = true;
						sellButton.GetComponent<Button> ().interactable = true;
						settingButton.GetComponent<Button> ().interactable = true;
						camEdgeScrollScript.enabled = true;
						camEdgeScrollScript.theParent = currentPlayer;
						returnCenterButton.interactable = true;
						//testScript.isCamReturnPlayer = false;
						//allowEdgeScrolling = true;
						}
						else
						{
							CheckAI ();
						}
					}
				} 
				else
				{
					if (!testScript.isRolling)
					{
						camEdgeScrollScript.enabled = true;
						camEdgeScrollScript.theParent = currentPlayer;
						returnCenterButton.interactable = true;
						CheckAI ();
					}
				}
			}
			else 
			{
				GameManager.instance.moveToLandmark = false;
			}
			camReturn (false);
			firstEnable = false;
		}
	}

	public void CheckAI()
	{
		int currentPlayer = 0;
		//Debug.Log (followPathForPlayer.name.Substring (followPathForPlayer.name.Length - 1));
		if (int.Parse (followPathForPlayer.name.Substring (followPathForPlayer.name.Length - 1)) - 1 > 0) 
		{
			currentPlayer = int.Parse (followPathForPlayer.name.Substring (followPathForPlayer.name.Length - 1)) - 1;
		} 
		else 
		{
			currentPlayer = 0;
		}

		if (GameManager.instance.player [currentPlayer].AI) 
		{
			rollDicesButton.GetComponent<Button> ().interactable = false;
			buyButton.GetComponent<Button> ().interactable = false;
			sellButton.GetComponent<Button> ().interactable = false;
			settingButton.GetComponent<Button> ().interactable = false;
			StartCoroutine (AiComputer.instance.GetData (currentPlayer));
		} 
		else 
		{
			rollDicesButton.GetComponent<Button> ().interactable = true;
			buyButton.GetComponent<Button> ().interactable = true;
			sellButton.GetComponent<Button> ().interactable = true;
			settingButton.GetComponent<Button> ().interactable = true;
			AiComputer.instance.curPlayer = currentPlayer;
		}
	}

	void Update ()
	{
		moveCameraAnimation ();
	}
	//move the camera from current Player to next Player
	public void moveCameraAnimation ()
	{
		cameraCurrentPos = this.gameObject.transform.position;
		nextCameraPos = currentPlayer.transform.position - cameraOffset;

		elapsedTime += Time.deltaTime;
		fracPath = elapsedTime / cameraMoveTime;	//get the fraction of the path that has been moved
		//move the camera follows the animation curve that is preset (but still not like wat i exptected D:)
		transform.position = Vector3.Lerp (cameraCurrentPos, nextCameraPos, animationCurve.Evaluate (fracPath));
		//if the elapsed time exceeded the preset camera move time, parent the camera and disable script
		//so that the elapsed time and fracpath will be reset
		if (elapsedTime >= cameraMoveTime) 
		{
			if (!GameManager.instance.moveToLandmark) 
			{
				this.gameObject.transform.parent = currentPlayer.transform;	//new currentPlayer (which is the next player after the camera moved) already assigned from FollowPath script
				transform.localPosition = current;	//set the local position of camera same as the initial local position
				//if this camera enabled is to move back to center of player, no need add turn
				if (!isCamReturnPlayer)
				{
					GameManager.instance.addTurn (int.Parse (currentPlayer.name.Substring (currentPlayer.name.Length - 1)) - 1);
				}
				delayedCurrentCharacter = testScript.EliminationPlayerChecking();
			} 
			else
			{
				buyLandScript.ActiveLandmark (GameManager.instance.particleSystemForLandmark, buyLandScript);
			}
			cameraFollowScript.enabled = false;
		}
	}
	public void camReturn (bool camReturnVar)
	{
		isCamReturnPlayer = camReturnVar;
	}
}