﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestScript : MonoBehaviour
{
	public Button rollDicesButton;
    public Color rayColor = Color.white;
	public int dice1, dice2;
    public List<Transform> path_objs = new List<Transform>();
    Transform[] Arrays;
    public int currentCharacter = 0;
	public GameObject[] diceObject;
	public bool isRolling;
	public bool isCamReturnPlayer;
	public GameObject[] player;

	private CamEdgeScrolling camEdgeScroll;
	private CameraFollow camFollow;
	private GameObject camTarget;

	//use to deactive all the opened buy sell ui
	private GameObject mortgageBg;
	private GameObject playerSelectionBg;
	private GameObject priceNegoBg;
	private GameObject confirmSellerMsgBg;
	private GameObject confirmBuyerMsgBg;
	private GameObject warningBg;

	public static TestScript instance;
	public int totalDiceNum;

	void Awake () 
	{
		if (instance == null) 
		{
			instance = this;
		} 
		else if (instance != this) 
		{
			Destroy (gameObject);
		}
	}

	public void assignNextTarget()
	{
		for (int i = 0; i < GameManager.instance.maxPlayer; i++) 
		{
			player [i].GetComponent<FollowPath> ().nextTarget = GameManager.instance.player [(i + 1) % GameManager.instance.maxPlayer].character;
		}
	}

    public void Start ()
    {
		totalDiceNum = 0;
		isRolling = false;
		camTarget = GameObject.Find ("CameraTarget");
		camEdgeScroll = camTarget.GetComponent<CamEdgeScrolling> ();
		camFollow = camTarget.GetComponent<CameraFollow> ();
		GameObject canvas = GameObject.Find ("Canvas");
		mortgageBg = canvas.transform.Find ("MortgageScrollBackground").gameObject;
		playerSelectionBg = canvas.transform.Find ("PlayerSelectionBackground").gameObject;
		priceNegoBg = canvas.transform.Find ("PriceNegoBackground").gameObject;
		confirmBuyerMsgBg = canvas.transform.Find ("BuyerConfirmMsgBackground").gameObject;
		confirmSellerMsgBg = canvas.transform.Find ("SellerConfirmMsgBackground").gameObject;
		warningBg = canvas.transform.Find ("WarningForNegoPrice").gameObject;

        // End of lines requires to change
    }
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Alpha0))
		{
			player[currentCharacter].GetComponent<FollowPath>().LandChecking (player[currentCharacter].GetComponent<FollowPath>().CurrentWayPointID);
			currentCharacter++;
			currentCharacter %= GameManager.instance.maxPlayer;
		}
		if (Input.GetKeyDown (KeyCode.F1))
		{
			totalDiceNum = 1;
		}
		if (Input.GetKeyDown (KeyCode.F2))
		{
			totalDiceNum = 2;
		}
		if (Input.GetKeyDown (KeyCode.F3))
		{
			totalDiceNum = 3;
		}
		if (Input.GetKeyDown (KeyCode.F4))
		{
			totalDiceNum = 4;
		}
		if (Input.GetKeyDown (KeyCode.F5))
		{
			totalDiceNum = 5;
		}
		if (Input.GetKeyDown (KeyCode.F6))
		{
			totalDiceNum = 6;
		}
		if (Input.GetKeyDown (KeyCode.F7))
		{
			totalDiceNum = 7;
		}
		if (Input.GetKeyDown (KeyCode.F8))
		{
			totalDiceNum = 8;
		}
		if (Input.GetKeyDown (KeyCode.F9))
		{
			totalDiceNum = 9;
		}
		if (Input.GetKeyDown (KeyCode.F10))
		{
			totalDiceNum = 10;
		}
		if (Input.GetKeyDown (KeyCode.F11))
		{
			totalDiceNum = 11;
		}
		if (Input.GetKeyDown (KeyCode.F12))
		{
			totalDiceNum = 12;
		}
	}
    void OnDrawGizmos()
    {
        Gizmos.color = rayColor;
        Arrays = GetComponentsInChildren<Transform>();
        path_objs.Clear();

        foreach (Transform path_obj in Arrays)
        {
			if (path_obj != this.transform && path_obj.gameObject.tag == "lands")
            {
                path_objs.Add(path_obj);
            }
        }

        for (int i = 0; i < path_objs.Count; i++)
        {
            Vector3 position = path_objs[i].position;
            if (i > 0)
            {
                Vector3 previous = path_objs[i - 1].position;
                Gizmos.DrawLine(previous, position);
                Gizmos.DrawWireSphere(position, 0.3f);
            }
        }
    }
	public void rollDices()
    {
		mortgageBg.SetActive (false);
		playerSelectionBg.SetActive (false);
		priceNegoBg.SetActive (false);
		confirmBuyerMsgBg.SetActive (false);
		confirmSellerMsgBg.SetActive (false);
		warningBg.SetActive (false);
		GameManager.instance.isBuy = false;
		GameManager.instance.isSell = false;
		GameManager.instance.isProceedToPropertyList = false;
		isRolling = true;
		camFollow.enabled = true;
		camFollow.camReturn (true);
		camEdgeScroll.enabled = false;
		rollDicesButton.GetComponent<Button>().interactable = false;
		camFollow.buyButton.GetComponent<Button> ().interactable = false;
		camFollow.sellButton.GetComponent<Button> ().interactable = false;
		camFollow.settingButton.GetComponent<Button> ().interactable = false;
		dice1 = Random.Range(1, 7);
        dice2 = Random.Range(1, 7);
		totalDiceNum = dice1 + dice2;
		StartCoroutine(RollDiceAnimation(dice1,dice2));
    }

	public IEnumerator RollDiceAnimation(int num1, int num2)
	{
		diceObject [0].transform.gameObject.SetActive(true);
     	diceObject [1].transform.gameObject.SetActive(true);
		SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.rollDicesSfx);
		diceObject [0].GetComponent<Animator> ().SetInteger ("DiceNum",0);
		diceObject [1].GetComponent<Animator> ().SetInteger ("DiceNum",0);
		yield return new WaitForSeconds (1.5f);
		diceObject [0].GetComponent<Animator> ().SetInteger ("DiceNum",num1);
		diceObject [1].GetComponent<Animator> ().SetInteger ("DiceNum",num2);
		yield return new WaitForSeconds (2.5f);
		GameManager.instance.steps++;
		currentCharacter = EliminationPlayerChecking ();
//		GameManager.instance.player [currentCharacter].totalSteps += dice1 + dice2;
//		player[currentCharacter].GetComponent<FollowPath>().steps = dice1 + dice2;
		GameManager.instance.player [currentCharacter].totalSteps += totalDiceNum;
		player[currentCharacter].GetComponent<FollowPath>().steps = totalDiceNum;
		//int theSteps = GameObject.Find (current [currentCharacter]).GetComponent<FollowPath> ().steps;
		//GameObject.Find (current [currentCharacter]).GetComponent<FollowPath> ().totalElapsedSteps += theSteps;
		diceObject [0].transform.gameObject.SetActive(false);
		diceObject [1].transform.gameObject.SetActive(false);
		isRolling = false;
		StartCoroutine(player[currentCharacter].GetComponent<FollowPath>().move());
		currentCharacter++;
		currentCharacter %= GameManager.instance.maxPlayer;
	}
	public int EliminationPlayerChecking ()
	{
		if (GameManager.instance.player [currentCharacter].eliminated)
		{
			currentCharacter++;
			currentCharacter %= GameManager.instance.maxPlayer;
			return EliminationPlayerChecking ();
		} 
		return currentCharacter;
	}
}