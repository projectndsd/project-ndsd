﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QNAAnswerScript : MonoBehaviour
{
	[Header("Setting")]
	public GameObject resultUI;
	public GameObject QNAbg;

	private Text selectedButtonText;
	private Text displayResultText;
	private RandomChance randomChanceScript;
	private ChanceScript chanceCardScript;
	private int fees;

	void Start ()
	{
		selectedButtonText = this.gameObject.transform.Find ("Text").gameObject.GetComponent<Text>();
		randomChanceScript = GameObject.Find ("Card Manager").GetComponent<RandomChance> ();
		resultUI = GameObject.Find ("Canvas").transform.Find ("ChanceCardBackground").gameObject;
		chanceCardScript = resultUI.GetComponent<ChanceScript> ();
		displayResultText = chanceCardScript.chanceCardText;
	}

	public void CheckAnswer ()
	{
		SoundScript.instance.audioSrc.Stop ();
		QNAbg.SetActive (false);
		resultUI.SetActive (true);
		chanceCardScript.playerIndex = randomChanceScript.playerIndexArray;
		if (selectedButtonText.text == randomChanceScript.qnaListCollector.QNAList [randomChanceScript.selectedCardNum].correctAnswer) 
		{
			SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.happySfx);
			displayResultText.text = "Congratulation! You answered it correctly! No need pay for the ferry fees!";
			displayResultText.color = GameManager.instance.greenColorSelection;
		} 
		else
		{
			SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.sadSfx);
			if (randomChanceScript.ferryType == LandData.landType.Ferry1)
			{
				fees = randomChanceScript.ferryFee1;
			}
			else
			{
				fees = randomChanceScript.ferryFee2;
			}
			displayResultText.text = "Opps... Wrong answer... Pay " + fees.ToString() + " for the ferry fees...";
			displayResultText.color = Color.red;
			SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.moneySfx);
			GameManager.instance.TotalAssetAmountCalculation (-fees, randomChanceScript.playerIndexArray);
			GameManager.instance.moneyCalculation (-fees, randomChanceScript.playerIndexArray);
		}
	}
}