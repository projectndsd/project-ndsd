﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PassValueToGameManager: MonoBehaviour
{
    public int playerAmountInt;
    public static PassValueToGameManager instance;
    public static RaceSelectScript raceSelectScript;
    // Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
	
}
