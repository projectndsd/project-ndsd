﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MortgageCheck : MonoBehaviour
{
	private GameObject mortgagedSuccessBg;
	private GameObject mortgageScrollBg;
	private GameObject priceNegoBg;
	private Text mortgageSuccessText;
	private Text placeHolderText;
	private Text priceMsg;
	private int landOriginalPrice;
	private CanvasScript canvasScript;

	// Use this for initialization
	void Start ()
	{
		landOriginalPrice = 0;
		canvasScript = GameObject.Find ("UI_canvas").GetComponent<CanvasScript>();
		mortgageScrollBg = GameObject.Find ("Canvas").transform.Find ("MortgageScrollBackground").gameObject;
		mortgagedSuccessBg = GameObject.Find ("Canvas").transform.Find ("ChanceCardBackground").gameObject;
		mortgageSuccessText = mortgagedSuccessBg.transform.Find ("ChanceCardMessage").GetComponent<Text>();
		priceNegoBg = GameObject.Find ("Canvas").transform.Find ("PriceNegoBackground").gameObject;
		placeHolderText = priceNegoBg.transform.Find ("InputField").transform.Find ("Placeholder").gameObject.GetComponent<Text> ();
		priceMsg = priceNegoBg.transform.Find ("PriceMessage").gameObject.GetComponent<Text>();
	}
	public void Checking ()
	{
		
		string theLandName;
		theLandName = this.gameObject.transform.Find ("LandNameText").GetComponent<Text> ().text;
		//if the player not click on sell or buy button, which means this button is for mortgage part
		if (!GameManager.instance.isSell && !GameManager.instance.isBuy) 
		{
			string mortgageAmountString = this.gameObject.transform.Find ("MortgagePriceText").GetComponent<Text> ().text;
			int mortgageAmount = int.Parse (mortgageAmountString);
			mortgageScrollBg.SetActive (false);
			mortgagedSuccessBg.SetActive (true);
			GameManager.instance.isMortgage = true;
			GameManager.instance.mortgageCount += 1;
			mortgagedSuccessBg.GetComponent<ChanceScript> ().mortgagePrice = mortgageAmount;
			mortgageSuccessText.text = theLandName + " has been mortgaged to bank!";
			GameManager.instance.DeactivateMultiplier (theLandName);

			//find the land object in normal land list in order to make the map piece to grey
			foreach (GameObject normalLand in GameManager.instance.normalLandList)
			{
				if (normalLand.GetComponent<LandData> ().landName == theLandName) 
				{
					GameManager.instance.CalculateLandAsset (normalLand, canvasScript.currentCharacter, true);
					normalLand.GetComponent<LandData> ().landStatusBeforeMortgage = normalLand.GetComponent<LandData> ().landCurrentStatus; //this is needed for redeem part
                    ChangeUIDescriptionAfterMortgageLand(normalLand.GetComponent<LandData>().landCurrentStatus);
                    normalLand.GetComponent<LandData> ().landCurrentStatus = LandData.landStatus.Mortgaged;
					int indexOfTheLand = GameManager.instance.normalLandList.IndexOf (normalLand);
					GameManager.instance.mapPieceList [indexOfTheLand].GetComponent<MeshRenderer> ().material.SetColor ("_Color", Color.grey);
					GameManager.instance.player [canvasScript.currentCharacter].property.Remove (normalLand);
					GameManager.instance.player [canvasScript.currentCharacter].mortgagedProperty.Add (normalLand);
					break;
				}
			}
		} 
		else 
		{
			mortgageScrollBg.SetActive (false);
			priceNegoBg.SetActive (true);
			//find the land object in normal land list in order to get the original price
			foreach (GameObject normalLand in GameManager.instance.normalLandList)
			{
				if (normalLand.GetComponent<LandData> ().landName == theLandName) 
				{
					LandData landData = normalLand.GetComponent<LandData> ();
					switch (landData.landCurrentStatus) 
					{
					case LandData.landStatus.Land:
						landOriginalPrice = landData.landPrice;
						break;
					case LandData.landStatus.Kampung:
						landOriginalPrice = landData.landPrice + landData.kampungPrice;
						break;
					case LandData.landStatus.City:
						landOriginalPrice = landData.landPrice + landData.kampungPrice + landData.cityPrice;
						break;
					case LandData.landStatus.Metropolis:
						landOriginalPrice = landData.landPrice + landData.kampungPrice + landData.cityPrice + landData.metropolisPrice;
						break;
					}
					break;
				}
			}
			if (GameManager.instance.isSell)
			{
				priceMsg.text = "How much do you want to sell the land: " + theLandName + "? Original price: " + landOriginalPrice.ToString();
				placeHolderText.text = "Enter Selling Price...";
				AiComputer.instance.originalPrice = landOriginalPrice;
				if (GameManager.instance.player [AiComputer.instance.curPlayer].AI) 
				{
					AiComputer.instance.DelayForPriceNegoBuySell (true, priceNegoBg);
				}
			}
			else
			{
				priceMsg.text = "How much do you want to buy the land: " + theLandName + "? Original price: " + landOriginalPrice.ToString();
				placeHolderText.text = "Enter Buying Price...";
				AiComputer.instance.originalPrice = landOriginalPrice;
				if (GameManager.instance.player [AiComputer.instance.curPlayer].AI) 
				{
					AiComputer.instance.DelayForPriceNegoBuySell (false, priceNegoBg);
				}
			}
			priceNegoBg.GetComponent<BuySellLand> ().theLandName = theLandName;
		}
	}

    void ChangeUIDescriptionAfterMortgageLand(LandData.landStatus currentStatus)
    {
        switch (currentStatus)
        {
            case LandData.landStatus.EmptyLand:
                canvasScript.playerProperties[canvasScript.currentCharacter].landOwned -= 1;
                break;

            case LandData.landStatus.Kampung:
                canvasScript.playerProperties[canvasScript.currentCharacter].kampungOwned -= 1;
                break;
            case LandData.landStatus.City:
                canvasScript.playerProperties[canvasScript.currentCharacter].cityOwned -= 1;
                break;
            case LandData.landStatus.Metropolis:
                canvasScript.playerProperties[canvasScript.currentCharacter].metropolisOwned -= 1;
                break;

        }
    }
}