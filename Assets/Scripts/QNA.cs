﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class QNA
{
	public string question;
	public string correctAnswer;
	public string wrongAnswer1;
	public string wrongAnswer2;
}