using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimation : MonoBehaviour {

	public Animator anim;

	public void Awake()
	{
		anim.SetBool ("LeftRight", false);
		Idle ();
	}

	public void OnSea(bool LeftRight)
	{
		anim.SetBool ("Walk", false);
		if (LeftRight) 
		{
			anim.SetBool ("LeftRight", true);
		} 
		else 
		{
			anim.SetBool ("LeftRight", false);
		}
		anim.SetBool ("Sea", true);
	}

	public void Walk(bool LeftRight)
	{
		anim.SetBool ("Sea", false);
		if (LeftRight) 
		{
			anim.SetBool ("LeftRight", true);
		} 
		else 
		{
			anim.SetBool ("LeftRight", false);
		}
		anim.SetBool ("Walk", true);
	}

	public void Idle()
	{
		anim.SetBool ("Walk", false);
	}

	public bool IsOnSea()
	{
		if (anim.GetBool ("Sea")) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
