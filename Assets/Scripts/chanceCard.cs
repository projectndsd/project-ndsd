﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class ChanceCard	//class for chance card details
{
    public string name;
    public string cardMessage;
    public int amountOfMoney;
}