﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectGameData : ScriptableObject
{
    public List<ChanceCard> cardList;
}