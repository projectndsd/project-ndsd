﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChanceScript : MonoBehaviour
{
	[Header ("Setting")]
    public Text chanceCardText;
	public GameObject escapeJailBg;
	public Text passMsgText;
	public RandomChance randomChanceScript;
    [HideInInspector]
    public GameObject currentPlayer;	//this is important to decide the camera moves from which player to which player
										//because this current player will decide who is the next player in moveTheCamera function under FollowPath script
	[Header("Internal Setting")]
	public int playerIndex;
	public int mortgagePrice;

	private int moneyAmount;
	private CanvasScript canvasScript;
	private int totalCost;

	void Awake ()
	{
		totalCost = 0;
		canvasScript = GameObject.Find ("UI_canvas").GetComponent<CanvasScript> ();
	}
    public void open()
    {
        this.gameObject.SetActive(true);
    }
    public void close()
    {
        this.gameObject.SetActive(false);
		//below 2 lines cannot simply revert the order because it will affect the pop up msg
		GameManager.instance.playerSurviveChecking ();
		GameManager.instance.CheckingForBankruptAndElimination (playerIndex, currentPlayer);
		GameManager.instance.isDeducted = false;
	}
    public void DisplayCard(string selectedCardMessage, Color sentenceColor, int amountOfMoney, int whichPlayer)
    {
        chanceCardText.text = selectedCardMessage;  //display the message from the preset list
        chanceCardText.color = sentenceColor;   //display the text color by following the respective color
		playerIndex = whichPlayer;
		moneyAmount = amountOfMoney;
		SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.moneySfx);
		GameManager.instance.TotalAssetAmountCalculation (amountOfMoney, whichPlayer);
		GameManager.instance.moneyCalculation(amountOfMoney, whichPlayer);	//do the calculation
    }
	//this is the function when the player click on yes button for using the escape pass
	public void UseEscapePassFunction ()
	{
		escapeJailBg.SetActive (false);
		this.gameObject.SetActive (true);
		GameManager.instance.player [playerIndex].escapePass -= 1;
		randomChanceScript.escapeCardTakenList.Remove (randomChanceScript.escapeCardTakenList [Random.Range (0, randomChanceScript.escapeCardTakenList.Count)]);
		for (int num = 0; num < randomChanceScript.escapeCardTakenList.Count; num++)
		{
			Debug.Log ("escape pass left (element in array): " + randomChanceScript.escapeCardTakenList [num]);
		}
		chanceCardText.text = "You used up 1 escape pass! No need send to jail! Left: " + GameManager.instance.player [playerIndex].escapePass;
		chanceCardText.color = GameManager.instance.greenColorSelection;
		GameManager.instance.useEscapeCard = false;
	}
	//if the player choose to not use the escape pass, then send him/her to jail
	public void SendJail ()
	{
		escapeJailBg.SetActive (false);
		this.gameObject.SetActive (true);
		chanceCardText.text = "Go to jail!";
		chanceCardText.color = Color.red;
		GameManager.instance.useEscapeCard = false;
		randomChanceScript.SendToJail ();
	}
    public void closeChanceCard()
    {
		
		//check if the player already mortgaged at least 1, if so, check whether he/she need to mortgage again or not
		if (GameManager.instance.mortgageCount > 0) 
		{
			canvasScript.MortgageMoneyCalculation (-GameManager.instance.player [canvasScript.currentCharacter].money, mortgagePrice);
			if (!GameManager.instance.proceedToMortgage) 
			{
				return;
			}
		}
		if (GameManager.instance.proceedToMortgage)
		{
			this.gameObject.SetActive (false);
			if ((GameManager.instance.player [canvasScript.currentCharacter].totalAssetAmount <= 0) && (!GameManager.instance.player [canvasScript.currentCharacter].eliminated)) 
			{
				playerIndex = canvasScript.currentCharacter;
				GameManager.instance.player [canvasScript.currentCharacter].money = 0;
				GameManager.instance.proceedToMortgage = false;
				GameManager.instance.mortgageCount = 0;
				GameManager.instance.isMortgage = false;
			} 
			else
			{
				canvasScript.MortgageFunction (int.Parse (canvasScript.cameraFollow.currentPlayer.name.Substring (canvasScript.cameraFollow.currentPlayer.name.Length - 1)) - 1);
				return;
			}
		}
		if (GameManager.instance.useEscapeCard)
		{
			this.gameObject.SetActive (false);
			escapeJailBg.SetActive (true);
			passMsgText.text = "Do you want to use ur escape pass? Owned: " + GameManager.instance.player [playerIndex].escapePass;
			if (GameManager.instance.player [AiComputer.instance.curPlayer].AI) 
			{
				AiComputer.instance.JailEscape (this.gameObject);
			}
			return;
		}
		if (randomChanceScript.isChanceCard)
		{
			if ((randomChanceScript.chanceCardCollector.cardList [randomChanceScript.selectedCardNum].name == "repair village and city") && !GameManager.instance.isDeducted) {
				totalCost = (canvasScript.playerProperties [playerIndex].kampungOwned * 250) + (canvasScript.playerProperties [playerIndex].cityOwned * 500);
				DisplayCard ("You owned " + canvasScript.playerProperties [playerIndex].kampungOwned + " village and " + canvasScript.playerProperties [playerIndex].cityOwned + " city. Total cost: " + totalCost, 
					Color.red, -totalCost, playerIndex);
				GameManager.instance.isDeducted = true;
				return;
			}
			if (randomChanceScript.chanceCardCollector.cardList [randomChanceScript.selectedCardNum].name == "repair city and metropolis" && !GameManager.instance.isDeducted) {
				totalCost = (canvasScript.playerProperties [playerIndex].cityOwned * 500) + (canvasScript.playerProperties [playerIndex].metropolisOwned * 750);
				DisplayCard ("You owned " + canvasScript.playerProperties [playerIndex].cityOwned + " city and " + canvasScript.playerProperties [playerIndex].metropolisOwned + " metropolis. Total cost: " + totalCost, 
					Color.red, -totalCost, playerIndex);
				GameManager.instance.isDeducted = true;
				return;
			}
			randomChanceScript.isChanceCard = false;
		}
		close(); //close the background
		if (GameManager.instance.gameMode == GameManager.GameModeChoices.TurnLimit)
		{
			GameManager.instance.TurnLimitModeChecking ();
		}
		else if (GameManager.instance.gameMode == GameManager.GameModeChoices.Metropolis)
		{
			GameManager.instance.MetropolisModeChecking (canvasScript.currentCharacter);
		}
    }
}