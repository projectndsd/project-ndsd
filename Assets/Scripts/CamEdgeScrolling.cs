﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CamEdgeScrolling : MonoBehaviour 
{
	[Header("Scroll Setting")]
	public int distanceTriggerScrolling;		//distance to trigger edge scrolling
	public float scrollingSpeed;

	[Header("Uneditable")]
	public GameObject theParent;

	private int screenWidth;
	private int screenHeight;
	private Vector3 leftBoundaryVec;
	private Vector3 rightBoundaryVec;
	private Vector3 topBoundaryVec;
	private Vector3 bottomBoundaryVec;

	private Vector3 finalDirection;
	private Vector3 modifiedDirection;
	private CameraFollow camFollow;
	private CamEdgeScrolling camEdgeScrollScript;
	//private float xDirection;
	//private float yDirection;
	private bool camScrolling;

	// Use this for initialization
	void Start () 
	{
		camScrolling = false;
		finalDirection = Vector3.zero;
		screenWidth = Screen.width;
		screenHeight = Screen.height;
		camFollow = this.gameObject.GetComponent<CameraFollow> ();
		camEdgeScrollScript = this.gameObject.GetComponent<CamEdgeScrolling> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		finalDirection = transform.localPosition;
		//check whether the cursor is moving out of the preset trigger boundaries
		if (Input.mousePosition.x > screenWidth - distanceTriggerScrolling) 
		{
			finalDirection.x += scrollingSpeed * Time.deltaTime;
			transform.localPosition = finalDirection;
			camScrolling = true;
		}
		if (Input.mousePosition.x < 0 + distanceTriggerScrolling) 
		{
			finalDirection.x -= scrollingSpeed * Time.deltaTime;
			transform.localPosition = finalDirection;
			camScrolling = true;
		}
		if (Input.mousePosition.y > screenHeight - distanceTriggerScrolling) 
		{
			finalDirection.y += scrollingSpeed * Time.deltaTime;
			transform.localPosition = finalDirection;
			camScrolling = true;
		}
		if (Input.mousePosition.y < 0 + distanceTriggerScrolling)
		{
			finalDirection.y -= scrollingSpeed * Time.deltaTime;
			transform.localPosition = finalDirection;
			camScrolling = true;
		}
		if (!(Input.mousePosition.y < 0 + distanceTriggerScrolling) && !(Input.mousePosition.y > screenHeight - distanceTriggerScrolling)
		    && !(Input.mousePosition.x < 0 + distanceTriggerScrolling) && !(Input.mousePosition.x > screenWidth - distanceTriggerScrolling))
		{
			camScrolling = false;
		}
		//if the camera is scrolling and the camera is still attached to the parent, detach it
		if (camScrolling && (this.gameObject.transform.parent != null))
		{
			DetachParent ();
		}
		//if the camera is not scrolling and it has no parent attaching, attach current player
		else if (!camScrolling && (this.gameObject.transform.parent == null))
		{
			AttachParent ();
		}
	}

	public void DetachParent ()
	{
		this.gameObject.transform.parent = null;
	}

	public void AttachParent ()
	{
		this.gameObject.transform.parent = theParent.transform;
	}
}