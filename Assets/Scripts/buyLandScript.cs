﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyLandScript : MonoBehaviour
{
	[Header ("Setting")]
	public Text buyOrUpgradeLandText;
	public Text notificationMsg;
	public GameObject notificationMsgBg;

	[Header ("Data passing and Debug Purpose")]
	public int playerIndex;
	public int moneyAmount;
	public GameObject currentPlayer;
	public GameObject land;
	public GameObject cameraPreviousPlayer;

	private GameObject retrievedPlaceHolder;
	private GameObject landParticleSystem;
	private GameObject theLandmark;
	private LandData theLandData;
	private float priceRate;
	private bool landBoughtOrUpgraded;
	private bool landmarkIsBuilt;
	private bool landIsRedeemed;
	private string[] splitedName;

    //Below is for the UI script
    CanvasScript canvasScript;

	void Awake ()
	{
        canvasScript = GameObject.Find("UI_canvas").GetComponent<CanvasScript>();
		landBoughtOrUpgraded = false;
	}
	//pop up the buy land or upgrade land message
	public void openBuyOrUpgradeMessage (int playerArrayIndex, GameObject theLand, GameObject thisPlayer)
	{
		this.gameObject.SetActive (true);
		landBoughtOrUpgraded = false;
		landmarkIsBuilt = false;
		landIsRedeemed = false;
		playerIndex = playerArrayIndex;
		land = theLand;
		currentPlayer = thisPlayer;
		theLandData = land.GetComponent<LandData> ();
		if ((GameManager.instance.player [playerArrayIndex].race == GameManager.Race.Malay) && (theLandData.Owner == LandData.owner.Default)) 
		{
			priceRate = GameManager.instance.malayBuyLandRate;
		} 
		else if (GameManager.instance.player [playerArrayIndex].race == GameManager.Race.Chinese)
		{
			priceRate = GameManager.instance.chineseBuyLandRate;
		} 
		else
		{
			priceRate = 1f;
		}
		switch (theLandData.landCurrentStatus) {
		case LandData.landStatus.EmptyLand:
			moneyAmount = (int)((float)theLandData.landPrice * priceRate);
		buyOrUpgradeLandText.text = "Do you want to buy " + theLandData.landName + "? Click No will end your turn. Price: " + moneyAmount.ToString ();
			break;
		case LandData.landStatus.Land:
			moneyAmount = (int)((float)theLandData.kampungPrice * priceRate);
		buyOrUpgradeLandText.text = "Do you want to upgrade " + theLandData.landName + " to village? Click No will end your turn. Price: " + moneyAmount.ToString ();
			break;
		case LandData.landStatus.Kampung:
			moneyAmount = (int)((float)theLandData.cityPrice * priceRate);
		buyOrUpgradeLandText.text = "Do you want to upgrade " + theLandData.landName + " to city? Click No will end your turn. Price: " + moneyAmount.ToString ();
			break;
		case LandData.landStatus.City:
			moneyAmount = (int)((float)theLandData.metropolisPrice * priceRate);
		buyOrUpgradeLandText.text = "Do you want to upgrade " + theLandData.landName + " to metropolis? Click No will end your turn. Price: " + moneyAmount.ToString ();
			break;
		case LandData.landStatus.Metropolis:
			this.gameObject.SetActive (false);
			notificationMsg.GetComponent<Text> ().text = "Your land already upgraded to highest status which is metropolis! No further upgrading needed!";
			notificationMsgBg.SetActive (true);
			break;
		case LandData.landStatus.Mortgaged:
			switch (theLandData.landStatusBeforeMortgage)
			{
			case LandData.landStatus.Land:
				moneyAmount = theLandData.landRedeemPrice;
				break;
			case LandData.landStatus.Kampung:
				moneyAmount = theLandData.villageRedeemPrice;
				break;
			case LandData.landStatus.City:
				moneyAmount = theLandData.cityRedeemPrice;
				break;
			case LandData.landStatus.Metropolis:
				moneyAmount = theLandData.metropolisRedeemPrice;
				break;
			}
			buyOrUpgradeLandText.text = "Your land is mortgaged. Do you want to redeem back? Click No will end your turn. Price: " + moneyAmount.ToString();
			break;
		}
		if (GameManager.instance.player [playerArrayIndex].AI == true) 
		{
			AiComputer.instance.BuyLandDecision (this.gameObject, playerArrayIndex,moneyAmount);
		}
	}
	//close the buy or upgrade land message
	public void closeBuyMessage ()
	{
		
		if (GameManager.instance.gameMode == GameManager.GameModeChoices.TurnLimit)
		{
			GameManager.instance.TurnLimitModeChecking ();
		}
		this.gameObject.SetActive (false);
		currentPlayer.GetComponent<FollowPath> ().moveTheCamera ();
	}
	//if the Player clicked on Yes button, buy or upgrade the land
	public void buyOrUpgradeLand ()
	{	
		
		//check if the player has enough money to buy land or upgrade land
		if (GameManager.instance.player [playerIndex].money >= moneyAmount)
		{
			if (theLandData.landCurrentStatus == LandData.landStatus.Mortgaged)
			{
				landBoughtOrUpgraded = true;
				GameManager.instance.isRedeem = true;
				SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.moneySfx);
				GameManager.instance.TotalAssetAmountCalculation (-moneyAmount, playerIndex);
				GameManager.instance.moneyCalculation (-moneyAmount, playerIndex);
				theLandData.landCurrentStatus = theLandData.landStatusBeforeMortgage;
                switch (theLandData.landCurrentStatus)
                {
                    case LandData.landStatus.Land:
                        canvasScript.playerProperties[canvasScript.currentCharacter].landOwned += 1;
                        break;

                    case LandData.landStatus.Kampung:
                        canvasScript.playerProperties[canvasScript.currentCharacter].kampungOwned += 1;
                        break;
                    case LandData.landStatus.City:
                        canvasScript.playerProperties[canvasScript.currentCharacter].cityOwned += 1;
                        break;
                    case LandData.landStatus.Metropolis:
                        canvasScript.playerProperties[canvasScript.currentCharacter].metropolisOwned += 1;
                        break;
                }
				GameManager.instance.player [playerIndex].mortgagedProperty.Remove (land);
				GameManager.instance.player [playerIndex].property.Add (land);
				GameManager.instance.CalculateLandAsset (land, playerIndex, false);
				int indexOfTheLand = GameManager.instance.normalLandList.IndexOf (land);
				GameManager.instance.mapPieceList [indexOfTheLand].GetComponent<MeshRenderer> ().material.SetColor ("_Color", Color.white);
				land.transform.Find ("LandIndicatorSprite").gameObject.GetComponent<SpriteRenderer>().sprite = GameManager.instance.indicatorList [playerIndex + 1];	//change the indicator from bank to new owner
				landIsRedeemed = true;
				GameManager.instance.MultiplierChecking (theLandData.landName, playerIndex + 1);
				openNotificationMessage ();
			} 
			else 
			{
				SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.moneySfx);
				GameManager.instance.TotalAssetAmountCalculation (-moneyAmount, playerIndex);
				GameManager.instance.moneyCalculation (-moneyAmount, playerIndex);
				land.GetComponent<LandData> ().Owner = (LandData.owner)(playerIndex + 1);	//update the owner to current Player
				int currentLandStatusNum = (int)theLandData.landCurrentStatus;
				LandData.landStatus newStatus = (LandData.landStatus)(currentLandStatusNum + 1);	//update the land status

				//if the land is new bought, then add it into player's property list
				if (newStatus == LandData.landStatus.Land)
				{
					GameManager.instance.player [playerIndex].property.Add (land);
				}

				theLandData.landCurrentStatus = newStatus;
				GameManager.instance.CalculateLandAsset (land, playerIndex, false);

				//if the current land status is kampung, city or metropolis, activate the particle system before building it
				if ((theLandData.landCurrentStatus == LandData.landStatus.Kampung) || (theLandData.landCurrentStatus == LandData.landStatus.City) || (theLandData.landCurrentStatus == LandData.landStatus.Metropolis)) 
				{
					landParticleSystem = land.transform.Find ("Particle System").gameObject;
					if (theLandData.landCurrentStatus == LandData.landStatus.Kampung) 
					{
						SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.buildToVillageSfx);
					} 
					else
					{
						SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.buildSfx);
					}
					GameManager.instance.upgradeProcess (landParticleSystem, this.gameObject.GetComponent<BuyLandScript> ());
				} 
				else if (theLandData.landCurrentStatus == LandData.landStatus.Land)
				{
					LandChecking ();
				}
			}
		} 
		else
		{
			openNotificationMessage ();
		}
		this.gameObject.SetActive (false);
	}
	//update the land data based on races and check the land for state multiplier and spawning landmark
	public void LandChecking ()
	{
		landBoughtOrUpgraded = true;
		switch (theLandData.landCurrentStatus)
		{
		case LandData.landStatus.Land:
			int indexOfBoughtLand = GameManager.instance.normalLandList.IndexOf (land);
			GameManager.instance.mapPieceList [indexOfBoughtLand].GetComponent<MeshRenderer> ().material.SetColor ("_Color", Color.white);	//change the map to bright color indicates the land is bought
			land.transform.Find ("LandIndicatorSprite").gameObject.GetComponent<SpriteRenderer>().sprite = GameManager.instance.indicatorList [playerIndex + 1];	//change the indicator from bank to new owner
			//check for state multiplier (only when buy land)
			GameManager.instance.MultiplierChecking (theLandData.landName, playerIndex + 1);
			if (GameManager.instance.player [playerIndex].race == GameManager.Race.Chinese)
			{
				theLandData.landRental = (int)((float)theLandData.landRental * GameManager.instance.chineseRentalRate);
				theLandData.kampungRental = (int)((float)theLandData.kampungRental * GameManager.instance.chineseRentalRate);
				theLandData.cityRental = (int)((float)theLandData.cityRental * GameManager.instance.chineseRentalRate);
				theLandData.metropolisRental = (int)((float)theLandData.metropolisRental * GameManager.instance.chineseRentalRate);
			}
			//check for the land that bought by indian player, if the land is under the bonus state, add extra money
			else if (GameManager.instance.player [playerIndex].race == GameManager.Race.Indian)
			{
				splitedName = land.GetComponent<LandData> ().landName.Split (' ');	//split the name for state checking
				foreach (string stateName in GameManager.instance.indianBonusLandState)
				{
					if (splitedName [splitedName.Length - 1] == stateName) 
					{
						currentPlayer.GetComponent<FollowPath> ().extraBonusEachRound += GameManager.instance.indianExtraBonusForEachMamakLand;
						break;
					}
				}
			}
			openNotificationMessage ();
			canvasScript.BuildLand (playerIndex);
			break;
		case LandData.landStatus.Kampung:
			land.transform.Find ("BuildingSprite").gameObject.GetComponent<SpriteRenderer> ().sprite = GameManager.instance.buildingList [0];
			canvasScript.BuildKampung (playerIndex);
			break;
		case LandData.landStatus.City:
			land.transform.Find ("BuildingSprite").gameObject.GetComponent<SpriteRenderer> ().sprite = GameManager.instance.buildingList [1];
			canvasScript.BuildCity (playerIndex);
			break;
		case LandData.landStatus.Metropolis:
			land.transform.Find ("BuildingSprite").gameObject.GetComponent<SpriteRenderer> ().sprite = GameManager.instance.buildingList [2];
			//for building landmarks
			splitedName = theLandData.landName.Split (' ');
			foreach (State state in GameManager.instance.stateMultiplerScript.landClassification)
			{
				if (splitedName [splitedName.Length - 1] == state.stateName)
				{
					for (int i = 0; i < state.landList.Count; i++) 
					{
						if (state.landList [i].gameObject.GetComponent<LandData> ().landCurrentStatus != LandData.landStatus.Metropolis)
						{
							break;
						}
						if (i == state.landList.Count - 1) 
						{
							foreach (GameObject landmark in GameManager.instance.landmarkList)
							{
								if (landmark.name == state.stateName)
								{
									GameManager.instance.buildLandmark = true;
									theLandmark = landmark;
								}
							}
						}
					}
				}
			}
			canvasScript.BuildMetropolis (playerIndex);
			break;
		}
	}
	public void ActiveLandmark (GameObject particle, BuyLandScript theScript)
	{
		particle.transform.position = new Vector3 (theLandmark.transform.position.x, theLandmark.transform.position.y - 0.129f, theLandmark.transform.position.z);
		SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.buildSfx);
		GameManager.instance.upgradeProcess (particle, theScript);
	}
	//activate the landmark
	public void LandmarkChecking ()
	{
		theLandmark.SetActive (true);
		GameManager.instance.buildLandmark = false;
		landmarkIsBuilt = true;
	}
	//to tell the Player whether success or fail to buy the land
	public void openNotificationMessage ()
	{
		if (this.gameObject.activeInHierarchy) 
		{
			this.gameObject.SetActive (false);
		} 
		//check if the player already bought the land
		if (!landBoughtOrUpgraded)
		{
			notificationMsg.GetComponent<Text> ().text = "You do not have enough money to buy or upgrade the land :(";
		}
		else if (theLandData.landCurrentStatus == LandData.landStatus.Land && !landIsRedeemed)
		{
			notificationMsg.GetComponent<Text> ().text = "Player " + (playerIndex + 1).ToString () + " purchased the land successfully!"; 
		}
		else
		{
			if (landmarkIsBuilt) 
			{
				notificationMsg.GetComponent<Text> ().text = "Landmark of the state, " + theLandmark.GetComponent<SpriteRenderer> ().sprite.name + " is built!";
				landmarkIsBuilt = false;
			}
			else if (landIsRedeemed)
			{
				notificationMsg.GetComponent<Text> ().text = "Player " + (playerIndex + 1).ToString () + " redeemed the land successfully!";
				landIsRedeemed = false;
			} 
			else
			{
				notificationMsg.GetComponent<Text> ().text = "Player " + (playerIndex + 1).ToString () + " upgraded the land successfully!";
			}
		}
		notificationMsgBg.SetActive (true);
	}
	//close the notification message if the Player clicked on ok button
	public void closeNotificationMessage ()
	{
		
		if (GameManager.instance.buildLandmark) 
		{
			GameManager.instance.moveToLandmark = true;
			CameraFollow cameraFollowScript = GameObject.Find ("CameraTarget").GetComponent<CameraFollow> ();
			cameraFollowScript.enabled = true;
			cameraPreviousPlayer = cameraFollowScript.currentPlayer;
			cameraFollowScript.currentPlayer = theLandmark;
		} 
		else
		{
			if (GameManager.instance.gameMode == GameManager.GameModeChoices.TurnLimit)
			{
				GameManager.instance.TurnLimitModeChecking ();
			}
			else if (GameManager.instance.gameMode == GameManager.GameModeChoices.Metropolis)
			{
				GameManager.instance.MetropolisModeChecking (int.Parse (currentPlayer.name.Substring (currentPlayer.name.Length - 1)) - 1);
			}
			currentPlayer.GetComponent<FollowPath> ().moveTheCamera ();
		}
		notificationMsgBg.SetActive (false);
	}
}