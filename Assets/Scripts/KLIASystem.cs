﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KLIASystem : MonoBehaviour
{
	public string buttonName;
	public int fee;

	private bool firstEnable = true;

	void OnEnable ()
	{
		if (!firstEnable) 
		{
			//check to only show the locations that the player ables to reach based on the money amount owned
			if (GameManager.instance.player [int.Parse (GameManager.instance.theCurrentPlayer.name.Substring (GameManager.instance.theCurrentPlayer.name.Length - 1)) - 1].money < fee) 
			{
				this.gameObject.SetActive (false);
			} 
		} 
		else 
		{
			firstEnable = false;
		}
	}
	public void FlyToSelectedLand ()
	{
		GameManager.instance.theSelectedLandName = buttonName;
		GameManager.instance.kliaFees = fee;
		GameManager.instance.DeactivateKLIABg ();
	}
}