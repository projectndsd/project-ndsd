﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QNAScript : MonoBehaviour 
{
	[Header("Setting")]
	public Text questionText;
	public Text countDownText;
	public int countDownDuration;
	public GameObject resultUI;
	public List<Text> answerButtonText;

	private int countDownTime;
	private int fees;
	private Text displayResultText;
	private RandomChance randomChanceScript;

	void Start ()
	{
		displayResultText = resultUI.transform.Find ("ChanceCardMessage").gameObject.GetComponent<Text> ();
		randomChanceScript = GameObject.Find ("Card Manager").GetComponent<RandomChance> ();
	}

	void OnEnable ()
	{
		SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.qnaCountDownSfx);
		countDownTime = countDownDuration;
		StartCoroutine (CountDown());
	}

	IEnumerator CountDown ()
	{
		countDownText.text = countDownTime.ToString ();
		yield return new WaitForSeconds (1f);
		countDownTime--;

		//if the time is 0, display deduct money message and stop the countdown
		if (countDownTime <= 0) 
		{
			SoundScript.instance.audioSrc.Stop ();
			SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.sadSfx);
			if (randomChanceScript.ferryType == LandData.landType.Ferry1)
			{
				fees = randomChanceScript.ferryFee1;
			}
			else
			{
				fees = randomChanceScript.ferryFee2;
			}
			resultUI.SetActive (true);
			displayResultText.text = "Time's out! And you failed to answer... Pay " + fees.ToString ();
			displayResultText.color = Color.red;
			SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.moneySfx);
			GameManager.instance.TotalAssetAmountCalculation (-fees, randomChanceScript.playerIndexArray);
			GameManager.instance.moneyCalculation (-fees, randomChanceScript.playerIndexArray);
			StopCoroutine (CountDown ());
			this.gameObject.SetActive (false);
		}
		else 
		{
			yield return StartCoroutine (CountDown ());
		}
	}
}