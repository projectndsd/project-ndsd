﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StepOnPlayerScript : MonoBehaviour 
{
	[Header ("Setting")]
	public Text stepOnOtherPlayerMessage;

	private GameObject thePlayer;
	private CanvasScript canvasScript;
	private ChanceScript chanceScript;

	void Start ()
	{
		canvasScript = GameObject.Find ("UI_canvas").GetComponent<CanvasScript> ();
		chanceScript = GameObject.Find ("Canvas").transform.Find ("ChanceCardBackground").gameObject.GetComponent<ChanceScript> ();
	}
	//pop up the message to the Player as warning or notification about stepping on other Player's land
	public void openMessage (GameObject currentPlayer, LandData.owner landOwner, GameObject land)
	{
		this.gameObject.SetActive (true);
		thePlayer = currentPlayer;
		int amount = 0;
		int whichPlayer = int.Parse (currentPlayer.name.Substring (currentPlayer.name.Length - 1));	//to get the Player number
		int theOwner = int.Parse (landOwner.ToString().Substring (landOwner.ToString().Length - 1)); //to get the owner number

		LandData currentLandData = land.GetComponent<LandData> ();
		//determine the rental type based on the land type
		switch (currentLandData.landCurrentStatus)
		{
		case LandData.landStatus.Land:
			amount = currentLandData.landRental;
			break;
		case LandData.landStatus.Kampung:
			amount = currentLandData.kampungRental;
			break;
		case LandData.landStatus.City:
			amount = currentLandData.cityRental;
			break;
		case LandData.landStatus.Metropolis:
			amount = currentLandData.metropolisRental;
			break;
		}
		SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.moneySfx);
		GameManager.instance.TotalAssetAmountCalculation (-amount, whichPlayer - 1);
		GameManager.instance.TotalAssetAmountCalculation (amount, theOwner - 1);
		GameManager.instance.moneyCalculation (-amount, whichPlayer - 1);	//deduct money from the stepped Player
		GameManager.instance.moneyCalculation (amount, theOwner - 1);		//add money to the owner

		stepOnOtherPlayerMessage.text = "Player " + whichPlayer + " stepped on the land that belongs to Player " + ((int)landOwner).ToString() + ". Pay " + amount;
		stepOnOtherPlayerMessage.color = Color.red;
	}
	//close the message by clicking on OK button
	public void closeMessage ()
	{
		
		this.gameObject.SetActive (false);
		chanceScript.currentPlayer = thePlayer;
		chanceScript.playerIndex = int.Parse (thePlayer.name.Substring (thePlayer.name.Length - 1)) - 1;
		chanceScript.closeChanceCard();
	}
}