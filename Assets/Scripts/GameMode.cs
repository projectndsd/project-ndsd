﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMode : MonoBehaviour
{
    //public void OnChinese()
    //{
	//	GameManager.instance.playerRace = GameManager.Race.chinese;
    //}
	//
    //public void OnIndian()
    //{
	//	GameManager.instance.playerRace = GameManager.Race.indian;
	//
    //}
	//
    //public void OnMalay()
    //{
	//	GameManager.instance.playerRace = GameManager.Race.malay;
    //}
	//
    //public void OnGameMode()
    //{
    //    Debug.Log("Game Mode Button Clicked");
    //}

    public void OnStartGame()
    {
		GameManager.instance.startGameButton();
    }

	public void OnBackButton()
	{
		GameManager.instance.BackMain();
	}

	public void OnMenuStartGame()
	{
		GameManager.instance.onStartButton ();
	}

	public void OnSettingButton()
	{
		GameManager.instance.onSettingButton ();
	}

	public void OnCreditButton()
	{
		GameManager.instance.onCreditButton ();
	}
}