﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AiComputer : MonoBehaviour {

	public static AiComputer instance;
	public int playerTotalLand;
	public int selectedPlayer;
	public int[] zeroSumForAI;
	public TargetForAI aiTarget;
	public CanvasScript canvas;
	public List<int> playerSelectionPlayerNum;
	public List<float> selectedPlayerLandPrice;
	public List<GameObject> playerSelectionButton;
	public List<GameObject> playerLandButton;
	public int curPlayer;
	public int negolandPrice;
	public bool move;
	public bool BuySell;
	public int originalPrice;
	public List<Button> kliaButton;
	public List<Button> qna;

	public struct TargetForAI
	{
		public int playerNumber;
		public int losingPoint;
		public bool losingMoney;
		public bool losingProperty;
		public bool losingTotalAsset;
		public bool losingSteps;
	}

	void Awake () 
	{
		if (instance == null) 
		{
			instance = this;
		} 
		else if (instance != this) 
		{
			Destroy (gameObject);
		}
		zeroSumForAI = new int[GameManager.instance.maxPlayer];
		canvas = GameObject.Find ("UI_canvas").GetComponent<CanvasScript> ();
	}

	public IEnumerator GetData(int currentPlayer)
	{
		curPlayer = currentPlayer;
		for (int i = 0; i < GameManager.instance.maxPlayer; i++) 
		{
			if (i == currentPlayer) 
			{
				continue;
			}

			zeroSumForAI[i] = GameManager.instance.ZeroSumCalculation (currentPlayer,i);

			if (aiTarget.losingPoint < zeroSumForAI [i]) 
			{
				aiTarget.playerNumber = i;
				aiTarget.losingPoint = zeroSumForAI [i];
				GameManager.instance.ZeroSumCalculation (currentPlayer,i);
			}

			Debug.Log ("Player" + currentPlayer + " AI zero sum with Player" + i + " : " + zeroSumForAI[i]); 
		}
		yield return new WaitForSeconds (0.05f);
		BuyRoll (currentPlayer);
	}

	public void BuyRoll(int currentPlayer)
	{
		int chanceDecision = Random.Range (0, 41) + Random.Range (0, 41) + Random.Range (0, 31);

		if (GameManager.instance.player [currentPlayer].money >= 10000 
			&& ((!aiTarget.losingMoney && aiTarget.losingProperty) || !aiTarget.losingTotalAsset) 
			&& (GameManager.instance.player [curPlayer].completeRound > 2) && chanceDecision >= 80) 
		{
			BuySell = true;
			ChoosePlayer (currentPlayer, false);
		} 
		else if (aiTarget.losingMoney && aiTarget.losingProperty && aiTarget.losingTotalAsset && chanceDecision <= 10) 
		{
			BuySell = true;
			ChoosePlayer (currentPlayer, true);
		}
		else 
		{
			if (move) 
			{
				TestScript.instance.rollDices ();
			}
		}
	}

	public void ChoosePlayer(int currentPlayer, bool sell)
	{
		int playerNum = 0;
		ClearAllList ();
		do 
		{
			playerNum = Random.Range(0,GameManager.instance.maxPlayer);
			if(playerNum == aiTarget.playerNumber && GameManager.instance.maxPlayer > 2)
			{
				if(aiTarget.losingTotalAsset)
				{
					playerNum = currentPlayer;
				}
			}
		} while(playerNum == currentPlayer);

		if (sell) 
		{
			canvas.SellFunction ();
		}
		else
		{
			canvas.BuyFunction ();
		}
		StartCoroutine(CheckingButton (playerNum));
	}

	public void AutoBuy(BuyLandScript buyLandUI,int playerNumber, GameObject landObject, GameObject aiGameObject)
	{
			buyLandUI.openBuyOrUpgradeMessage (playerNumber, landObject, aiGameObject);
	}

	public void BuyLandDecision(GameObject buyLandObject, int playerNum, int landPrice)
	{
		if ((GameManager.instance.player [playerNum].money - landPrice) >= 3000 ) 
		{
			buyLandObject.GetComponent<BuyLandScript> ().buyOrUpgradeLand ();
		} 
		else 
		{
			Debug.Log ("Close");
			buyLandObject.GetComponent<BuyLandScript> ().closeBuyMessage ();
		}
	}

	public IEnumerator CloseMessageAction(GameObject notificationMsgBgd)
	{
		yield return new WaitForSeconds(3f);
		notificationMsgBgd.SetActive (false);
	}

	public void GetPlayerSelection(GameObject passInButton, int playerNum)
	{
		playerSelectionButton.Add (passInButton);
		playerSelectionPlayerNum.Add (playerNum);
	}

	public void GetPlayerLandSelection(GameObject passInButton, LandData property)
	{
		playerLandButton.Add (passInButton);
		int moneyAmount = 0;
		float priceRate = 1;

		if (GameManager.instance.player [selectedPlayer].race == GameManager.Race.Malay) 
		{
			priceRate = GameManager.instance.malayBuyLandRate;
		} 
		else if (GameManager.instance.player [selectedPlayer].race == GameManager.Race.Chinese)
		{
			priceRate = GameManager.instance.chineseBuyLandRate;
		} 

		switch (property.landCurrentStatus) 
		{
		case LandData.landStatus.Land:
			moneyAmount = (int)((float)property.landPrice * priceRate);
			break;
		case LandData.landStatus.Kampung:
			moneyAmount =  (int)((float)property.kampungPrice * priceRate);
			break;
		case LandData.landStatus.City:
			moneyAmount =  (int)((float)property.cityPrice * priceRate);
			break;
		case LandData.landStatus.Metropolis:
			moneyAmount =  (int)((float)property.metropolisPrice * priceRate);
			break;
		}
		Debug.Log ("Price : " + moneyAmount);
		selectedPlayerLandPrice.Add(moneyAmount);
	}
		
	public void GetPlayerLandNumber(int maxNumLand)
	{
		playerTotalLand = maxNumLand;
	}

	//Checking button in player list
	public IEnumerator CheckingButton(int playerNum)
	{
		if (playerSelectionButton.Count == 0) 
		{
			yield return new WaitForSeconds (0.05f);
			StartCoroutine (CheckingButton (playerNum));
		}
		else
		{
			for (int i = 0; i < playerSelectionPlayerNum.Count; i++) 
			{
				Debug.Log ("Checking");
				Debug.Log ("PlayerNum : " + playerNum + " : playerSelectionPlayerNum [i] : " + playerSelectionPlayerNum [i]);
				if (playerSelectionPlayerNum [i] == playerNum) 
				{
					Debug.Log ("Player" + (playerSelectionPlayerNum [i] + 1) + " is choosed");
					selectedPlayer = i;
					yield return new WaitForSeconds (0.05f);
					playerSelectionButton [i].GetComponent<PlayerSelectionCheck> ().Checking ();
				}
			}
		}
	}

	//get the List of property of player
	public void FinishGetPlayerProperty()
	{
		if(GameManager.instance.player[curPlayer].money > 0)
		{
			if (playerTotalLand > 0) 
			{
				int randomLand = Random.Range (0, playerTotalLand);
				float rate = RandomRate();

				while (GameManager.instance.player [curPlayer].money < (selectedPlayerLandPrice [randomLand] * rate)) 
				{
					rate = RandomRate ();
				}

				if ((GameManager.instance.player [curPlayer].totalAssetAmount + (selectedPlayerLandPrice [randomLand] * rate)) > (GameManager.instance.player [selectedPlayer].totalAssetAmount - (selectedPlayerLandPrice [randomLand] * rate))) 
				{
					negolandPrice = (int)selectedPlayerLandPrice [randomLand] - (int)(selectedPlayerLandPrice [randomLand] * rate);
					StartCoroutine (WaitMortgageButton (randomLand));
				} 
				else 
				{
					selectedPlayerLandPrice.RemoveAt (randomLand);
					playerTotalLand--;
					FinishGetPlayerProperty ();
				}
			}
			else
			{
				canvas.CloseMortgageList ();
				TestScript.instance.rollDices ();
			}
		}
		else
		{
			int randomLand = Random.Range (0, playerTotalLand);
			StartCoroutine (WaitMortgageButton (randomLand));
		}
	}

	//random rate for bot to enter the final price
	public float RandomRate()
	{
		float rate = 0f;
		switch (aiTarget.losingPoint) 
		{
		case 1: 
			rate = Random.Range(-0.3f,0.3f);
			break;
		case 2:
			rate = Random.Range(-0.4f,0.4f);
			break;
		case 3:
			rate = Random.Range(-0.5f,0.5f);
			break;
		default:
			rate = Random.Range(-0.1f,0.5f);
			break;
		}

		return rate;
	}

	//wait mortage button and click
	public IEnumerator WaitMortgageButton(int randomLand)
	{
		yield return new WaitForSeconds(0.05f);
		playerLandButton [randomLand].GetComponent<MortgageCheck> ().Checking ();
	}

	//call for the negotiate input price function
	public void DelayForPriceNegoBuySell(bool sell, GameObject priceNegoBg)
	{
		StartCoroutine(PriceNegoBuySell (sell, priceNegoBg));
	}

	//auto input negotiate price
	public IEnumerator PriceNegoBuySell(bool sell, GameObject priceNegoBg)
	{
		InputField inputField = priceNegoBg.transform.Find ("InputField").gameObject.GetComponent<InputField> ();
		yield return new WaitForSeconds(0.05f);
		if (sell) 
		{
			Debug.Log ("Sell land to Player with price : " + negolandPrice);
			inputField.text = negolandPrice.ToString ();
			yield return new WaitForSeconds(0.05f);
			priceNegoBg.GetComponent<BuySellLand> ().ConfirmMsgForSeller ();
		} 
		else 
		{
			Debug.Log ("Buy Player's land with price : " + negolandPrice);
			inputField.text = negolandPrice.ToString ();
			yield return new WaitForSeconds(0.05f);
			priceNegoBg.GetComponent<BuySellLand> ().ConfirmMsgForSeller ();
		}
		ClearAllList ();
	}

	public void ClearAllList()
	{
		playerSelectionButton.Clear ();
		playerSelectionPlayerNum.Clear ();
		selectedPlayerLandPrice.Clear ();
		playerLandButton.Clear ();
	}
	//call confirm negotiate button
	public void DelayForConfirmNegoBuySell(GameObject buySellLand,bool sell, string price, int sellerbuyer, int botNum)
	{
		StartCoroutine(ConfirmNegoBuySell (buySellLand, sell, price, sellerbuyer, botNum));
	}

	//wait message pop out and click confirm else go analyse player input price
	public IEnumerator ConfirmNegoBuySell(GameObject buySellLand, bool sell, string price, int sellerbuyer, int botNum)
	{
		yield return new WaitForSeconds(0.05f);
		if (BuySell) 
		{
			if (sell) 
			{
				buySellLand.GetComponent<BuySellLand> ().BuySellLandFunction ();
			} 
			else 
			{
				buySellLand.GetComponent<BuySellLand> ().ConfirmMsgForBuyer();
			}
			BuySell = false;
		} 
		else 
		{
			int inputPrice = int.Parse (price);
			if (sell) 
			{
				AnalysePrice (buySellLand, inputPrice, sellerbuyer, sell, botNum);
			} 
			else 
			{
				AnalysePrice(buySellLand, inputPrice, sellerbuyer, sell, botNum);
			}
		}
	}

	//call cancel Negotiate function
	public void DelayForCancelNegoBuySell(GameObject buySellLand)
	{
		StartCoroutine (CancelNegoBuySell (buySellLand));
	}

	//Wait Message popup and click cancel for Negotiate
	public IEnumerator CancelNegoBuySell(GameObject buySellLand)
	{
		yield return new WaitForSeconds(0.05f);
		buySellLand.GetComponent<BuySellLand> ().CloseNego ();
	}

	//Analyse Player input price
	public void AnalysePrice(GameObject buySellLand, int price, int sellerbuyer, bool sell, int botNum)
	{
		float randomRate = 0f;
		if (sell) 
		{
			Debug.Log ("Bot Buy");
			if (sellerbuyer == aiTarget.playerNumber) 
			{
				if (aiTarget.losingTotalAsset) 
				{
					randomRate = Random.Range (0.7f, 1.2f);
					Debug.Log ("Ai Target Player");
					if ((price < (int)((float)originalPrice * randomRate)) && price < GameManager.instance.player[botNum].money)
					{
						BuySell = true;
						DelayForConfirmNegoBuySell (buySellLand, sell, price.ToString (), sellerbuyer, botNum);
					} 
					else 
					{
						DelayForCancelNegoBuySell (buySellLand);
					}
				}
				else
				{
					randomRate = Random.Range (0.7f, 1.5f);
					if ((price < (int)((float)originalPrice * randomRate))  && price < GameManager.instance.player[botNum].money)
					{
						BuySell = true;
						DelayForConfirmNegoBuySell (buySellLand, sell, price.ToString (), sellerbuyer, botNum);
					} 
					else 
					{
						DelayForCancelNegoBuySell (buySellLand);
					}
				}
			}
			else
			{
				randomRate = Random.Range (0.8f, 1.3f);
				Debug.Log ("Not Ai Target Player");
				if ((price > (int)((float)originalPrice * randomRate))  && price < GameManager.instance.player[botNum].money)
				{
					BuySell = true;
					DelayForConfirmNegoBuySell (buySellLand, sell, price.ToString (), sellerbuyer, botNum);
				} 
				else 
				{
					DelayForCancelNegoBuySell (buySellLand);
				}
			}
		}
		else
		{
			Debug.Log ("Bot sell");
			if (sellerbuyer == aiTarget.playerNumber) 
			{
				if (aiTarget.losingTotalAsset) 
				{
					randomRate = Random.Range (1.2f, 1.5f);
					Debug.Log ("Ai Target Player");
					if (price > (int)((float)originalPrice * randomRate)) 
					{
						BuySell = true;
						DelayForConfirmNegoBuySell (buySellLand, sell, price.ToString (), sellerbuyer, botNum);
					} 
					else 
					{
						DelayForCancelNegoBuySell (buySellLand);
					}
				}
				else
				{
					randomRate = Random.Range (0.8f, 1.3f);
					if (price > (int)((float)originalPrice * randomRate)) 
					{
						BuySell = true;
						DelayForConfirmNegoBuySell (buySellLand, sell, price.ToString (), sellerbuyer, botNum);
					} 
					else 
					{
						DelayForCancelNegoBuySell (buySellLand);
					}
				}
			}
			else
			{
				Debug.Log ("Not Ai Target Player");
				randomRate = Random.Range (0.8f, 1.3f);
				if (price > (int)((float)originalPrice * randomRate)) 
				{
					BuySell = true;
					DelayForConfirmNegoBuySell (buySellLand, sell, price.ToString (), sellerbuyer, botNum);
				} 
				else 
				{
					DelayForCancelNegoBuySell (buySellLand);
				}
			}
		}

		Debug.Log ("Bot Buy : " + sell);
		Debug.Log ("original price : " + originalPrice);
		Debug.Log ("Ideal price : " + (int)((float)originalPrice * randomRate) + " || rate : " + (randomRate * 100));
	}

	public void DecForKLIA()
	{
		if (GameManager.instance.player [curPlayer].completeRound >= 2) 
		{
			for (int i = 0; i < kliaButton.Count; i++) 
			{
				for (int j = 0; j < GameManager.instance.player [curPlayer].property.Count; j++) 
				{
					if (kliaButton [i].name == GameManager.instance.player [curPlayer].property [j].name) 
					{
						if ((kliaButton [i].GetComponent<KLIASystem> ().fee < GameManager.instance.player [curPlayer].money) 
							&& ((GameManager.instance.player [curPlayer].money - kliaButton [i].GetComponent<KLIASystem> ().fee) > 3000))
						{
							kliaButton [i].GetComponent<KLIASystem> ().FlyToSelectedLand ();
							break;
						}
					}
				}
				if (i == kliaButton.Count) 
				{
					canvas.CloseKLIAFunction ();
				}
			}
		}
		else
		{
			canvas.CloseKLIAFunction ();
		}
	}

	public IEnumerator DecGenting(GameObject gentingBG)
	{
		yield return new WaitForSeconds (0.05f);
		int randomGamble = Random.Range (0, 41) + Random.Range (0, 41) + Random.Range (0, 31);
		if ((randomGamble < 80 || randomGamble != 0) || GameManager.instance.player[curPlayer].money < 15000)
		{
			Debug.Log ("Gamble chance : " + randomGamble);
			gentingBG.GetComponent<GentingScript> ().GetSmallCard ();
		} 
		else 
		{
			gentingBG.GetComponent<GentingScript> ().GetBigCard ();
		}
	}

	public IEnumerator QNA()
	{
		yield return new WaitForSeconds (0.05f);
		qna [Random.Range (0, qna.Count)].GetComponent<QNAAnswerScript> ().CheckAnswer ();
		qna.Clear ();
	}

	public void JailEscape(GameObject chanceCardBG)
	{
		StartCoroutine (EscapeFromJail(chanceCardBG));
	}

	public IEnumerator EscapeFromJail(GameObject chanceCardBG)
	{
		yield return new WaitForSeconds (0.05f);
		chanceCardBG.GetComponent<ChanceScript> ().UseEscapePassFunction ();
	}

	public void Update()
	{
			//if(Input.GetKeyDown(KeyCode.Keypad0))
			//{
			//	Debug.Log ("Player : " + aiTarget.playerNumber);
			//	Debug.Log("Losing Point : " + aiTarget.losingPoint);
			//	Debug.Log("Losing Money : " + aiTarget.losingMoney);
			//	Debug.Log("Losing Property : " + aiTarget.losingProperty);
			//	Debug.Log("Losing Steps : " + aiTarget.losingSteps);
			//	Debug.Log("Losing total asset : " + aiTarget.losingTotalAsset);
			//}
			//	
			//if(Input.GetKeyDown(KeyCode.Keypad1))
			//{
			//	if (move) 
			//	{
			//		move = false;
			//		Debug.Log ("Disable move");
			//	} 
			//	else 
			//	{
			//		move = true;
			//		Debug.Log ("Enable move");
			//	}
			//}
			//
			//if(Input.GetKeyDown(KeyCode.Keypad2))
			//{
			//	BuyRoll (curPlayer);
			//}
			//
			//if(Input.GetKeyDown(KeyCode.Keypad3))
			//{
			//	GameManager.instance.player [curPlayer].money += 1000;
			//	GameManager.instance.player [curPlayer].totalAssetAmount += 1000;
			//	Debug.Log ("Player " + curPlayer + " money : " + GameManager.instance.player [curPlayer].money);
			//	Debug.Log ("Player " + curPlayer + " total asset amount : " + GameManager.instance.player [curPlayer].totalAssetAmount);
			//}
			//
			//if(Input.GetKeyDown(KeyCode.Keypad4))
			//{
			//	if(GameManager.instance.player [curPlayer].AI)
			//	{
			//		GameManager.instance.player [curPlayer].money -= 1000;
			//		GameManager.instance.player [curPlayer].totalAssetAmount -= 1000;
			//		Debug.Log ("Player " + curPlayer + " money : " + GameManager.instance.player [curPlayer].money);
			//		Debug.Log ("Player " + curPlayer + " total asset amount : " + GameManager.instance.player [curPlayer].totalAssetAmount);
			//	}
			//}
			//
			//if(Input.GetKeyDown(KeyCode.Keypad5))
			//{
			//	BuySell = true;
			//	Debug.Log ("auto choose random player's land and buy");
			//	ChoosePlayer (curPlayer, false);
			//}
			//
			//if(Input.GetKeyDown(KeyCode.Keypad6))
			//{
			//	BuySell = true;
			//	Debug.Log ("auto choose random player and sell land");
			//	ChoosePlayer (curPlayer, true);
			//}
	}
		
}
