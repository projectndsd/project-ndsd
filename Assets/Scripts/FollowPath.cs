﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FollowPath : MonoBehaviour
{
	public GameObject buyMessageBg;
	public GameObject stepOnOtherPlayerBg;
	public GameObject gentingSelectionBg;
	public GameObject FOCRentalAndThemeParkMsg;
	public GameObject kliaBg;
    public TestScript test;
	public GameObject nextTarget;

    public int CurrentWayPointID = 0;
    public float speed;
    private float reachDistance = 0.00001f;
    public float rotationSpeed = 5.0f;
    public string pathName;
    public int steps = 0;
	public int returnToGameTurn;
	public bool isJailed = false;
	public bool isInThemePark = false;
	public int totalElapsedSteps;
	public int extraBonusEachRound;	//access from other scripts to assign value

    GameObject Manager;
    GameObject randomChance;
    Vector3 lastPos;
    Vector3 curPos;
	BuyLandScript buyMessageUI;
	StepOnPlayerScript stepOnOtherLand;
	GentingScript gentingSelectionScript;
	CameraFollow theCamera;
	int totalPathObjs;
	int nextStepsToFinishRound;
	public Animator anim;

    void Start()
    {
		extraBonusEachRound = 0;
		totalElapsedSteps = 0;
		totalPathObjs = test.path_objs.Count;
		nextStepsToFinishRound = totalPathObjs;
        lastPos = transform.position;
        randomChance = GameObject.Find("Card Manager");
		GameObject theCanvas = GameObject.Find ("Canvas");
		FOCRentalAndThemeParkMsg = theCanvas.transform.Find ("ChanceCardBackground").gameObject;
		buyMessageBg = theCanvas.transform.Find ("BuyLandBackground").gameObject;
		stepOnOtherPlayerBg = theCanvas.transform.Find ("StepOnOtherPlayer").gameObject;
		gentingSelectionBg = theCanvas.transform.Find ("GentingSelection").gameObject;
		kliaBg = theCanvas.transform.Find ("KLIABackground").gameObject;
		buyMessageUI = buyMessageBg.GetComponent<BuyLandScript> ();
		stepOnOtherLand = stepOnOtherPlayerBg.GetComponent<StepOnPlayerScript> ();
		gentingSelectionScript = gentingSelectionBg.GetComponent<GentingScript> ();
		theCamera = GameObject.Find ("CameraTarget").GetComponent<CameraFollow> ();
    }
    public IEnumerator move()
    {
		CurrentWayPointID++;
		steps--;
		if (CurrentWayPointID >= totalPathObjs) //if last waypoint
		{
			CurrentWayPointID %= totalPathObjs;                      //next become initial waypoint
		}
		CheckDirection();
		if (transform.GetChild (0).GetComponent<CharacterAnimation> ().IsOnSea () && CurrentWayPointID != 0) 
		{
			SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.ferrySfx);

		}
		//=======================BELOW IS THE ANIMATION/MOVEMENT OF THE PLAYER BASED ON THE DICE RESULT==============================================================
        do
        {
            yield return new WaitForEndOfFrame(); //same as Update(), without this the game might hang for awhile
            float distance = Vector3.Distance(test.path_objs[CurrentWayPointID].position, transform.position);  //distance between current position and next waypoint
			//Debug.Log(Mathf.Rad2Deg * Vector3.Angle(test.path_objs[CurrentWayPointID].position,transform.position));
            transform.position = Vector3.MoveTowards(transform.position, test.path_objs[CurrentWayPointID].position, Time.deltaTime * speed); //movement speed moving toward the next waypoint
			//Debug.Log ("distance: " + distance);
			if (distance <= reachDistance) //distance between current position and next waypoint less than certain distance then change the next waypoint
            {
				if(CurrentWayPointID == 0)
				{
					GameManager.instance.player[int.Parse (this.name.Substring (this.name.Length - 1)) - 1].completeRound++;
					SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.moneySfx);
					GameManager.instance.TotalAssetAmountCalculation (GameManager.instance.moneyForEachCompletedRound + extraBonusEachRound, int.Parse (this.name.Substring (this.name.Length - 1)) - 1);
					GameManager.instance.moneyCalculation (GameManager.instance.moneyForEachCompletedRound + extraBonusEachRound, int.Parse (this.name.Substring (this.name.Length - 1)) - 1);	//for bonus money each round
				}

                if (steps > 0)
                {
                   // Debug.Log(steps);
					steps--;
					CurrentWayPointID++;
					if (CurrentWayPointID >= totalPathObjs) //if last waypoint
					{
						CurrentWayPointID %= totalPathObjs;                      //next become initial waypoint
					}
					CheckDirection();
                }
            }
        } while (transform.position != test.path_objs[(CurrentWayPointID + steps) % (test.path_objs.Count)].position);  //before it reach the number of the dice

		if(CurrentWayPointID == 0)
		{
			GameManager.instance.player[int.Parse (this.name.Substring (this.name.Length - 1)) - 1].completeRound++;
			SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.moneySfx);
			GameManager.instance.TotalAssetAmountCalculation (GameManager.instance.moneyForEachCompletedRound + extraBonusEachRound, int.Parse (this.name.Substring (this.name.Length - 1)) - 1);
			GameManager.instance.moneyCalculation (GameManager.instance.moneyForEachCompletedRound + extraBonusEachRound, int.Parse (this.name.Substring (this.name.Length - 1)) - 1);	//for bonus money each round
		}

		transform.GetChild (0).GetComponent<CharacterAnimation> ().Idle ();
       // Debug.Log(CurrentWayPointID);
       // Debug.Log(test.path_objs[CurrentWayPointID].GetComponent<LandData>().type);

		//===========================================================================================================================================================

		//=======================BELOW IS THE CHECKING FOR TYPE OF LAND TO DECIDE WHAT HAPPEN NEXT===================================================================

		LandChecking (CurrentWayPointID);

		//===========================================================================================================================================================

		//===========================================================================================================================================================
	}
	//move the camera to next Player
    public void moveTheCamera()
    {
		theCamera.enabled = true;	//activate the camera script to let the camera move to next target
		theCamera.currentPlayer = nextTarget;
    }
	//function to check the land type to decide what happen next
	public void LandChecking (int currentWayPointID)
	{
		//if the land type is normal land
		if (test.path_objs[currentWayPointID].GetComponent<LandData>().type == LandData.landType.NormalLand)
		{   //if it does not sold, prompt the buy land message
			if (test.path_objs[currentWayPointID].GetComponent<LandData>().Owner == 0)
			{   
				if (GameManager.instance.player [int.Parse (this.name.Substring (this.name.Length - 1)) - 1].AI == false) 
				{
					buyMessageUI.openBuyOrUpgradeMessage (int.Parse (this.name.Substring (this.name.Length - 1)) - 1, test.path_objs [currentWayPointID].gameObject, this.gameObject); //send the Player data and land data
				} 
				else 
				{
					AiComputer.instance.AutoBuy (buyMessageUI,int.Parse (this.name.Substring (this.name.Length - 1)) - 1, test.path_objs [currentWayPointID].gameObject, this.gameObject);
				}
			}
			//if the land has an owner
			else if (test.path_objs[currentWayPointID].GetComponent<LandData>().Owner != 0)
			{ 
				//get the Player number and the index of the owner
				int playerNum = int.Parse(this.name.Substring(this.name.Length - 1));
				int ownerNum = (int)test.path_objs [currentWayPointID].GetComponent<LandData> ().Owner;

				//check whether the land is belongs to the current Player
				//if the owner of the land is not current Player, prompt message about stepping on other Player's land
				if (ownerNum != playerNum)
				{
					//if the owner is in jail, show the "free-of-charge" message to tell the player
					if (GameManager.instance.player [ownerNum - 1].isInJail)
					{
						FOCRentalAndThemeParkMsg.SetActive (true);
						ChanceScript FOCMsg = FOCRentalAndThemeParkMsg.GetComponent<ChanceScript> ();
						FOCMsg.chanceCardText.text = "The owner of the land Player " + ((int)test.path_objs [currentWayPointID].GetComponent<LandData> ().Owner).ToString() + " is in prisoned. "
							+ "NO RENTAL will be collected :D";
						FOCMsg.currentPlayer = this.gameObject;	//change the current player to this player
					}
					//if the land is mortgaged
					else if (test.path_objs [currentWayPointID].GetComponent<LandData> ().landCurrentStatus == LandData.landStatus.Mortgaged)
					{
						FOCRentalAndThemeParkMsg.SetActive (true);
						ChanceScript FOCMsg = FOCRentalAndThemeParkMsg.GetComponent<ChanceScript> ();
						FOCMsg.chanceCardText.text = "The owner of the land (Player " + ((int)test.path_objs [currentWayPointID].GetComponent<LandData> ().Owner).ToString() + ") mortgaged this land (" + test.path_objs [currentWayPointID].GetComponent<LandData> ().landName + "). "
							+ "NO RENTAL will be collected :D";
						FOCMsg.currentPlayer = this.gameObject;	//change the current player to this player
					}
					else
					{
						//if not in jail, collect rental as usual
						stepOnOtherPlayerBg.SetActive (true);
						stepOnOtherLand.openMessage (this.gameObject, 
							test.path_objs [currentWayPointID].GetComponent<LandData> ().Owner, 
							test.path_objs [currentWayPointID].gameObject);
					}
				} 
				//if the Player is the owner of the land, prompt whether the Player want to upgrade his/her land
				else
				{
					buyMessageBg.SetActive (true);
					buyMessageUI.openBuyOrUpgradeMessage (int.Parse(this.name.Substring(this.name.Length - 1)) - 1, test.path_objs[currentWayPointID].gameObject, this.gameObject);
				}
			}
		}
		//if the land is chance land, pop up random chance card
		else if (test.path_objs[currentWayPointID].GetComponent<LandData>().type == LandData.landType.ChanceLand)
		{
			randomChance.GetComponent<RandomChance>().getCard(int.Parse(this.name.Substring(this.name.Length - 1)) - 1, this.gameObject, test.path_objs[currentWayPointID].GetComponent<LandData>().type);
		}
		//if the land is jail, straightaway move the camera to next Player
		else if (test.path_objs[currentWayPointID].GetComponent<LandData>().type == LandData.landType.Jail
			|| test.path_objs[currentWayPointID].GetComponent<LandData>().type == LandData.landType.StartingPoint)
		{
			if (GameManager.instance.gameMode == GameManager.GameModeChoices.TurnLimit) 
			{
				GameManager.instance.TurnLimitModeChecking ();
			}
			moveTheCamera();
		}
		else if (test.path_objs[currentWayPointID].GetComponent<LandData>().type == LandData.landType.Genting)
		{
			if (GameManager.instance.player [int.Parse (this.name.Substring (this.name.Length - 1)) - 1].race == GameManager.Race.Malay)
			{
				FOCRentalAndThemeParkMsg.SetActive (true);
				ChanceScript notification = FOCRentalAndThemeParkMsg.GetComponent<ChanceScript> ();
				notification.chanceCardText.text = "You are too loyal to your religion! So you are not going to gamble!";
				notification.chanceCardText.color = Color.black;
				notification.currentPlayer = this.gameObject;
			}
			else
			{
				gentingSelectionBg.SetActive (true);																//show the genting gambling message
				gentingSelectionScript.whichPlayer = int.Parse (this.name.Substring (this.name.Length - 1)) - 1;	//assign the number for player array
				gentingSelectionScript.currentPlayer = this.gameObject;												//pass the game object
				if (GameManager.instance.player [gentingSelectionScript.whichPlayer].AI) 
				{
					StartCoroutine (AiComputer.instance.DecGenting (gentingSelectionBg));
				}
			}
		}
		else if (test.path_objs[currentWayPointID].GetComponent<LandData>().type == LandData.landType.ThemePark)
		{
			FOCRentalAndThemeParkMsg.SetActive (true);
			int themeParkStopRound = test.path_objs [currentWayPointID].GetComponent<LandData> ().stopRound;
			returnToGameTurn = GameManager.instance.player [int.Parse (this.name.Substring (this.name.Length - 1)) - 1].turn
				+ themeParkStopRound + 1;	//calculation for returnToGameTurn
			ChanceScript themeParkMsg = FOCRentalAndThemeParkMsg.GetComponent<ChanceScript> ();
			themeParkMsg.chanceCardText.text = "You reached one of the Water Park in Sarawak! Have fun at there for " + themeParkStopRound + " day!";
			themeParkMsg.chanceCardText.color = Color.black;
			themeParkMsg.currentPlayer = this.gameObject;		//change the current player to this player for camera moving
			isInThemePark = true;
		}
		else if ((test.path_objs[currentWayPointID].GetComponent<LandData>().type == LandData.landType.Ferry1) || (test.path_objs[currentWayPointID].GetComponent<LandData>().type == LandData.landType.Ferry2))
		{
			randomChance.GetComponent<RandomChance> ().QNAMsg (int.Parse (this.name.Substring (this.name.Length - 1)) - 1, test.path_objs[currentWayPointID].GetComponent<LandData>().type);
			FOCRentalAndThemeParkMsg.GetComponent<ChanceScript>().currentPlayer = this.gameObject;
			if (GameManager.instance.player [int.Parse (this.name.Substring (this.name.Length - 1)) - 1].AI) 
			{
				StartCoroutine(AiComputer.instance.QNA ());
			}
		}
		else if (test.path_objs[currentWayPointID].GetComponent<LandData>().type == LandData.landType.KLIA)
		{
			GameManager.instance.theCurrentPlayer = this.gameObject;
			kliaBg.SetActive (true);
			if (GameManager.instance.player [AiComputer.instance.curPlayer].AI) 
			{
				AiComputer.instance.DecForKLIA ();
			}
		}
		CurrentWayPointID = currentWayPointID;
	}

	public void CheckDirection()
	{
		//Debug.Log (test.path_objs [CurrentWayPointID].position.x - transform.position.x);
		//if (test.path_objs [CurrentWayPointID].position.x - transform.position.x > 0) 
		//{
		//	transform.GetChild (0).GetComponent<CharacterAnimation> ().Walk (true);
		//}
		//Debug.Log(Mathf.Rad2Deg * Vector3.Angle(test.path_objs[CurrentWayPointID].position,transform.position));
		//if(Mathf.Rad2Deg * Vector3.Angle(test.path_objs[CurrentWayPointID].position,transform.position) >= 45 && Mathf.Rad2Deg * Vector3.Angle(test.path_objs[CurrentWayPointID].position,transform.position) <= 135)
		//{
		//	transform.GetChild (0).GetComponent<CharacterAnimation> ().Walk (true);	
		//}
		//else if(Mathf.Rad2Deg * Vector3.Angle(test.path_objs[CurrentWayPointID].position,transform.position) >= 225 && Mathf.Rad2Deg * Vector3.Angle(test.path_objs[CurrentWayPointID].position,transform.position) <= 315)
		//{
		//	transform.GetChild (0).GetComponent<CharacterAnimation> ().Walk (true);	
		//}
		//else if(Mathf.Rad2Deg * Vector3.Angle(test.path_objs[CurrentWayPointID].position,transform.position) > 315 || Mathf.Rad2Deg * Vector3.Angle(test.path_objs[CurrentWayPointID].position,transform.position) < 45)
		//{
		//	transform.GetChild (0).GetComponent<CharacterAnimation> ().Walk (false);	
		//}
		//else if(Mathf.Rad2Deg * Vector3.Angle(test.path_objs[CurrentWayPointID].position,transform.position) > 135 && Mathf.Rad2Deg * Vector3.Angle(test.path_objs[CurrentWayPointID].position,transform.position) < 315)
		//{
		//	transform.GetChild (0).GetComponent<CharacterAnimation> ().Walk (false);	
		//}
		//Debug.Log(test.path_objs [CurrentWayPointID].position.x - transform.position.x);
		if (test.path_objs [CurrentWayPointID].position.x - transform.position.x >= test.path_objs [CurrentWayPointID].position.z - transform.position.z) 
		{
			if ((CurrentWayPointID > 30 && CurrentWayPointID < 37) ||  (CurrentWayPointID > 46 && CurrentWayPointID <= 52) || CurrentWayPointID < 1)
			{
				transform.GetChild (0).GetComponent<CharacterAnimation> ().OnSea (false);
				if (CurrentWayPointID == 31 || CurrentWayPointID == 47 ) 
				{
					SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.ferrySfx);
				}
			} 
			else 
			{
				SoundScript.instance.audioSrc.Stop ();
				transform.GetChild (0).GetComponent<CharacterAnimation> ().Walk (false);
			}
		} 
		else
		{
			SoundScript.instance.audioSrc.Stop ();
			if ((CurrentWayPointID > 30 && CurrentWayPointID < 37) ||  (CurrentWayPointID > 46 && CurrentWayPointID <= 52) || CurrentWayPointID < 1) 
			{
				transform.GetChild (0).GetComponent<CharacterAnimation> ().OnSea (true);
				if (CurrentWayPointID == 31 || CurrentWayPointID == 47 ) 
				{
					SoundScript.instance.audioSrc.PlayOneShot (SoundScript.instance.ferrySfx);
				}
			} 
			else 
			{
				SoundScript.instance.audioSrc.Stop ();
				transform.GetChild (0).GetComponent<CharacterAnimation> ().Walk (true);
			}
		} 
		CheckDirection2 ();
	}

	public void CheckDirection2()
	{
		if ((test.path_objs [CurrentWayPointID].position.x - transform.position.x) - (test.path_objs [CurrentWayPointID].position.z - transform.position.z) <= 0) 
		{
			transform.GetChild (0).GetComponent<SpriteRenderer> ().flipX = true;
		} 
		else if ((test.path_objs [CurrentWayPointID].position.x - transform.position.x) - (test.path_objs [CurrentWayPointID].position.z - transform.position.z) > 0) 
		{
			transform.GetChild (0).GetComponent<SpriteRenderer> ().flipX = false;
		}
	}
}