﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundScript : MonoBehaviour 
{
	public static SoundScript instance;
	public AudioSource audioSrc; //create audio
	public AudioClip moneySfx;
	public AudioClip buttonClickSfx;
	public AudioClip happySfx;
	public AudioClip sadSfx;
	public AudioClip rollDicesSfx;
	public AudioClip ferrySfx;
	public AudioClip qnaCountDownSfx;
	public AudioClip buildToVillageSfx;
	public AudioClip buildSfx;
	public AudioClip kliaSfx;

	void Awake ()
	{
		if (instance == null) 
		{
			instance = this;
		} 
		else if (instance != this) 
		{
			Destroy (gameObject);
		}
	}
	public void ButtonClickSound ()
	{
		audioSrc.PlayOneShot (buttonClickSfx);
	}
}