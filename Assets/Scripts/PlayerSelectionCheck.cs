﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSelectionCheck : MonoBehaviour 
{
	CanvasScript canvasScript;
	// Use this for initialization
	void Start ()
	{
		canvasScript = GameObject.Find ("UI_canvas").GetComponent<CanvasScript>();
	}
	public void Checking ()
	{
		
		string playerNumText = this.gameObject.transform.Find ("PlayerNumText").GetComponent<Text> ().text;
		int playerIndex = int.Parse (playerNumText.Substring (playerNumText.Length - 1)) - 1;
		GameManager.instance.isProceedToPropertyList = true;
		Debug.Log ("playerIndex  : " + playerIndex);
		canvasScript.ProceedToPropertyList (playerIndex);
	}
}