﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaceSelectScript : MonoBehaviour {

    [System.Serializable]
    public struct SelectedRace
    {
        /*For the indicator
         0 means Chinese Race
         1 is Indian race
         2 is Malay race
         3 is No player
         4 is AI player
         */
        public Text raceDescription;
        public Image selectedRaceBackground;
        public Image selectedRaceCharacter;
        public int indicator;
    }
    public struct RaceSelector
    {
        public int i, j, indicator;
        public GameObject selectionParent;
        public Text canvasRaceDescription;
        public Image canvasSelectedRaceBackground, canvasSelectedRaceCharacter;
        public Button leftButton, rightButton;
    }
    public PassValueToGameManager passValueToGameManager;
    public Button startButton;
    public GameObject preloadedAssets;
    public SelectedRace[] selectedRace;
    public RaceSelector[] raceSelector;
    public int numberOfPlayer;
    private int raceAmount = 5; //When add new character, add this.
	bool[] maxNumPlayer = new bool[4];
    public GameMode gameMode;
    public Dropdown GameModeDropdown;
    public List<string> GameModeList;
	// Use this for initialization
	void Start ()
    {
        selectedRace = new SelectedRace[raceAmount];
        raceSelector = new RaceSelector[4];
        startButton = GameObject.Find("Start_Button").GetComponent<Button>();
        gameMode = new GameMode();
        startButton.onClick.AddListener(StartOnClick);
        LoadRaces();

        preloadedAssets = GameObject.Find("preloaded_assets");
        PreloadAssets();
        SetDefaultRace();

        GameModeDropdown = GameObject.Find("GameMode_List").GetComponent<Dropdown>();
        LoadGameModes();
        GameManager.instance.gameMode = GameManager.GameModeChoices.Bankrupt; //If nothing is chosen, use the default value.
        GameModeDropdown.onValueChanged.AddListener(delegate 
        {
            DropdownValueChange(GameModeDropdown);
        });

    }
	    
	
    void StartOnClick()
    {
        numberOfPlayer = 0;
        for (int k = 0; k < 4; k++){
            if (raceSelector[k].indicator != 3)
                numberOfPlayer++;
            //Debug.Log("Player "+ k + "is " + raceSelector[k].indicator);
        }
        Debug.Log("There are " + numberOfPlayer + " player in this game");
        PassValueToGameManager.instance.playerAmountInt = numberOfPlayer;
        if (numberOfPlayer != 0)
        {
            gameMode.OnStartGame();
        }
        else
        {
            Debug.Log("No player is in the game");
        }
    }
    public void LeftButtonOnClick(int k)
    {
        if (raceSelector[k].i == 0)
            raceSelector[k].i = raceAmount;
        raceSelector[k].i--;
        raceSelector[k].j = raceSelector[k].i % raceAmount; //When add new character, edit this

        raceSelector[k].canvasRaceDescription.text = selectedRace[raceSelector[k].j].raceDescription.text;
        raceSelector[k].canvasSelectedRaceBackground.sprite = selectedRace[raceSelector[k].j].selectedRaceBackground.sprite;
        raceSelector[k].canvasSelectedRaceCharacter.sprite = selectedRace[raceSelector[k].j].selectedRaceCharacter.sprite;
        raceSelector[k].indicator = selectedRace[raceSelector[k].j].indicator;
		UpdateGameManager (k, raceSelector[k].j);
    }

    public void RightButtonOnClick(int k)
    {
       // Debug.Log("clicked right button");
        raceSelector[k].i++;
       // Debug.Log(raceSelector[k].i);
        raceSelector[k].j = raceSelector[k].i % raceAmount; //When add new character, edit this
       // Debug.Log(raceSelector[k].j);

        raceSelector[k].canvasRaceDescription.text = selectedRace[raceSelector[k].j].raceDescription.text;
        raceSelector[k].canvasSelectedRaceBackground.sprite = selectedRace[raceSelector[k].j].selectedRaceBackground.sprite;
        raceSelector[k].canvasSelectedRaceCharacter.sprite = selectedRace[raceSelector[k].j].selectedRaceCharacter.sprite;
        raceSelector[k].indicator = selectedRace[raceSelector[k].j].indicator;
		UpdateGameManager (k, raceSelector[k].j);
    }

	public void UpdateGameManager(int k, int raceNum)
	{
		Debug.Log ("Race Num is: " + raceNum);
		int playerNum = 0;
		if (k > 1) 
			playerNum = k;
		else
			playerNum = 0;
		

		if (raceNum == 3) 
		{
			maxNumPlayer [k] = false;
			GameManager.instance.maxPlayer--;
			GameManager.instance.player = new GameManager.Player[GameManager.instance.maxPlayer];
		}
		else 
		{
			if (maxNumPlayer [k] == false) 
			{
				maxNumPlayer [k] = true;
				GameManager.instance.maxPlayer++;
				GameManager.instance.player = new GameManager.Player[GameManager.instance.maxPlayer];
			}
			if (raceNum < 3) 
			{
				GameManager.instance.player [playerNum].AI = false;
				GameManager.instance.player [playerNum].race = (GameManager.Race)raceNum;
			}
			else if (raceNum == 4) 
			{
				GameManager.instance.player [playerNum].AI = true;
				GameManager.instance.player [playerNum].race = (GameManager.Race)Random.Range (0, 2);
			}
		}
		//Debug.Log ("MaxPlayer:" + GameManager.instance.maxPlayer);
		//Debug.Log ("Player [" + playerNum + "] race :" +  GameManager.instance.player [playerNum].race);
	}

    void PreloadAssets()
    {
        selectedRace[0].raceDescription = preloadedAssets.transform.Find("chinese").Find("chinese_description").GetComponent<Text>();
        selectedRace[0].selectedRaceBackground = preloadedAssets.transform.Find("chinese").Find("chinese_bg").GetComponent<Image>();
        selectedRace[0].selectedRaceCharacter = preloadedAssets.transform.Find("chinese").Find("chinese_character").GetComponent<Image>();
        selectedRace[0].indicator = 0;

        selectedRace[1].raceDescription = preloadedAssets.transform.Find("indian").Find("indian_description").GetComponent<Text>();
        selectedRace[1].selectedRaceBackground = preloadedAssets.transform.Find("indian").Find("indian_bg").GetComponent<Image>();
        selectedRace[1].selectedRaceCharacter = preloadedAssets.transform.Find("indian").Find("indian_character").GetComponent<Image>();
        selectedRace[1].indicator = 1;

        selectedRace[2].raceDescription = preloadedAssets.transform.Find("malay").Find("malay_description").GetComponent<Text>();
        selectedRace[2].selectedRaceBackground = preloadedAssets.transform.Find("malay").Find("malay_bg").GetComponent<Image>();
        selectedRace[2].selectedRaceCharacter = preloadedAssets.transform.Find("malay").Find("malay_character").GetComponent<Image>();
        selectedRace[2].indicator = 2;

        selectedRace[3].raceDescription = preloadedAssets.transform.Find("no_player").Find("no_player_description").GetComponent<Text>();
        selectedRace[3].selectedRaceBackground = preloadedAssets.transform.Find("no_player").Find("no_player_bg").GetComponent<Image>();
        selectedRace[3].selectedRaceCharacter = preloadedAssets.transform.Find("no_player").Find("no_player_character").GetComponent<Image>();
        selectedRace[3].indicator = 3;

        selectedRace[4].raceDescription = preloadedAssets.transform.Find("ai_player").Find("ai_player_description").GetComponent<Text>();
        selectedRace[4].selectedRaceBackground = preloadedAssets.transform.Find("ai_player").Find("ai_player_bg").GetComponent<Image>();
        selectedRace[4].selectedRaceCharacter = preloadedAssets.transform.Find("ai_player").Find("ai_player_character").GetComponent<Image>();
        selectedRace[4].indicator = 4;
    }

    void LoadGameModes()
    {
        GameModeList = new List<string>
        { 
            //Any list can be added here
            "Bankrupt Mode",                    //0
            "Turn Limit Mode - 100 turns",      //1
            "Turn Limit Mode - 200 turns",      //2
            "Turn Limit Mode - 300 turns",      //3
            "Metropolis Mode - 3 Metropolis",   //4
            "Metropolis Mode - 5 Metropolis",   //5
            "Metropolis Mode - 7 Metropolis",   //6
            
        };
        GameModeDropdown.AddOptions(GameModeList);
        GameModeDropdown.captionText.text = "Game Mode Selection";
    }

    void LoadRaces()
    {
        for (int k = 0; k < 4; k++)
        {
            raceSelector[k].i = 0;
            raceSelector[k].j = 0;
            raceSelector[k].indicator = 3;
            raceSelector[k].selectionParent = GameObject.Find("Player_Selection_" + (k + 1));
            raceSelector[k].leftButton = raceSelector[k].selectionParent.transform.Find("left_button").GetComponent<Button>();
            raceSelector[k].rightButton = raceSelector[k].selectionParent.transform.Find("right_button").GetComponent<Button>();
            raceSelector[k].canvasRaceDescription = raceSelector[k].selectionParent.transform.Find("race_description").GetComponent<Text>();
            raceSelector[k].canvasSelectedRaceBackground = raceSelector[k].selectionParent.transform.Find("selected_race").Find("layer_1").GetComponent<Image>();
            raceSelector[k].canvasSelectedRaceCharacter = raceSelector[k].selectionParent.transform.Find("selected_race").Find("layer_2").GetComponent<Image>();

        }
    }

    void DropdownValueChange(Dropdown change)
    {
        Debug.Log(change.value);
        switch (change.value)
        {
            case 0: GameManager.instance.gameMode = GameManager.GameModeChoices.Bankrupt;
                break;
            case 1: GameManager.instance.gameMode = GameManager.GameModeChoices.TurnLimit;
                GameManager.instance.numberOfTurn = 100;
                break;
            case 2: GameManager.instance.gameMode = GameManager.GameModeChoices.TurnLimit;
                GameManager.instance.numberOfTurn = 200;
                break;
            case 3: GameManager.instance.gameMode = GameManager.GameModeChoices.TurnLimit;
                GameManager.instance.numberOfTurn = 300;
                break;
            case 4: GameManager.instance.gameMode = GameManager.GameModeChoices.Metropolis;
                GameManager.instance.numberOfMetropolis = 3;
                break;
            case 5: GameManager.instance.gameMode = GameManager.GameModeChoices.Metropolis;
                GameManager.instance.numberOfMetropolis = 5;
                break;
            case 6: GameManager.instance.gameMode = GameManager.GameModeChoices.Metropolis;
                GameManager.instance.numberOfMetropolis = 7;
                break;
            default:
                break;
        }
    }

    void SetDefaultRace()
    {
        
        for (int k = 0; k < 4; k++)
        {
            raceSelector[k].canvasRaceDescription.text = selectedRace[3].raceDescription.text;
            raceSelector[k].canvasSelectedRaceBackground.sprite = selectedRace[3].selectedRaceBackground.sprite;
            raceSelector[k].canvasSelectedRaceCharacter.sprite = selectedRace[3].selectedRaceCharacter.sprite;
            raceSelector[k].indicator = selectedRace[3].indicator;
        }
        
    }
}
