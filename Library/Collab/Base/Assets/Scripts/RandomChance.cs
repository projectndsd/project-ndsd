﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomChance : MonoBehaviour
{
	[Header ("Setting")]
    public GameObject chanceCardBg;
	public GameObject gentingSelectionBg;
	public GameObject jailGameObject;
	public Button chanceCardOKButton;
    public CollectGameData chanceCardCollector;
	public CollectGameData gentingSmallCardCollector;
	public CollectGameData gentingBigCardCollector;
	public CollectQNAData qnaListCollector;
	public int jailStopRound;
	public float characterMoveToJailTime;
	public AnimationCurve animCurve;
	public int ferryFee1;
	public int ferryFee2;

	[Header("Debug Purpose")]
	public Text turnText;	//will remove

	[Header ("Internal Setting")]
	public bool isSmallCard;
	public int selectedCardNum;
	public int playerIndexArray;
	public LandData.landType ferryType;

	private GameObject qnaBg;
    private ChanceScript chanceCardBgUI;
	private GentingScript gentingSelectionBgUI;
	private QNAScript qnaScript;
	private string[] answerChoices;
    private Color selectedColor;

    // Use this for initialization
    void Start()
    {
		answerChoices = new string[3];
        chanceCardBgUI = chanceCardBg.GetComponent<ChanceScript> ();
		gentingSelectionBgUI = gentingSelectionBg.GetComponent<GentingScript> ();
		GameObject theCanvas = GameObject.Find ("Canvas");
		qnaBg = theCanvas.transform.Find ("QNABackground").gameObject;
		qnaScript = qnaBg.GetComponent<QNAScript> ();
    }
//	void Update ()
//	{
//		//for function testing
//		if (Input.GetKeyDown (KeyCode.I))
//			QNAMsg ();
//	}
	//get the card based on the type of the land that the player stepped on
	public void getCard (int whichPlayer, GameObject currentPlayer, LandData.landType theLandType)
    {
		switch (theLandType) 
		{
		case LandData.landType.ChanceLand:
			ChanceLandMsg (currentPlayer, whichPlayer);
			break;
		case LandData.landType.Genting:
			GentingMsg (currentPlayer, whichPlayer);
			break;
		}
    }
	//function for chance card message
	public void ChanceLandMsg (GameObject currentPlayer, int whichPlayer)
	{
		selectedCardNum = Random.Range(0, chanceCardCollector.cardList.Count); //get random card
		SelectTextColor (chanceCardCollector.cardList[selectedCardNum].amountOfMoney);
		chanceCardBgUI.open();
		chanceCardBgUI.currentPlayer = currentPlayer;
		chanceCardBgUI.DisplayCard(
			chanceCardCollector.cardList[selectedCardNum].cardMessage,
			selectedColor,
			chanceCardCollector.cardList[selectedCardNum].amountOfMoney, whichPlayer);
            
		//check if the selected chance card is "go to jail" card
		if (chanceCardCollector.cardList [selectedCardNum].name == "go to jail") 
		{
			FollowPath followPathForPlayer = currentPlayer.GetComponent <FollowPath>();
			followPathForPlayer.returnToGameTurn = 
				GameManager.instance.player [int.Parse (currentPlayer.name.Substring (currentPlayer.name.Length - 1)) - 1].turn
				+ jailStopRound + 1;	//assign the turn number that the player will back to the game to the returnToGameTurn variable in the player's FollowPath script
			followPathForPlayer.isJailed = true;
			GameManager.instance.player [int.Parse (currentPlayer.name.Substring (currentPlayer.name.Length - 1)) - 1].isInJail =
				followPathForPlayer.isJailed;	//make the isInJailed variable to true for rental system checking
			StartCoroutine (MoveCharToJail (currentPlayer));	//move the player to the jail
		}
	}
	//pop up QNA Message
	public void QNAMsg (int playerIndex, LandData.landType ferryFeeType)
	{
		playerIndexArray = playerIndex;
		ferryType = ferryFeeType;
		selectedCardNum = Random.Range(0, qnaListCollector.QNAList.Count); //get random question
		//assign all the preset answers to an array
		answerChoices [0] = qnaListCollector.QNAList [selectedCardNum].correctAnswer;
		answerChoices [1] = qnaListCollector.QNAList [selectedCardNum].wrongAnswer1;
		answerChoices [2] = qnaListCollector.QNAList [selectedCardNum].wrongAnswer2;
		qnaBg.SetActive (true);
		qnaScript.questionText.text = qnaListCollector.QNAList [selectedCardNum].question;	//display the question
		int randomAnswerPlacementNum = Random.Range (0, answerChoices.Length);	//get random answer placement number for buttons

		//assign answer texts to all of the button texts
		for (int i = 0; i < qnaScript.answerButtonText.Count; i++)
		{
			qnaScript.answerButtonText [i].text = answerChoices [randomAnswerPlacementNum];
			if (randomAnswerPlacementNum >= (qnaScript.answerButtonText.Count - 1)) 
			{
				randomAnswerPlacementNum = 0;
			}
			else
			{
				randomAnswerPlacementNum++;
			}
		}
	}
	//animation for moving the player to jail
	public IEnumerator MoveCharToJail (GameObject theCurrentPlayer)
	{
		chanceCardOKButton.interactable = false;
		Vector3 currentPos = theCurrentPlayer.transform.position;
		Vector3 nextPos = jailGameObject.transform.position;
		float elapsedTime = 0f;
		float fracPath = 0f;
		while (Vector3.Distance (theCurrentPlayer.transform.position, nextPos) > 0f)
		{
			yield return new WaitForEndOfFrame ();
			elapsedTime += Time.deltaTime;
			fracPath = elapsedTime / characterMoveToJailTime;
			theCurrentPlayer.transform.position = Vector3.Lerp (currentPos, nextPos, animCurve.Evaluate (fracPath));	
		}
		theCurrentPlayer.GetComponent<FollowPath> ().CurrentWayPointID = 9;	//assign the jail id to currentwaypointid
		chanceCardOKButton.interactable = true;
	}
	//function for genting message
	public void GentingMsg (GameObject currentPlayer, int whichPlayer)
	{
		string cardMsg;
		int amount;
		//check if the clicked button is for small card or big card
		//then randomly get a card from small card list or big card list
		if (isSmallCard)
		{
			selectedCardNum = Random.Range (0, gentingSmallCardCollector.cardList.Count); //get random card
			SelectTextColor (gentingSmallCardCollector.cardList[selectedCardNum].amountOfMoney);
			amount = gentingSmallCardCollector.cardList [selectedCardNum].amountOfMoney;
		}
		else
		{
			selectedCardNum = Random.Range (0, gentingBigCardCollector.cardList.Count); //get random card
			SelectTextColor (gentingBigCardCollector.cardList[selectedCardNum].amountOfMoney);
			amount = gentingBigCardCollector.cardList [selectedCardNum].amountOfMoney;
		}
		//if the selected color is red, display the sad message
		//else, display the happy message
		if (selectedColor == Color.red) 
		{
			cardMsg = "Today is not your lucky day... " + amount.ToString ();
		} 
		else
		{
			cardMsg = "Wow! You are so lucky! " + amount.ToString ();
		}

		chanceCardBgUI.open();
		chanceCardBgUI.currentPlayer = currentPlayer;
		chanceCardBgUI.DisplayCard (cardMsg, selectedColor, amount, whichPlayer);
		gentingSelectionBg.SetActive (false);		//inactive the genting selection message
	}

	public void SelectTextColor (int moneyAmount)
	{
		if (moneyAmount > 0)
		{
			selectedColor = Color.green;
		}
		else if (moneyAmount <= 0)
		{
			selectedColor = Color.red;
		}
	}
}