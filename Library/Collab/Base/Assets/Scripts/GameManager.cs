﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public enum GameModeChoices
	{
		TurnLimit,
		Bankrupt,
		Metropolis
	}
	//public GameObject landMultiplierGO;
	public int steps;
	public int maxPlayer = 2; //default player is 2
	public enum Race 
	{
		chinese, 
		indian,
		malay
	}
	public Race playerRace;
    public PassValueToGameManager passValueToGameManager;
	public static GameManager instance;
	public int moneyForEachCompletedRound;
	public RuntimeAnimatorController[] animatorOverride;
	public int a;

	public struct Player
	{
		public GameObject character;
		public List<GameObject> property;
		public int steps;
		public int money;
		public Race race;
		public int turn;
		public int totalAssetAmount;
		public bool isInJail;
		public bool AI;
		//public List<GameObject> landButtonSpawned;
	}

	public Player[] player;

	public GameModeChoices gameMode;

	[Header("Turn Limit Mode Setting")]
	public int numberOfTurn;

	[Header("Metropolis Mode Setting")]
	public int numberOfMetropolis;

	private StateMultiplier stateMultiplerScript;
	private string[] splitedName;
	private int ownerNum;

	// Use this for initialization
	void Awake () 
	{
		if (instance == null) 
		{
			instance = this;
		} 
		else if (instance != this) 
		{
			Destroy (gameObject);
		}
		DontDestroyOnLoad (this.gameObject);
		Debug.Log (maxPlayer);
	}

	public void onStartButton()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene ("2.ChooseRace");
	}

	public void onCreditButton()
	{
		Debug.Log ("Credit Button Clicked");
	}

	public void onSettingButton()
	{
		Debug.Log ("Setting Button Clicked");
	}
	public void startGameButton()
	{
        //passValueToGameManager = GameObject.Find("PlayerAmount").GetComponent<PassValueToGameManager>(); //Pass the value from the input box

		UnityEngine.SceneManagement.SceneManager.LoadScene ("3.InGame");
		StartCoroutine(LoadGame ());

	}

	public void moneyCalculation (int amount, int playerArrayIndex)
	{
		player [playerArrayIndex].money += amount;
		//Debug.Log ("player" + (playerArrayIndex + 1) + " " + player [playerArrayIndex].money);
	}
	public void addTurn (int playerArrayIndex)
	{
		player [playerArrayIndex].turn += 1;
		GameObject.Find ("Card Manager").GetComponent<RandomChance>().turnText.text = player [playerArrayIndex].turn.ToString();
		//Debug.Log ("player" + (playerArrayIndex + 1) + "'s turn: " + player [playerArrayIndex].turn);
	}
	public void EndGameChecking ()
	{	//check if the game mode is turn limit
		if (gameMode == GameModeChoices.TurnLimit)
		{	//check the last player's turn if equal to preset number of turn limit
			if (player [maxPlayer - 1].turn == numberOfTurn) 
			{	//if yes, calculate all the assets owned by each player and the cash owned
				for (int i = 0; i < maxPlayer; i++)
				{
					foreach (GameObject land in player [i].property)
					{
						LandData landData = land.GetComponent<LandData> ();
						switch (landData.landCurrentStatus)
						{
						case LandData.landStatus.Land:
							player [i].totalAssetAmount += landData.landPrice;
							break;
						case LandData.landStatus.Kampung:
							player [i].totalAssetAmount += landData.landPrice + landData.kampungPrice;
							break;
						case LandData.landStatus.City:
							player [i].totalAssetAmount += landData.landPrice + landData.kampungPrice + landData.cityPrice;
							break;
						case LandData.landStatus.Metropolis:
							player [i].totalAssetAmount += landData.landPrice + landData.kampungPrice + landData.cityPrice + landData.metropolisPrice;
							break;
						}
					}
					player [i].totalAssetAmount += player [i].money;
				}
				UnityEngine.SceneManagement.SceneManager.LoadScene ("4.ScoreBoard");
			}
		}
	}
	public void multiplierChecking (string theLandName, int playerNum)
	{
		splitedName = theLandName.Split (' ');	//split the name for state checking
		foreach (State state in stateMultiplerScript.landClassification) 
		{	//if same state, check for land
			if (splitedName [splitedName.Length - 1] == state.stateName)
			{
				for (int i = 0; i < state.landList.Count; i++)
				{
					ownerNum = (int)state.landList [i].GetComponent<LandData> ().Owner;	//get the owner number to know which player is the owner
					if (ownerNum != playerNum)
					{
						break;
					} 
					//if all the lands of the state is under same player, change the rental according to the multiplier bonus
					if (i == (state.landList.Count - 1))
					{
						for (int a = 0; a < state.landList.Count; a++) 
						{
							LandData landData = state.landList [a].GetComponent<LandData> ();
							landData.landRental = (int)((float)landData.landRental * (1.0f + (float)state.landList.Count * 0.1f));	//update the rental
							landData.kampungRental = (int)((float)landData.kampungRental * (1.0f + (float)state.landList.Count * 0.1f));
							landData.cityRental = (int)((float)landData.cityRental * (1.0f + (float)state.landList.Count * 0.1f));
							landData.metropolisRental = (int)((float)landData.metropolisRental * (1.0f + (float)state.landList.Count * 0.1f));
						}
					}
				}
				break;
			}
		}
	}
	public void Update()
	{
		//the following line is to debug the player property
		if(Input.GetKeyDown(KeyCode.A))
			{
				for (int i = 0; i < maxPlayer; i++) 
				{
					for(int j = 0; j < player[i].property.Count; j++)
					{
						Debug.Log (player [i].property [j].gameObject.name);
						Debug.Log (player [i].property [j].GetComponent<LandData> ().grade);
						Debug.Log (player [i].property [j].GetComponent<LandData> ().landCurrentStatus);
						Debug.Log (player [i].property [j].GetComponent<LandData> ().Owner);
					}
					Debug.Log (player [i].AI);
				}
			}
		//the followng line is to test the change races function
		if (Input.GetKeyDown (KeyCode.S)) 
		{
			a++;
			for (int i = 0; i < maxPlayer; i++) 
			{
				player [i].race = (Race)(a + i % ((int)Race.malay + 1)); //change race
				player [i].character.transform.GetChild (0).GetComponent<Animator> ().runtimeAnimatorController = animatorOverride[a % 3]; //change race animation in the game object
			}
		}
	}

	public IEnumerator LoadGame()
	{
		while(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "3.InGame")
		{
		yield return new WaitForSeconds (0.05f); //need to wait only work, i dont know why
		}
		Debug.Log (maxPlayer);
		for (int i = 0; i < maxPlayer; i++) 
		{
			Debug.Log ((int)player [i].race);
			player [i].character = GameObject.Find ("Player" + (i+1)); //assinging the gameobject into this array
			player [i].character.transform.GetChild(0).GetComponent<Animator> ().runtimeAnimatorController = animatorOverride[(int)player[i].race];
			player [i].money = 50000;
			player [i].steps = 0;
			player [i].property = new List<GameObject> ();
			player [i].turn = 0;
			player [i].totalAssetAmount = 0;
			//player [i].landButtonSpawned = new List<GameObject> ();
			//Debug.Log (player [i].race);
		}
		for (int i = 4; i > maxPlayer; i--) 
		{
			while (!GameObject.Find ("Player" + i)) 
			{
				yield return new WaitForSeconds (0.05f); //need to wait only work, i dont know why
			}
			GameObject.Find ("Player" + i).SetActive(false); //deactive the extra character
		}
		stateMultiplerScript = GameObject.Find("Land Manager").GetComponent<StateMultiplier>();
	}

	public void BackMain()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene ("1.Menu");
	}
}