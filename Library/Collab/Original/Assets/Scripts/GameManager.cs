﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public int steps;
	public int maxPlayer = 2; //default player is 2
	public enum Race {chinese, indian, malay}
	public Race playerRace;
    public PassValueToGameManager passValueToGameManager;
	public static GameManager instance;
	public int moneyForEachCompletedRound;
	public RuntimeAnimatorController[] animatorOverride;
	public int a;

	public struct Player
	{
		public GameObject character;
		public List<GameObject> property;
		public int steps;
		public int money;
		public Race race;
		public int turn;
		public bool isInJail;
		public bool AI;
		//public List<GameObject> landButtonSpawned;
	}

	public Player[] player;

	// Use this for initialization
	void Awake () 
	{
		if (instance == null) 
		{
			instance = this;
		} 
		else if (instance != this) 
		{
			Destroy (gameObject);
		}
		DontDestroyOnLoad (this.gameObject);
	}

	public void onStartButton()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene ("2.ChooseRace");
	}

	public void onCreditButton()
	{
		Debug.Log ("Credit Button Clicked");
	}

	public void onSettingButton()
	{
		Debug.Log ("Setting Button Clicked");
	}
	public void startGameButton()
	{
        //passValueToGameManager = GameObject.Find("PlayerAmount").GetComponent<PassValueToGameManager>(); //Pass the value from the input box
        maxPlayer = PassValueToGameManager.instance.playerAmountInt; //Now the maxPlayer will be the the amount the player input. 

		UnityEngine.SceneManagement.SceneManager.LoadScene ("3.InGame");
		StartCoroutine(LoadGame ());

	}

	public void moneyCalculation (int amount, int playerArrayIndex)
	{
		player [playerArrayIndex].money += amount;
		//Debug.Log ("player" + (playerArrayIndex + 1) + " " + player [playerArrayIndex].money);
	}
	public void addTurn (int playerArrayIndex)
	{
		player [playerArrayIndex].turn += 1;
		GameObject.Find ("Card Manager").GetComponent<RandomChance>().turnText.text = player [playerArrayIndex].turn.ToString();
		//Debug.Log ("player" + (playerArrayIndex + 1) + "'s turn: " + player [playerArrayIndex].turn);
	}
	public void Update()
	{
		//the following line is to debug the player property
		if(Input.GetKeyDown(KeyCode.A))
			{
				for (int i = 0; i < maxPlayer; i++) 
				{
					for(int j = 0; j < player[i].property.Count; j++)
					{
						Debug.Log (player [i].property [j].gameObject.name);
						Debug.Log (player [i].property [j].GetComponent<LandData> ().grade);
						Debug.Log (player [i].property [j].GetComponent<LandData> ().landCurrentStatus);
						Debug.Log (player [i].property [j].GetComponent<LandData> ().Owner);
					}
					Debug.Log (player [i].AI);
				}
			}
		//the followng line is to test the change races function
		if (Input.GetKeyDown (KeyCode.S)) 
		{
			a++;
			for (int i = 0; i < maxPlayer; i++) 
			{
				player [i].race = (Race)(a + i % ((int)Race.malay + 1)); //change race
				player [i].character.transform.GetChild (0).GetComponent<Animator> ().runtimeAnimatorController = animatorOverride[a % 3]; //change race animation in the game object
			}
		}
	}

	public IEnumerator LoadGame()
	{
		while(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "3.InGame")
		{
		yield return new WaitForSeconds (0.05f); //need to wait only work, i dont know why
		}
		player = new Player[maxPlayer];
		for (int i = 0; i < maxPlayer; i++) 
		{
			player [i].character = GameObject.Find ("Player" + (i+1)); //assinging the gameobject into this array
			player [i].money = 50000;
			player [i].steps = 0;
			player [i].property = new List<GameObject> ();
			player [i].race = (Race)(i % ((int)Race.malay + 1));
			player [i].turn = 0;
			//player [i].landButtonSpawned = new List<GameObject> ();
			//Debug.Log (player [i].race);
		}
		player [1].AI = true;
		for (int i = 4; i > maxPlayer; i--) 
		{
			GameObject.Find ("Player" + i).SetActive(false); //deactive the extra character
		}

	}

	public void BackMain()
	{
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("1.Menu");
	}
}